-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 21 2015 г., 09:39
-- Версия сервера: 5.5.41
-- Версия PHP: 5.3.10-1ubuntu3.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `geo.itsgis.ru`
--

-- --------------------------------------------------------

--
-- Структура таблицы `layer`
--

CREATE TABLE IF NOT EXISTS `layer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(500) NOT NULL,
  `layer_resources` varchar(500) NOT NULL,
  `is_use_wms` tinyint(1) NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  `wms_layer` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `layer`
--

INSERT INTO `layer` (`id`, `alias`, `layer_resources`, `is_use_wms`, `is_available`, `wms_layer`) VALUES
(1, 'Закрепленные территории', 'http://saturn.itsgis.ru:8733/geoserver/itsgis/wms', 1, 1, 'itsgis:streets');

-- --------------------------------------------------------

--
-- Структура таблицы `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `id` bigint(20) NOT NULL,
  `alias` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `zoom_levels` int(11) NOT NULL,
  `tiles_path` varchar(200) NOT NULL,
  `bound_lon_1` bigint(20) NOT NULL,
  `bound_lat_1` bigint(20) NOT NULL,
  `bound_lon_2` bigint(20) NOT NULL,
  `bound_lat_2` bigint(20) NOT NULL,
  `min_resolution` double NOT NULL,
  `max_resolution` double NOT NULL,
  `custom_map_center_lon` double NOT NULL,
  `custom_map_center_lat` double NOT NULL,
  `available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `map`
--

INSERT INTO `map` (`id`, `alias`, `name`, `zoom_levels`, `tiles_path`, `bound_lon_1`, `bound_lat_1`, `bound_lon_2`, `bound_lat_2`, `min_resolution`, `max_resolution`, `custom_map_center_lon`, `custom_map_center_lat`, `available`) VALUES
(3833856, 'Samara', 'Самара', 7, 'http://app.itsgis.ru:80/municipal-geoportal/samara/map/', -25607, -12496, 21963, 40620, 0.2026214599609375, 12.9677734375, 4046.7511820995, 2135.5947661482, 1),
(10584064, 'Sol-Iletsk', 'Соль-Илецк', 7, 'http://app.itsgis.ru:80/municipal-geoportal/sol-iletsk/map/', 53946, 69856, 62508, 78418, 0.03266143798828125, 2.09033203125, 58608.761230469, 72125.077148438, 1),
(27295744, 'Surgut', 'Сургут', 8, 'http://app.itsgis.ru:80/municipal-geoportal/surgut/map/', -29410, -6219, 21118, 44309, 0.09637451171875, 12.3359375, 406.7004394531, -339.4310302735, 1),
(34439168, 'Oktyabrsk', 'Октябрьск', 7, 'http://app.itsgis.ru/municipal-geoportal/oktyabrsk/map/', -14645, -4855, 13925, 23715, 0.10898590087890625, 6.97509765625, 2.7004394531, 2.4310302735, 1),
(39387136, 'Balakovskaya_aes', 'Балаковская АЭС', 4, 'http://app.itsgis.ru/municipal-geoportal/balakovskaya_aes/map/', -2055, -4012, 4277, 2320, 0.1932373046875, 1.5458984375, 230.25897917551, -3.6646009496399, 1),
(40599552, 'Kinel-Cherkassy', 'Кинель-Черкассы', 7, 'http://app.itsgis.ru/municipal-geoportal/kinel-cherkassy/map/', -271375, 913504, -259462, 925417, 0.045444488525390625, 2.908447265625, -265141.10684203, 917168.27999876, 1),
(45973504, 'Zhigulevsk', 'Жигулёвск', 7, 'http://app.itsgis.ru/municipal-geoportal/zhigulevsk/map/', -7258, -6351, 5877, 6784, 0.050106048583984375, 3.206787109375, 100, 100, 1),
(50298880, 'Vladimir', 'Владимир', 7, 'http://app.itsgis.ru:80/municipal-geoportal/vladimir/map/', -14912, -11322, 18317, 21907, 0.1267585754394531, 8.112548828125, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `map_options`
--

CREATE TABLE IF NOT EXISTS `map_options` (
  `id` bigint(20) NOT NULL,
  `map_resources` varchar(500) NOT NULL,
  `zoom_levels` bigint(20) NOT NULL,
  `alias` varchar(200) NOT NULL,
  `minx` bigint(20) NOT NULL,
  `miny` bigint(20) NOT NULL,
  `maxx` bigint(20) NOT NULL,
  `maxy` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `map_options`
--

INSERT INTO `map_options` (`id`, `map_resources`, `zoom_levels`, `alias`, `minx`, `miny`, `maxx`, `maxy`) VALUES
(3833856, 'http://storage.its-spc.ru/itsgis-samara-backing-20110720/', 9, '', 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
