<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'KPA371163',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('85.236.178.2','::80'),
		),
		
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/

		// database settings are configured in database.php
		'db'=>array(
			'connectionString' => 'pgsql:host=app.itsgis.ru;port=5432;dbname=itsgis',
			'emulatePrepare' => true,
			'username' => 'pgroot',
			'password' => 'KPA371163',
			'charset' => 'utf8',
		),
		'db_geo'=>array(
			'class'=>'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=geo.itsgis.ru',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		'geo'=>array(
			'class'=>'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=i-GIS',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'xZTXncW9',
			'charset' => 'utf8',
		),
		//'db'=>require(dirname(__FILE__).'/database.php'),
		//'geo.itsgis.db'=>require(dirname(__FILE__).'/geo-itsgis-database.php'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => CLogger::LEVEL_ERROR,
					'logFile' => 'errors.log'
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => CLogger::LEVEL_WARNING,
					'logFile' => 'warnings.log'
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => CLogger::LEVEL_INFO,
					'logFile' => 'info.log'
				),
				/*array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),*/
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
	'sourceLanguage'=>'ru',
	'language' => 'ru'
);
