<?php
class UserLoginForm extends CFormModel
{
	public $username;
	public $password;
	public $captcha;
	private $_identity;

	public function get_username(){
		return $this->username;
	}
	public function get_password(){
		return $this->password;
	}
	public function get_captcha(){
		return $this->captcha;
	}
	public function get_identity(){
		return $this->_identity;
	}

	public function set_username($username){
		$this->username = $username;
	}
	public function set_password($password){
		$this->password = $password;
	}
	public function set_captcha($captcha){
		$this->captcha = $captcha;
	}
	public function set_identity($identity){
		$this->_identity = $identity;
	}

	public function rules()
	{
		return array(
			array('username', 'required', 'message' => 'Необходимо ввести логин.'),
      		array('password', 'required', 'message' => 'Необходимо ввести пароль.'),
      		array('captcha', 'captcha', 'message' => 'Результат выражения с картинки неправильный.'),
      		array('password', 'authenticate'),
			// username and password are required
			//array('username, password', 'required'),
			// rememberMe needs to be a boolean
			//array('rememberMe', 'boolean'),
			// password needs to be authenticated
			//array('password', 'authenticate'),
		);
	}

	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors()){
			$this->_identity = new UserIdentity($this->username, $this->password);
			if(!$this->_identity->authenticate()){
				$this->addError('password','Неверный логин и/или пароль.');
			}
			else{
				$user = UserManager::get_user($this->username);
				$date = $user->get_user_expired_time();
				if(!$user->get_user_enabled()){
					$this->addError('username', 'Ваша учетная запись заблокирована! Обратитесь к администратору.');
				}
				else{
					if($date != ""){
						if(date("Y-m-d H:i:s") > $date){
							$this->addError('username', 'Ваша учетная запись просрочена! Обратитесь к администратору.');
						}
					}
				}

				/*if(((date("Y-m-d H:i:s") > $date)||($user->get_user_enabled() == false)) && ($date != "")){
						$this->addError('username', 'Ваша учетная запись просрочена или заблокирована! Обратитесь к администратору.');
        		}*/
			}
		}
		/*$identity = new UserIdentity($_POST['username'], $_POST['password']);
        		if($identity->authenticate()){*/
		/*if(!$this->hasErrors()){
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','Incorrect username or password.');
		}*/
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	/*public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}*/
}
