<?php
class MapFeedbackForm extends CFormModel {
	public $sender_name;
	public $sender_email;
	public $sender_comments;
	public $attachment;
	public $address;
	public $lon;
	public $lat;

	public function get_sender_name() {
		return $this->sender_name;
	}
	public function get_sender_email() {
		return $this->sender_email;
	}
	public function get_sender_comments() {
		return $this->sender_comments;
	}
	public function get_attachment() {
		return $this->attachment;
	}
	public function get_address() {
		return $this->address;
	}
	public function get_lon() {
		return $this->lon;
	}
	public function get_lat() {
		return $this->lat;
	}

	public function set_sender_name($sender_name) {
		$this->sender_name = $sender_name;
	}
	public function set_sender_email($sender_email) {
		$this->sender_email = $sender_email;
	}
	public function set_sender_comments($sender_comments) {
		$this->sender_comments = $sender_comments;
	}
	public function set_attachment($attachment) {
		$this->attachment = $attachment;
	}
	public function set_address($address) {
		$this->address = $address;
	}
	public function set_lon($lon) {
		$this->lon = $lon;
	}
	public function set_lat($lat) {
		$this->lat = $lat;
	}

	public function get_message() {
		return "Имя: ".$this->sender_name."<br>"."E-mail: ".$this->sender_email."<br>"."Адрес: ".$this->address->get_district().", ".$this->address->get_street_house()."<br>"."Комментарий: ".$this->sender_comments."<br>"."Координаты: "."<br>"."Широта ".$this->lat."<br>"."Долгота ".$this->lon;
	}

	public function rules() {
		return array(
			//required
			array('sender_name', 'required', 'message' => 'Необходимо заполнить поле имя'),
			array('sender_email', 'required', 'message' => 'Необходимо заполнить поле e-mail'),
			array('sender_comments', 'required', 'message' => 'Необходимо добавить комментарий'),
			//e-mail
			array('sender_email', 'email', 'message' => 'Неправльный адрес электронной почты.')
		);
	}
}