<?php
class UserRegistrationForm extends CFormModel{
  public $surname;
  public $name;
  public $lastname;
  public $login;
  public $password;
  public $retype_password;
  public $office;
  public $organization;
  public $email;
  public $captcha;

  public function get_surname(){
    return $this->surname;
  }
  public function get_name(){
    return $this->name;
  }
  public function get_lastname(){
    return $this->lastname;
  }
  public function get_login(){
    return $this->login;
  }
  public function get_password(){
    return $this->password;
  }
  public function get_retyped_password(){
  	return $this->retype_password;
  }
  public function get_office(){
    return $this->office;
  }
  public function get_organization(){
    return $this->organization;
  }
  public function get_email(){
    return $this->email;
  }
  public function get_captcha(){
    return $this->captcha;
  }

  public function set_surname($surname){
    $this->surname = $surname;
  }
  public function set_name($name){
    $this->name = $name;
  }
  public function set_lastname($lastname){
    $this->lastname = $lastname;
  }
  public function set_login($login){
    $this->login = $login;
  }
  public function set_password($password){
    $this->password = $password;
  }
  public function set_retyped_password($password){
    $this->retype_password = $password;
  }
  public function set_office($office){
    $this->office = $office;
  }
  public function set_organization($organization){
    $this->organization = $organization;
  }
  public function set_email($email){
    $this->email = $email;
  }
  public function set_captcha($captcha){
    $this->captcha = $captcha;
  }

  public function rules(){
  	return array(
      array('surname', 'required', 'message' => 'Необходимо заполнить поле «ФАМИЛИЯ».'),
      array('name', 'required', 'message' => 'Необходимо заполнить поле «ИМЯ».'),
      array('lastname', 'required', 'message' => 'Необходимо заполнить поле «ОТЧЕСТВО».'),
      array('login', 'required', 'message' => 'Необходимо заполнить поле «ЛОГИН».'),
      array('password', 'required', 'message' => 'Необходимо заполнить поле «ПАРОЛЬ».'),
      array('office', 'required', 'message' => 'Необходимо заполнить поле «ДОЛЖНОСТЬ».'),
      array('organization', 'required', 'message' => 'Необходимо заполнить поле «ОРГАНИЗАЦИЯ».'),
      array('email', 'required', 'message' => 'Необходимо заполнить поле «EMAIL».'),

      array('surname', 'length', 'max' => 200, 'message' => 'Фамилия слишком длинная (максимум 200 симв.).'),
      array('name', 'length', 'max' => 200, 'message' => 'Имя слишком длинное (максимум 200 симв.).'),
      array('lastname', 'length', 'max' => 200, 'message' => 'Отчество слишком длинное (максимум 200 симв.).'),
      array('login', 'length', 'max' => 200, 'message' => 'Логин слишком длинный (максимум 200 симв.).'),
      array('password', 'length', 'max' => 200, 'message' => 'Пароль слишком длинный (максимум 200 симв.).'),
      array('email', 'length', 'max' => 200, 'message' => 'Email слишком длинный (максимум 200 симв.).'),

      array('login', 'confirm_login'),

      array('retype_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают.'),

      array('email', 'email', 'message' => 'Неправльный адрес электронной почты.'),

      array('captcha', 'captcha', 'message' => 'Результат выражения с картинки неправильный.')
  		/*array('surname, name, lastname, login, password, retype_password, office, organization, email, captcha', 'required'),
      array('surname, name, lastname, login, password, email', 'length', 'max' => 200),
      array('login', 'confirm_login'),
      array('email', 'email'),
      array('captcha', 'captcha'),*/
  	);
  }
  public function confirm_login(){
    if(!UserManager::is_unique_username($this->login)){
      $this->addError('login', 'Такой логин уже существует.');
    }
  	/*if($this->surname == ""){
  		$this->addError('surname', 'Введите фамилию');
  	}*/
  }
}