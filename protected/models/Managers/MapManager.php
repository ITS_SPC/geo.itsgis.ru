<?php
class MapManager implements iMapManager{
	public static function get_available_maps($mapid){
		try{
			$connection = Yii::app()->db_geo;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select id, alias, name, zoom_levels, tiles_path, bound_lon_1, bound_lat_1, bound_lon_2, bound_lat_2, min_resolution, max_resolution, custom_map_center_lon, custom_map_center_lat From map Where id != :mapid AND available = 1 ORDER BY name";
				$command = $connection->createCommand($sql);
				$command->bindParam(":mapid", $mapid,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$maps = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$map = new Map;
					$map->set_id($row['id']);
					$map->set_alias($row['alias']);
					$map->set_name($row['name']);
					$map->set_zoom_levels($row['zoom_levels']);
					$map->set_tiles_path($row['tiles_path']);
					$map->set_bound_lon_1($row['bound_lon_1']);
					$map->set_bound_lat_1($row['bound_lat_1']);
					$map->set_bound_lon_2($row['bound_lon_2']);
					$map->set_bound_lat_2($row['bound_lat_2']);
					$map->set_min_resolution($row['min_resolution']);
					$map->set_max_resolution($row['max_resolution']);
					$map->set_custom_map_center_lon($row['custom_map_center_lon']);
					$map->set_custom_map_center_lat($row['custom_map_center_lat']);
					array_push($maps, $map);
				}
			}
			return $maps;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_map_by_id($mapid){
		try{
			$connection = Yii::app()->db_geo;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select id as map_id, alias, name, zoom_levels, tiles_path, bound_lon_1, bound_lat_1, bound_lon_2, bound_lat_2, min_resolution, max_resolution, custom_map_center_lon, custom_map_center_lat From map Where id = :mapid";
				$command = $connection->createCommand($sql);
				$command->bindParam(":mapid",$mapid,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$map = new Map;
			if(isset($row)){
				$map->set_id($row['map_id']);
				$map->set_alias($row['alias']);
				$map->set_name($row['name']);
				$map->set_zoom_levels($row['zoom_levels']);
				$map->set_tiles_path($row['tiles_path']);
				$map->set_bound_lon_1($row['bound_lon_1']);
				$map->set_bound_lat_1($row['bound_lat_1']);
				$map->set_bound_lon_2($row['bound_lon_2']);
				$map->set_bound_lat_2($row['bound_lat_2']);
				$map->set_min_resolution($row['min_resolution']);
				$map->set_max_resolution($row['max_resolution']);
				$map->set_custom_map_center_lon($row['custom_map_center_lon']);
				$map->set_custom_map_center_lat($row['custom_map_center_lat']);
			}
			return $map;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_layer_object_by_coordinates($switched_layer, $map_id,$clickPositionLon, $clickPositionLat){
		$layer_alias = "";
		$object = null;
		$layer_alias = MapManager::get_layer_name($switched_layer);
		$layer_alias = mb_strtolower($layer_alias,'UTF-8');
		switch ($layer_alias) {
			case 'закрепленные территории':{
				$object = TiedTerritoryManager::get_tied_territory($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'освещение':{
				$object = LightPostManager::get_light_post($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'дорожная разметка':{
				$object = RoadMarkingManager::get_road_marking($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'ограждения':{
				$object = BarricadoManager::get_barricado($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'опоры':{
				$object = SignPostManager::get_post($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'остановки':{
				$object = BusstopManager::get_busstop($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			case 'реклама':{
				$object = AdvertisementManager::get_advertisement($switched_layer, $clickPositionLon, $clickPositionLat);
			}break;
			default:{
				$object = TargetedPlanManager::get_targeted_plan_object($map_id, $clickPositionLon, $clickPositionLat);
			}break;
		}
		return $object;
	}
	public static function get_layer_name($layer_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select lo.alias as layer_alias From layerobject lo Where lo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$layer_alias = "";
			if(isset($row)){
				$layer_alias = $row['layer_alias'];
			}
			return $layer_alias;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_user_maps($user_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT mo.id as map_id, mo.alias as map_alias From user_georole ug Join pu_georole pug On ug.georole_id = pug.id Join pu_georole_map pugl On pug.id = pugl.id Join mapobject mo On pugl.mapid = mo.id Where ug.user_id = :user_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":user_id", $user_id, PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$maps = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$map = new Map;
					$map->set_id($row['map_id']);
					$map->set_alias($row['map_alias']);
					array_push($maps, $map);
				}
			}
	 		return $maps;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
	}
}