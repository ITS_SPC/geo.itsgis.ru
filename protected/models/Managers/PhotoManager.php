<?php
class PhotoManager implements iPhotoManager{
	public static function get_photoes($object_id, $object_type){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT id, photo_date, photo_type, photo_order, photo_description FROM photo WHERE photoable_id = :object_id AND photo_type = :object_type ORDER BY photo_order";
				$command = $connection->createCommand($sql);
				$command->bindParam(":object_id",$object_id,PDO::PARAM_STR);
				$command->bindParam(":object_type",$object_type,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$photoes = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$photo = new Photo;
					$photo->set_id($row['id']);
					$photo->set_photo_date($row['photo_date']);
					$photo->set_photo_type($row['photo_type']);
					$photo->set_photo_order($row['photo_order']);
					$photo->set_photo_description($row['photo_description']);
					$photo->set_photo_link($row['id'], $object_type);
					array_push($photoes, $photo);
				}
			}
			return $photoes;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}