<?php
class AdvertisementManager implements iAdvertisementManager{
	public static function get_advertisement($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT adv.id as id, adv.install_date as install_date, adv.uninstall_date as uninstall_date, adv.number as number, adv.angle as angle, adv.is_valid as is_valid, adv.address_id as address_id, st_astext(fo.geometry) as adv_geometry, adv_t.name as type_name, adv_t.id as type_id, adv_t.sides as type_sides, adv_t.places as type_places FROM  featureobject fo Join adv_advertisement adv On fo.id = adv.feature_object_id LEFT OUTER JOIN adv_type as adv_t ON adv.type_id = adv_t.id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$advertisement = null;
			if(isset($row) && $row['id'] != null){
				$advertisement = new Advertisement;
				$advertisement->set_id($row['id']);
				$advertisement->set_install_date($row['install_date']);
				$advertisement->set_uninstall_date($row['uninstall_date']);
				$advertisement->set_cadastral_number($row['number']);
				$advertisement->set_angle($row['angle']);
				$advertisement->set_valid($row['is_valid']);
				$advertisement->set_photos(PhotoManager::get_photoes($row['id'], "Advertisement"));

				if(isset($row['type_id']) && ($row['type_id'] != null)){
					$advertisement_type = new AdvertisementType;
					$advertisement_type->set_id($row['type_id']);
					$advertisement_type->set_type_name($row['type_name']);
					$advertisement_type->set_sides_number($row['type_sides']);
					$advertisement_type->set_canvas_number($row['type_places']);
					$advertisement_type->set_parameters(AdvertisementManager::get_advertisement_type_parameters($row['type_id']));
					
					$advertisement->set_type($advertisement_type);
				}

				if(isset($row['address_id']) && ($row['address_id'] != null)){
					$advertisement->set_address(AddressManager::get_address($row['address_id']));
				}

				$advertisement->set_sides(AdvertisementManager::get_advertisement_sides($row['id']));

				$advertisement->add_geometry($row['adv_geometry']);
				$advertisement->set_object_type("advertisement");
			}
			return $advertisement;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_advertisement_sides($advertisement_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT adv_s.id as side_id, adv_p.id as place_id, adv_r.id as rent_id, adv_r.organization_id as organization_id, adv_r.rent_start as rent_start, adv_r.rent_end as rent_end, adv_r.contact as contact, adv_r.isperson as is_person FROM adv_side adv_s LEFT OUTER JOIN adv_place adv_p ON adv_s.id = adv_p.side_id LEFT OUTER JOIN adv_rent adv_r ON adv_p.id = adv_r.place_id Where adv_s.advertisement_id = :advertisement_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":advertisement_id",$advertisement_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$sides = array();
			if(isset($rows)){
				foreach($rows as $row){
					$exists = AdvertisementManager::is_exist($sides, $row);

					//prepare side canvas
					$advertisement_canvas = new AdvertisementCanvas;
					$advertisement_canvas->set_id($row['place_id']);
					$advertisement_canvas->set_rent_start($row['rent_start']);
					$advertisement_canvas->set_rent_end($row['rent_end']);
					$advertisement_canvas->set_contact($row['contact']);
					$advertisement_canvas->set_is_person($row['is_person']);
					$advertisement_canvas->set_organizations(AdvertisementManager::get_advertisement_canvas_rent_organizations($row['place_id']));

					//check for existing side with canvas
					if(!$exists[0]){
						//create new side with canvas
						$advertisement_side = new AdvertisementSide;
						$advertisement_side->set_id($row['side_id']);

						$advertisement_side->add_canvas($advertisement_canvas);

						array_push($sides, $advertisement_side);
					}
					else{
						//add canvas to existing side
						$sides[$exists[1]]->add_canvas($advertisement_canvas);
					}
				}
			}
			return $sides;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function is_exist($sides, $side){
		$exists = array(false, null);
		if(count($sides) != 0){
			for ($i = 0, $maxi = count($sides); $i < $maxi; $i++) {
				if($sides[$i]->get_id() == $side['side_id']){
					$exists[0] = true;
					$side_canvases = $sides[$i]->get_canvases();
					$exists[1] = $i;
					for($j = 0, $maxj = count($side_canvases); $j < $maxj; $j++){
						if($side_canvases[$j]->get_id() == $side['place_id']){
							$exists[1] = null;
							$j = $maxj;
						}
					}
					$i = $maxi;
				}
			}
		}
		return $exists;
	}
	public static function get_advertisement_canvas_rent_organizations($canvas_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT adv_r.organization_id as organization_id FROM adv_rent adv_r WHERE adv_r.place_id = :canvas_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":canvas_id",$canvas_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$organizations = array();
			if(isset($rows) && (count($rows) != 0)){
				foreach ($rows as $row) {
					$organization = OrganizationManager::get_organization($row['organization_id']);
					array_push($organizations, $organization);
				}
			}
			return $organizations;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_advertisement_type_parameters($type_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT adv_p.name as name, adv_v.value as value FROM adv_parameter adv_p JOIN adv_value adv_v ON adv_p.id = adv_v.parameter_id WHERE adv_v.type_id = :type_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":type_id", $type_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$parameters = array();
			if(isset($rows) && (count($rows) != 0)){
				foreach ($rows as $row) {
					if(($row['name'] != null) && ($row['value'])){
						array_push($parameters, array($row['name'] => $row['value']));
					}
				}
			}
			return $parameters;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}