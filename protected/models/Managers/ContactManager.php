<?php
class ContactManager implements iContactManager{
	public static function get_contact($organization_id, $contact_type){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				switch ($contact_type) {
					case '':{
						$sql = "SELECT ic.information as contact_information, ic.contact_type as contact_type FROM info_contact ic Where ic.organization_id = :organization_id";
						$command = $connection->createCommand($sql);
						$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
						$command->execute();
					}break;		
					default:{
						$sql = "SELECT ic.information as contact_information, ic.contact_type as contact_type FROM info_contact ic Where ic.organization_id = :organization_id AND ic.contact_type = :contact_type";
						$command = $connection->createCommand($sql);
						$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
						$command->bindParam(":contact_type",$contact_type,PDO::PARAM_STR);
						$command->execute();
					}break;
				}
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$contact = new Contact;
			if(isset($rows)){
				foreach ($rows as $row){
					switch ($row['contact_type']) {
						case 'Phone':{
							$contact->add_phone($row['contact_information']);
						}break;
						case 'Site':{
							$contact->add_site($row['contact_information']);
						}break;
						case 'Email':{
							$contact->add_email($row['contact_information']);
						}break;
						case 'Fax':{
							$contact->add_fax($row['contact_information']);
						}break;
						case 'PhoneMobile':{
							$contact->add_mobile_phone($row['contact_information']);
						}break;
						case 'ICQ':{
							$contact->add_icq($row['contact_information']);
						}break;
					}
				}
			}
			return $contact;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}