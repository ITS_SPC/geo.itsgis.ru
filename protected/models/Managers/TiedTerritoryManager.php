<?php
class TiedTerritoryManager implements iTiedTerritoryManager{
	//const SOMECONSTANT = 'Здравствуй, мир.';
	public static function get_tied_territory($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT tt.id as tt_id, tt.territory_name as tt_name, tt.square as tt_square, st_astext(fo.geometry) as tt_geometry FROM  featureobject fo Join tt_territory tt On fo.id = tt.feature_object_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$tied_territory = null;
			//$tied_territory = new TiedTerritory;;
			if(isset($row) && $row['tt_id'] != null){
				$tied_territory = new TiedTerritory;
				$tied_territory->set_id($row['tt_id']);
				$tied_territory->set_territory_name($row['tt_name']);
				$tied_territory->set_square($row['tt_square']);
				$tied_territory->set_tied_territory_contracts(TiedTerritoryManager::get_tied_territory_contracts($row['tt_id']));
				$tied_territory->add_geometry($row['tt_geometry']);
				$tied_territory->set_object_type("tied_territory");
				//$tied_territory->trait_string = self::SOMECONSTANT;
			}
			return $tied_territory;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_tied_territory_contracts($tt_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT DISTINCT ttc.id as contract_id, ttc.contract_number as contract_number, ttc.contract_date as contract_date, ttc.square as square, ttc.begin_date as begin_date, ttc.end_date as end_date, ttc.organization_id as organization_id, ttc.address_id as address_id FROM tt_territory ttr JOIN tt_contract_territory ttct ON ttr.id = ttct.territory_id JOIN tt_contract ttc ON ttct.contract_id = ttc.id Where ttct.territory_id = :tt_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":tt_id",$tt_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$tied_territory_contracts = array();
			if(isset($rows)){
				foreach($rows as $row){
					$tied_territory_contract = new TiedTerritoryContract;
					$tied_territory_contract->set_id($row['contract_id']);
					$tied_territory_contract->set_contract_number($row['contract_number']);
					$tied_territory_contract->set_contract_date($row['contract_date']);
					$tied_territory_contract->set_square($row['square']);
					$tied_territory_contract->set_begin_date($row['begin_date']);
					$tied_territory_contract->set_end_date($row['end_date']);
					$tied_territory_contract->set_organization(OrganizationManager::get_organization($row['organization_id']));
					$tied_territory_contract->set_address(AddressManager::get_address($row['address_id']));
					$tied_territory_contract->set_photoes(PhotoManager::get_photoes($row['contract_id'], "TiedTerritory"));
					array_push($tied_territory_contracts, $tied_territory_contract);
				}
			}
			return $tied_territory_contracts;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}