<?php
class SignPostManager implements iSignPostManager{
	public static function get_post($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT p.post_id as id, p.posttype as post_type, p.quality as quality, p.angle as angle, p.address as address, p.lifetime as life_time, p.installdate as install_date, p.owner_id as owner_id,st_astext(fo.geometry) as post_geometry FROM  featureobject fo Join post p On fo.id = p.featureobject_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$post = null;
			if(isset($row) && $row['id'] != null){
				$post = new SignPost;
				$post->set_id($row['id']);
				$post->set_quality(ObjectsDictionary::translate("sign_post", $row['quality']));
				$post->set_post_type(ObjectsDictionary::translate("sign_post", $row['post_type']));
				$post->set_install_date($row['install_date']);
				$post->set_life_time($row['life_time']);
				$post->set_owner(OrganizationManager::get_organization($row['owner_id']));
				$post->set_angle($row['angle']);
				$post->set_address($row['address']);
				$post->set_post_groupings(SignPostManager::get_post_groupings($row['id']));
				$post->add_geometry($row['post_geometry']);
				$post->set_object_type("post");
			}
			return $post;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_post_groupings($post_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT pg.id as id, pg.number as number FROM postgrouping pg Where pg.postid = :post_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":post_id",$post_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$post_groupings = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$post_grouping = new PostGrouping;
					$post_grouping->set_id($row['id']);
					$post_grouping->set_group_number($row['number']);
					$post_grouping->set_post_objects(SignPostManager::get_post_grouping_objects($row['id']));
					array_push($post_groupings, $post_grouping);
				}
			}
			return $post_groupings;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_post_grouping_objects($grouping_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT post.id as id, post.orderingrouping as order_in_grouping, post.installdate as install_date FROM postable post Where post.grouping_id = :grouping_id ORDER BY post.orderingrouping";
				$command = $connection->createCommand($sql);
				$command->bindParam(":grouping_id",$grouping_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$post_grouping_objects = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$post_grouping_object = new PostObject;
					$post_grouping_object->set_id($row['id']);
					$post_grouping_object->set_post_order($row['order_in_grouping']);
					$post_grouping_object->set_install_date($row['install_date']);
					//array_push($post_grouping_objects, $post_grouping_object);
					$post_object = SignPostManager::get_sign($post_grouping_object);
					if($post_object == null){
						$post_object = SignPostManager::get_light($post_grouping_object);
						array_push($post_grouping_objects, $post_object);
					}
					else{
						array_push($post_grouping_objects, $post_object);
					}
				}
			}
			return $post_grouping_objects;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_light($post_grouping_object){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT li.id as id, li.construction_variant_id as construction_variant_id, li.signalization as signalization, li.role as role, li.is_light_diode as is_light_diode, li.numeric_display as numeric_display, li.inspectiondate as inspection_date, li.uninstalldate as uninstall_date, li.note as note, li.light_state as light_state, li.is_active as is_active FROM lights li Where li.id = :post_object_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":post_object_id",$post_grouping_object->get_id(),PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$light = null;
			if(isset($row) && ($row['id'] != null)){
				$light = new TrafficLight;
				$light->set_id($row['id']);
				$light->set_signalization($row['signalization']);
				$light->set_is_light_diode($row['is_light_diode']);
				$light->set_numeric_display($row['numeric_display']);
				$light->set_inspection_date($row['inspection_date']);
				$light->set_uninstall_date($row['uninstall_date']);
				$light->set_light_state(ObjectsDictionary::translate("traffic_light", $row['light_state']));
				$light->set_note($row['note']);
				$light->set_is_active($row['is_active']);
				$light->set_role(ObjectsDictionary::translate("traffic_light", $row['role']));
				$light->set_photoes(PhotoManager::get_photoes($row['id'], "Postable"));
				$light->set_sign_picture(SignPostManager::checkImageUrl("light", $row['id']));
				$light->set_construct(SignPostManager::get_light_construct($row['construction_variant_id']));

				$light->set_post_order($post_grouping_object->get_post_order());
				$light->set_post_object_type('traffic_light');
				$light->set_install_date($post_grouping_object->get_install_date());
			}
			return $light;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	/*TODO: implement method*/
	public static function get_light_construct($construct_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT lc.id as id, lc.variant_name as variant_name, lc.red_first as red_first, lc.yellow_first as yellow_first, lc.green_first as green_first, lc.green_additional as green_additional, lc.white_moon as white_moon, lc.red_second as red_second, lc.yellow_second as yellow_second, lc.green_second as green_second, lt.index_ru as type_name FROM lights_construct_variant lc JOIN lights_type lt ON lc.light_type_id = lt.id Where lc.id = :construct_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":construct_id",$construct_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$light_construct = null;
			if(isset($row) && ($row['id'] != null)){
				$light_construct = new TrafficLightConstruct;
				$light_construct->set_id($row['id']);
				$light_construct->set_variant_name($row['variant_name']);
				$light_construct->set_red_first($row['red_first']);
				$light_construct->set_yellow_first($row['yellow_first']);
				$light_construct->set_green_first($row['green_first']);
				$light_construct->set_green_additional($row['green_additional']);
				$light_construct->set_white_moon($row['white_moon']);
				$light_construct->set_red_second($row['red_second']);
				$light_construct->set_yellow_second($row['yellow_second']);
				$light_construct->set_green_second($row['green_second']);
				$light_construct->set_type_name($row['type_name']);
			}
			return $light_construct;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_sign($post_grouping_object){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT si.id as id, si.isgost as is_gost, si.signstate as sign_state, si.uninstalldate as uninstall_date, si.inspectiondate as inspection_date, si.note as note, si.streetside as street_side, si.skintype as skin_type, si.diode as diode, si.signtype as sign_type_id, si.size_id as size_id, si.manufacturer as manufacturer FROM sign si Where si.id = :post_object_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":post_object_id",$post_grouping_object->get_id(),PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$sign = null;
			if(isset($row) && ($row['id']!= null)){
				$sign = new Sign;
				$sign->set_id($row['id']);
				$sign->set_is_gost($row['is_gost']);
				$sign->set_sign_state(ObjectsDictionary::translate("sign", $row['sign_state']));
				$sign->set_uninstall_date($row['uninstall_date']);
				$sign->set_inspection_date($row['inspection_date']);
				$sign->set_note($row['note']);
				$sign->set_street_side($row['street_side']);
				$sign->set_skin_type($row['skin_type']);
				$sign->set_is_diode($row['diode']);
				$sign->set_sign_type(SignPostManager::get_sign_type($row['sign_type_id']));
				$sign->set_sign_size(SignPostManager::get_sign_size($row['size_id']));
				$sign->set_organization(OrganizationManager::get_organization($row['manufacturer']));
				$sign->set_photoes(PhotoManager::get_photoes($row['id'], "Postable"));
				$sign->set_sign_picture(SignPostManager::checkImageUrl("sign", $row['id']));

				$sign->set_post_order($post_grouping_object->get_post_order());
				$sign->set_post_object_type('sign');
				$sign->set_install_date($post_grouping_object->get_install_date());
			}
			return $sign;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_sign_type($sign_type_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT st.id as id, st.name as name, st.signrule as sign_rule, st.gostnumber as gost_number, st.format as format, st.isspecial as isspecial, st.danger as danger FROM signtype st WHERE st.id = :sign_type_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":sign_type_id",$sign_type_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$sign_type = new SignType;
			if(isset($row)){
				$sign_type->set_id($row['id']);
				$sign_type->set_name($row['name']);
				$sign_type->set_gost_number($row['gost_number']);
				$sign_type->set_sign_rule($row['sign_rule']);
				$sign_type->set_format($row['format']);
				$sign_type->set_isspecial($row['isspecial']);
				$sign_type->set_danger($row['danger']);
			}
			return $sign_type;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public function get_sign_size($size_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT ss.id as id, ss.size_type as size_type, ss.width as width, ss.height as height FROM signsize ss WHERE ss.id = :size_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":size_id",$size_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$sign_size = new SignSize;
			if(isset($row)){
				$sign_size->set_id($row['id']);
				$sign_size->set_size_type($row['size_type']);
				$sign_size->set_width($row['width']);
				$sign_size->set_height($row['height']);
			}
			return $sign_size;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function checkImageUrl($post_object_type, $post_object_id){
		$headers = get_headers("http://app.itsgis.ru:8732/?".$post_object_type."=".$post_object_id);
		if(preg_match("|200|", $headers[0])) {
			return "http://app.itsgis.ru:8732/?".$post_object_type."=".$post_object_id;
		} else {
			return "";
		}
	}
}