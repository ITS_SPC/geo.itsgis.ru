<?php
class LayerManager implements iLayerManager{
	public static function get_plugin_layers($mapid){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select lo.layerid as layer_id, lo.alias as alias From layerobject lo Where lo.mapid = :mapid AND lo.is_plugin_layer = true Order by lo.order_index";
				$command = $connection->createCommand($sql);
				$command->bindParam(":mapid", $mapid, PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$layers = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$layer = new Layer;
					$layer->set_id($row['layer_id']);
					$layer->set_alias($row['alias']);
					array_push($layers, $layer);
				}
			}
	 		return $layers;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
	}
	public static function get_user_available_layers($user_id, $mapid){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select lo.layerid as layer_id, lo.alias as alias From user_georole ug Join pu_georole pug On ug.georole_id = pug.id Join pu_georole_layer pugl On pug.id = pugl.id Join layerobject lo On pugl.layerid = lo.layerid Where ug.user_id = :user_id AND lo.mapid = :mapid AND lo.is_plugin_layer = true Order by lo.order_index";
				$command = $connection->createCommand($sql);
				$command->bindParam(":user_id", $user_id, PDO::PARAM_STR);
				$command->bindParam(":mapid", $mapid, PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$layers = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$layer = new Layer;
					$layer->set_id($row['layer_id']);
					$layer->set_alias($row['alias']);
					array_push($layers, $layer);
				}
			}
	 		return $layers;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
	}
	/*public static function get_users(){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select id, pu_username, pu_password, pu_metadata, pu_enabled From pu_user";
				$command = $connection->createCommand($sql);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$users = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$user = new User;
					$user->set_id($row['id']);
					$user->set_user_name($row['pu_username']);
					$user->set_user_password($row['pu_password']);
					$user->set_user_metadata($row['pu_metadata']);
					$user->set_user_enabled($row['pu_enabled']);
					array_push($users, $user);
				}
			}
	 		return $users;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
  	}*/
}
