<?php
class BusstopManager implements iBusstopManager{
	public static function get_busstop($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT b.id as id, b.bus_stop_name as busstop_name, b.is_waiting_area_present as landing_field, b.is_road_pocket_present as pocket, b.is_pavilion_present as pavilion, b.road_side as road_side, b.bus_stop_owner as owner_id, b.address_id as address_id, b.is_speed_change_rows_present as is_speed_change_rows_present, b.speed_up_row_norm as speed_up_row_norm, b.speed_down_row_norm as speed_down_row_norm, b.speed_up_row_fact as speed_up_row_fact, b.speed_down_row_fact as speed_down_row_fact, st_astext(fo.geometry) as busstop_geometry FROM  featureobject fo Join pubtr_busstop b On fo.id = b.feature_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$busstop = null;
			if(isset($row) && $row['id'] != null){
				$busstop = new Busstop;
				$busstop->set_id($row['id']);
				$busstop->set_name($row['busstop_name']);
				$busstop->set_landing_field($row['landing_field']);
				$busstop->set_pocket($row['pocket']);
				$busstop->set_pavilion($row['pavilion']);
				$busstop->set_road_side($row['road_side']);
				$busstop->set_has_speed_change_rows($row['is_speed_change_rows_present']);
				$busstop->set_norm_speed_up_lane_length($row['speed_up_row_norm']);
				$busstop->set_norm_speed_down_lane_length($row['speed_down_row_norm']);
				$busstop->set_fact_speed_up_lane_length($row['speed_up_row_fact']);
				$busstop->set_fact_speed_down_lane_length($row['speed_down_row_fact']);
				$busstop->set_routes(BusstopManager::get_busstop_routes($row['id']));
				$busstop->set_owner(OrganizationManager::get_organization($row['owner_id']));
				$busstop->set_address(AddressManager::get_address($row['address_id']));
				$busstop->set_photoes(PhotoManager::get_photoes($row['id'], "Busstop"));
				$busstop->add_geometry($row['busstop_geometry']);
				$busstop->set_object_type("busstop");
			}
			return $busstop;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_busstop_routes($busstop_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT r.id as id, r.route_name as route_name, r.transport_type as transport_type FROM pubtr_busstop pb JOIN pubtr_busstop_route_link pbrl ON pb.id = pbrl.bus_stops_id JOIN pubtr_route r ON pbrl.routes_id = r.id WHERE pbrl.bus_stops_id = :busstop_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":busstop_id", $busstop_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$routes = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$route = new Route;
					$route->set_id($row['id']);
					switch ($row['transport_type']) {
						case 'Tram':{
							$route->set_transport_type("Трамвай");
						}break;
						case 'Bus':{
							$route->set_transport_type("Автобус");
						}break;
						case 'Trolleybus':{
							$route->set_transport_type("Троллейбус");
						}break;
						default:{
							$route->set_transport_type($row['transport_type']);
						}break;
					}
					$route->set_route_name($row['route_name']);
					array_push($routes, $route);
				}
			}
			return $routes;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}