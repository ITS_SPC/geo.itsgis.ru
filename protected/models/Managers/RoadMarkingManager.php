<?php
class RoadMarkingManager implements iRoadMarkingManager{
	public static function get_road_marking($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT rm.id as id, rm.type_id as type_id, rm.material_id as material_id, rm.quality as quality, rm.brightness as brightness, rm.lrnightdry as lrnightdry, rm.lrnightwet as lrnightwet, rm.lrday as lrday, rm.drawing_date as drawing_date, st_astext(fo.geometry) as rm_geometry FROM  featureobject fo Join roadmarking_line rm On fo.id = rm.feature_object_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$road_marking = null;
			if(isset($row) && $row['id'] != null){
				$road_marking = new RoadMarking;
				$road_marking->set_id($row['id']);
				$road_marking->set_quality($row['quality']);
				$road_marking->set_brightness($row['brightness']);
				$road_marking->set_lrnightdry($row['lrnightdry']);
				$road_marking->set_lrnightwet($row['lrnightwet']);
				$road_marking->set_lrday($row['lrday']);
				$road_marking->set_drawing_date($row['drawing_date']);
				$road_marking->add_geometry($row['rm_geometry']);
				$road_marking->set_object_type('road_marking');

				$road_marking->set_road_marking_material(RoadMarkingManager::get_road_marking_material($row['material_id']));
				$road_marking->set_road_marking_type(RoadMarkingManager::get_road_marking_type($row['type_id']));
			}
			return $road_marking;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_road_marking_material($material_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT id, material_name, is_reflective FROM roadmarking_material Where id = :material_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":material_id",$material_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$road_marking_material = new RoadMarkingMaterial;
			if(isset($row)){
				$road_marking_material->set_id($row['id']);
				$road_marking_material->set_material_name($row['material_name']);
				$road_marking_material->set_is_reflective($row['is_reflective']);
			}
			return $road_marking_material;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_road_marking_type($type_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT id, type_number, width, mult, l FROM roadmarking_type Where id = :type_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":type_id",$type_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$road_marking_type = new RoadMarkingType;
			if(isset($row)){
				$road_marking_type->set_id($row['id']);
				$road_marking_type->set_type_number($row['type_number']);
				$road_marking_type->set_width($row['width']);
				$road_marking_type->set_mult($row['mult']);
				$road_marking_type->set_stroke_length($row['l']);
			}
			return $road_marking_type;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}