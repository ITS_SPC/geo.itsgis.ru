<?php
class UserManager implements iUserManager{
	public static function is_unique_username($username){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select COUNT(id) as user_counter From pu_user Where pu_username = :username";
				$command = $connection->createCommand($sql);
				$command->bindParam(":username",$username,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$is_unique = true;
			if(isset($row)){
				if($row['user_counter'] != 0){
					$is_unique = false;
				}
			}
	 		return $is_unique;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
	}
	public static function get_users(){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select id, pu_username, pu_password, pu_metadata, pu_enabled From pu_user";
				$command = $connection->createCommand($sql);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$users = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$user = new User;
					$user->set_id($row['id']);
					$user->set_user_name($row['pu_username']);
					$user->set_user_password($row['pu_password']);
					$user->set_user_metadata($row['pu_metadata']);
					$user->set_user_enabled($row['pu_enabled']);
					array_push($users, $user);
				}
			}
	 		return $users;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception...";	
        }
  	}
  	public static function get_user($username){
  		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select id, pu_username, pu_password, pu_metadata, pu_enabled, pu_create_time, pu_expired_time From pu_user Where pu_username = :username";
				$command = $connection->createCommand($sql);
				$command->bindParam(":username",$username,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$user = new User;
			if(isset($row)){
				$user = new User;
				$user->set_id($row['id']);
				$user->set_user_name($row['pu_username']);
				$user->set_user_password($row['pu_password']);
				$user->set_user_metadata($row['pu_metadata']);
				$user->set_user_enabled($row['pu_enabled']);
				$user->set_user_create_time($row['pu_create_time']);
				$user->set_user_expired_time($row['pu_expired_time']);
			}
	 		return $user;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
        	echo "Internal exception get_user...";	
        }
  	}
}
