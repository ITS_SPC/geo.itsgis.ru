<?php
class LightPostManager implements iLightPostManager{
	public static function get_light_post($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT lp.id as lp_id, lp.post_material as lp_material, lp.post_type as lp_post_type, st_astext(fo.geometry) as lp_geometry FROM  featureobject fo Join cable_post lp On fo.id = lp.feature_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0)";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$light_post = null;
			if(isset($row) && $row['lp_id'] != null){
				$light_post = new LightPost;
				$light_post->set_id($row['lp_id']);
				switch ($row['lp_material']) {
					case 'Metal':{
						$light_post->set_post_material('Металл');
					}break;
					case 'Concrete':{
						$light_post->set_post_material('Бетон');
					}break;
					default:{
						$light_post->set_post_material($row['lp_material']);
					}break;
				}
				switch ($row['lp_post_type']) {
					case 'Console':{
						$light_post->set_post_type('Консольный тип');
					}break;
					default:{
						$light_post->set_post_type($row['lp_post_type']);
					}break;
				}
				$light_post->set_photoes(PhotoManager::get_photoes($row['lp_id'], "Cablenetwork"));
				$light_post->add_geometry($row['lp_geometry']);
				$light_post->set_object_type("cable_post");
				$light_post->set_light_post_grouping(LightPostManager::get_ligth_post_grouping($row['lp_id']));
			}
			return $light_post;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_ligth_post_grouping($light_post_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT cpg.id as cpg_id, cpg.height as cpg_height, cpg.radius as cpg_radius, cpg.angle as cpg_angle FROM cable_post cp JOIN cable_post_grouping cpg ON cp.id=cpg.post_id Where cpg.post_id = :light_post_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":light_post_id",$light_post_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$light_post_groupings = array();
			if(isset($rows)){
				foreach($rows as $row){
					$light_post_grouping = new LightPostGrouping;
					$light_post_grouping->set_id($row['cpg_id']);
					$light_post_grouping->set_height($row['cpg_height']);
					$light_post_grouping->set_radius($row['cpg_radius']);
					$light_post_grouping->set_angle($row['cpg_angle']);
					$light_post_grouping->set_grouping_lamps(LightPostManager::get_light_post_grouping_lamps($row['cpg_id']));
					array_push($light_post_groupings, $light_post_grouping);
				}
			}
			return $light_post_groupings;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_light_post_grouping_lamps($light_post_grouping){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT cl.id as cl_id, cl.angle as cl_angle, cl.lamp_type_id as cl_lamp_type_id FROM cable_post_grouping cpg JOIN cable_lamp cl ON cpg.id=cl.group_id Where cl.group_id = :light_post_grouping";
				$command = $connection->createCommand($sql);
				$command->bindParam(":light_post_grouping",$light_post_grouping,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$light_post_grouping_lamps = array();
			if(isset($rows)){
				foreach($rows as $row){
					$light_post_lamp = new LightPostLamp;
					$light_post_lamp->set_id($row['cl_id']);
					$light_post_lamp->set_angle($row['cl_angle']);
					$light_post_lamp->set_lamp_type(LightPostManager::get_lamp_type($row['cl_lamp_type_id']));
					array_push($light_post_grouping_lamps, $light_post_lamp);
				}
			}
			return $light_post_grouping_lamps;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_lamp_type($lamp_type_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT id, type_name, type_power, type_flow FROM cable_lamp_type Where id = :lamp_type_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":lamp_type_id",$lamp_type_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				Yii::log('error', CLogger::LEVEL_ERROR);
				$transaction->rollback();
			}
			$lamp_type = new LampType;
			if(isset($row)){
				$lamp_type->set_id($row['id']);
				$lamp_type->set_name($row['type_name']);
				$lamp_type->set_power($row['type_power']);
				$lamp_type->set_light_flow($row['type_flow']);
			}
			return $lamp_type;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}