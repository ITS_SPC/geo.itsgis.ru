<?php
class AddressManager implements iAddressManager{
	public static function get_address($address_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT adr.id as address_id, adr.streethouse as streethouse, adr.district_id as district_id, adr.addresstype as address_type FROM addresses adr Where adr.id = :address_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":address_id",$address_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$address = new Address;
			if(isset($row)){
				$address->set_id($row['address_id']);
				$address->set_street_house($row['streethouse']);
				$address->set_address_type($row['address_type']);
				$address->set_district(AddressManager::get_district($row['district_id']));
			}
			return $address;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_district($district_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT dstr.name as district_name FROM districts dstr Where dstr.id = :district_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":district_id",$district_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$district = "";
			if(isset($row)){
				$district = $row['district_name'];
			}
			return $district;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_street_geometries($address_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "Select st_astext(fo.geometry) as geometry From featureobject fo Join address_featureobject adf On fo.id = adf.feature_id Join addresses adr On adf.address_id = adr.id Where adr.id = :address_id and adr.addresstype = 'Street'";
				$command = $connection->createCommand($sql);
				$command->bindParam(":address_id",$address_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$streets_geometry = array();
			if(isset($rows) && (count($rows) != 0)){
				foreach($rows as $row){
					array_push($streets_geometry, $row['geometry']);	
				}
			}
			return $streets_geometry;
		}
		catch(Exception $e){
			echo "Db connection error...";
		}
	}
	public static function get_street_name($address_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT str.name as street_name FROM addresses adr Join streets str ON adr.street_id = str.id Where adr.id = :address_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":address_id",$address_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$street_name = "";
			if(isset($row)){
				$street_name = $row['street_name'];
			}
			return $street_name;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_crossroad_name($address_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT crs.crossroadname as crossroad_name FROM addresses adr Join crossroads crs ON adr.crossroad_id = crs.id Where adr.id = :address_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":address_id",$address_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$crossroad_name = "";
			if(isset($row)){
				$crossroad_name = $row['crossroad_name'];
			}
			return $crossroad_name;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}