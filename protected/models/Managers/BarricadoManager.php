<?php
class BarricadoManager implements iBarricadoManager{
	public static function get_barricado($layer_id, $clickPositionLon, $clickPositionLat){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$point = "POINT(".$clickPositionLon." ".$clickPositionLat.")";
				$sql = "SELECT bar.id as id, bar.brc_condition as condition, bar.brc_road_side as road_side, bar.brc_reason_to_install as reason_to_install, bar.brc_length as length, bar.brc_height as height, bar.brc_bracket_step as bracket_step, bar.brc_bracket_count as bracket_count, bar.brc_responsible_organization as responsible_organization_id, bar.brc_status as status, bar.brc_address_id as address_id, bar.brc_install_date as install_date, bar.brc_update_date as update_date, st_astext(fo.geometry) as bar_geometry FROM  featureobject fo Join barricado_photoable bar On fo.id = bar.brc_featureobject_id Where st_dwithin(fo.geometry, st_GeomFromText(:point, 4326), 2.0) AND fo.layerid = :layer_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":point",$point,PDO::PARAM_STR);
				$command->bindParam(":layer_id",$layer_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$barricado = null;
			if(isset($row) && $row['id'] != null){
				$barricado = new Barricado;
				$barricado->set_id($row['id']);
				$barricado->set_condition(ObjectsDictionary::translate("barricado", $row['condition']));
				$barricado->set_road_side(ObjectsDictionary::translate("barricado", $row['road_side']));
				$barricado->set_install_reason($row['reason_to_install']);
				$barricado->set_length($row['length']);
				$barricado->set_height($row['height']);
				$barricado->set_bracket_step($row['bracket_step']);
				$barricado->set_bracket_count($row['bracket_count']);
				$barricado->set_status(ObjectsDictionary::translate("barricado", $row['status']));
				$barricado->set_install_date($row['install_date']);
				$barricado->set_update_date($row['update_date']);
				$barricado->set_barricado_type(BarricadoManager::get_barricado_type($row['id']));
				$barricado->set_address(AddressManager::get_address($row['address_id']));
				$barricado->set_responsible_organization(OrganizationManager::get_organization($row['responsible_organization_id']));
				$barricado->add_geometry($row['bar_geometry']);
				$barricado->set_photoes(PhotoManager::get_photoes($row['id'], "Barricado"));
				$barricado->set_object_type("barricado");
			}
			return $barricado;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_barricado_type($barricado_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT bar.id as id, bar.brc_class as barricado_class, bar.brc_subclass as barricado_subclass, bar.brc_group as barricado_group, bar.brc_subgroup as barricado_subgroup, bar.brc_functiontype as barricado_function_type, bar.brc_material as barricado_material FROM  barricado_barricado bar Where bar.id = :barricado_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":barricado_id",$barricado_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$barricado_type = null;
			if(isset($row) && $row['id'] != null){
				$barricado_type = new BarricadoType;
				$barricado_type->set_id($row['id']);
				$barricado_type->set_class(ObjectsDictionary::translate("barricado", $row['barricado_class']));
				$barricado_type->set_sub_class(ObjectsDictionary::translate("barricado", $row['barricado_subclass']));
				$barricado_type->set_group(ObjectsDictionary::translate("barricado", $row['barricado_group']));
				$barricado_type->set_sub_group(ObjectsDictionary::translate("barricado", $row['barricado_subgroup']));
				$barricado_type->set_function_type(ObjectsDictionary::translate("barricado", $row['barricado_function_type']));
				$barricado_type->set_material(ObjectsDictionary::translate("barricado", $row['barricado_material']));
			}
			return $barricado_type;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}