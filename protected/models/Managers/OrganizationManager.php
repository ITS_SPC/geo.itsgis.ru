<?php
class OrganizationManager implements iOrganizationManager{
	public static function get_organization($organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT io.id as organization_id, io.name as organization_name  From info_organization io Where io.id = :organization_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$organization = null;
			if(isset($row) && ($row['organization_id'] != null)){
				$organization = new Organization;
				$organization->set_id($row['organization_id']);
				$organization->set_name($row['organization_name']);
				$organization->set_address(OrganizationManager::get_organization_address($organization_id));
				$organization->set_contact(ContactManager::get_contact($row['organization_id'], ""));
				$organization->set_categories(OrganizationManager::get_organization_categories($row['organization_id']));
				$organization->set_work_time(OrganizationManager::get_organization_work_time($row['organization_id']));
			}
			return $organization;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_organizations_by_address_id($address_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT io.id as organization_id, io.name as organization_name  From info_organization io JOIN info_organization_address ioa ON io.id = ioa.organization_id Where ioa.address_id = :address_id ORDER BY io.name";
				$command = $connection->createCommand($sql);
				$command->bindParam(":address_id",$address_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$organizations = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					$organization = new Organization;
					$organization->set_id($row['organization_id']);
					$organization->set_name($row['organization_name']);
					$organization->set_address(OrganizationManager::get_organization_address($row['organization_id']));
					$organization->set_contact(ContactManager::get_contact($row['organization_id'], ""));
					$organization->set_categories(OrganizationManager::get_organization_categories($row['organization_id']));
					$organization->set_work_time(OrganizationManager::get_organization_work_time($row['organization_id']));
					array_push($organizations, $organization);
				}
			}
			return $organizations;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_organization_address($organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT ioa.address_id as organization_address_id FROM info_organization io JOIN info_organization_address ioa ON io.id = ioa.organization_id Where ioa.organization_id = :organization_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$address = new Address;
			if(isset($row)){
				$address = AddressManager::get_address($row['organization_address_id']);
			}
			return $address;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_organization_categories($organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT ic.name as category_name FROM info_category ic JOIN info_organization_category ioc ON ic.id = ioc.category_id JOIN info_organization io ON ioc.organization_id = io.id Where io.id = :organization_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
				$command->execute();
				$rows = $command->queryAll();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$categories = array();
			if(isset($rows)){
				foreach ($rows as $row) {
					array_push($categories, $row['category_name']);
				}
			}
			return $categories;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_organization_work_time($organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT itt.monday as monday, itt.tuesday as tuesday, itt.wednesday as wednesday, itt.thursday as thursday, itt.friday as friday, itt.saturday as saturday, itt.sunday as sunday FROM info_organization io JOIN info_time_table itt ON io.time_of_work = itt.id Where io.id = :organization_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$work_time = null;
			if(isset($row) && !empty($row)){
				foreach ($row as $key => $value) {
					$work_time[$key] = OrganizationManager::get_day_work_time($value, $key, $organization_id);
				}
			}
			return $work_time;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_day_work_time($day_id, $day_name, $organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT to_char(iti.start_time, 'HH24:MI') as start_time, to_char(iti.end_time, 'HH24:MI') as end_time FROM info_time_interval iti Where iti.id = :day_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":day_id",$day_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$work_time = array();
			if(isset($row)){
				$work_time['start_time'] = $row['start_time'];
				$work_time['end_time'] = $row['end_time'];
				$work_time['dinner_time'] = OrganizationManager::get_day_dinner_time($day_name, $organization_id);
				/*foreach ($row as $key => $value) {
					$work_time[$key] = $value;
				}*/
			}
			return $work_time;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_day_dinner_time($day_name, $organization_id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT itt.".$day_name." as day_name FROM info_time_table itt JOIN info_organization io ON itt.id = io.dinner_time Where io.id = :organization_id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":organization_id",$organization_id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$dinner_time = array();
			if(isset($row)){
				$dinner_time = OrganizationManager::get_time_interval($row['day_name']);
				/*$work_time['start_time'] = $row['start_time'];
				$work_time['end_time'] = $row['end_time'];
				$work_time['dinner_time'] = OrganizationManager::get_day_dinner_time($day_name, $organization_id);*/
				/*foreach ($row as $key => $value) {
					$work_time[$key] = $value;
				}*/
			}
			return $dinner_time;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
	public static function get_time_interval($id){
		try{
			$connection = Yii::app()->db;	
			$transaction = $connection->beginTransaction();
			try{
				$sql = "SELECT to_char(start_time, 'HH24:MI') as start_time, to_char(end_time, 'HH24:MI') as end_time FROM info_time_interval iti Where id = :id";
				$command = $connection->createCommand($sql);
				$command->bindParam(":id",$id,PDO::PARAM_STR);
				$command->execute();
				$row = $command->queryRow();
				$transaction->commit();
			}
			catch(Exception $e){
				$transaction->rollback();
			}
			$time_interval = array();
			if(isset($row)){
				$time_interval['start_time'] = $row['start_time'];
				$time_interval['end_time'] = $row['end_time'];
			}
			return $time_interval;
		}
		catch(Exception $e){
			Yii::log('error', CLogger::LEVEL_ERROR);
			echo "Db connection error...";
		}
	}
}