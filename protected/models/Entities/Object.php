<?php
class Object{

	public $geometries = array();
  public $object_type;

  	public function get_geometries(){
    	return $this->geometries;
  	}
    public function get_object_type(){
      return $this->object_type;
    }

  	public function set_geometries($geometries){
  		$this->geometries = $geometries;
  	}
    public function set_object_type($object_type){
      $this->object_type = $object_type;
    }

  	public function add_geometry($geometry){
  		array_push($this->geometries, $geometry);
  	}
}
