<?php
class RoadMarking extends Object{
	
	public $id;
	public $road_marking_type;
	public $road_marking_material;
	public $quality;
	public $brightness;
	public $lrnightdry;
	public $lrnightwet;
	public $lrday;
	public $drawing_date;


	public function get_id(){
    	return $this->id;
  	}
  	public function get_road_marking_type(){
    	return $this->road_marking_type;
  	}
  	public function get_road_marking_material(){
   		return $this->road_marking_material;
  	}
  	public function get_quality(){
   		return $this->quality;
  	}
  	public function get_brightness(){
   		return $this->brightness;
  	}
  	public function get_lrnightdry(){
   		return $this->lrnightdry;
  	}
  	public function get_lrnightwet(){
   		return $this->lrnightwet;
  	}
  	public function get_lrday(){
   		return $this->lrday;
  	}
  	public function get_drawing_date(){
   		return $this->drawing_date;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_road_marking_type($road_marking_type){
  		$this->road_marking_type = $road_marking_type;
  	}
  	public function set_road_marking_material($road_marking_material){
  		$this->road_marking_material = $road_marking_material;
  	}
  	public function set_quality($quality){
  		$this->quality = $quality;
  	}
  	public function set_brightness($brightness){
  		$this->brightness = $brightness;
  	}
  	public function set_lrnightdry($lrnightdry){
  		$this->lrnightdry = $lrnightdry;
  	}
  	public function set_lrnightwet($lrnightwet){
  		$this->lrnightwet = $lrnightwet;
  	}
  	public function set_lrday($lrday){
  		$this->lrday = $lrday;
  	}
  	public function set_drawing_date($drawing_date){
  		$this->drawing_date = $drawing_date;
  	}
}
