<?php
class BarricadoType{
	
	public $id;
	public $class_name;
	public $sub_class;
	public $group;
	public $sub_group;
	public $function_type;
	public $material;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_class(){
    	return $this->class_name;
  	}
  	public function get_sub_class(){
   		return $this->sub_class;
  	}
  	public function get_group(){
   		return $this->group;
  	}
  	public function get_sub_group(){
   		return $this->sub_group;
  	}
  	public function get_function_type(){
   		return $this->function_type;
  	}
  	public function get_material(){
   		return $this->material;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_class($class_name){
  		$this->class_name = $class_name;
  	}
  	public function set_sub_class($sub_class){
  		$this->sub_class = $sub_class;
  	}
  	public function set_group($group){
  		$this->group = $group;
  	}
  	public function set_sub_group($sub_group){
  		$this->sub_group = $sub_group;
  	}
  	public function set_function_type($function_type){
  		$this->function_type = $function_type;
  	}
  	public function set_material($material){
  		$this->material = $material;
  	}
}
