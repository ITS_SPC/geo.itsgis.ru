<?php
class TrafficLightConstruct{
	public $id;
	public $variant_name;
	public $red_first;
	public $yellow_first;
	public $green_first;
	public $green_additional;
	public $white_moon;
	public $red_second;
	public $yellow_second;
	public $green_second;
	public $type_name;

	public function get_id(){
		return $this->id;
	}
	public function get_variant_name(){
      return $this->variant_name;
    }
    public function get_red_first(){
    	return $this->red_first;
    }
    public function get_yellow_first(){
    	return $this->yellow_first;
    }
    public function get_green_first(){
    	return $this->green_first;
    }
    public function get_green_additional(){
    	return $this->green_additional;
    }
    public function get_white_moon(){
    	return $this->white_moon;
    }
    public function get_red_second(){
    	return $this->red_second;
    }
    public function get_yellow_second(){
    	return $this->yellow_second;
    }
    public function get_green_second(){
    	return $this->green_second;
    }
    public function get_type_name(){
    	return $this->type_name;
    }

	public function set_id($id){
		$this->id = $id;
	}
	public function set_variant_name($variant_name){
      $this->variant_name = $variant_name;
    }
    public function set_red_first($red_first){
    	$this->red_first = $red_first;
    }
    public function set_yellow_first($yellow_first){
    	$this->yellow_first = $yellow_first;
    }
    public function set_green_first($green_first){
    	$this->green_first = $green_first;
    }
    public function set_green_additional($green_additional){
    	$this->green_additional = $green_additional;
    }
    public function set_white_moon($white_moon){
    	$this->white_moon = $white_moon;
    }
    public function set_red_second($red_second){
    	$this->red_second = $red_second;
    }
    public function set_yellow_second($yellow_second){
    	$this->yellow_second = $yellow_second;
    }
    public function set_green_second($green_second){
    	$this->green_second = $green_second;
    }
    public function set_type_name($type_name){
    	$this->type_name = $type_name;
    }
}