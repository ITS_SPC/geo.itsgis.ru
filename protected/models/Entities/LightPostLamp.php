<?php
class LightPostLamp{
	
	public $id;
	public $angle;
	public $lamp_type;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_lamp_type(){
    	return $this->lamp_type;
  	}
  	public function get_angle(){
   		return $this->angle;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_lamp_type($lamp_type){
  		$this->lamp_type = $lamp_type;
  	}
  	public function set_angle($angle){
  		$this->angle = $angle;
  	}
}
