<?php
class LampType{
	
	public $id;
	public $name;
	public $power;
	public $light_flow;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_name(){
    	return $this->name;
  	}
  	public function get_power(){
   		return $this->power;
  	}
  	public function get_light_flow(){
   		return $this->light_flow;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_name($name){
  		$this->name = $name;
  	}
  	public function set_power($power){
  		$this->power = $power;
  	}
  	public function set_light_flow($light_flow){
  		$this->light_flow = $light_flow;
  	}
}
