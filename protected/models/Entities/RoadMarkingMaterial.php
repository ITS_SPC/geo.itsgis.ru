<?php
class RoadMarkingMaterial{
	
	public $id;
	public $material_name;
	public $is_reflective;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_material_name(){
    	return $this->material_name;
  	}
  	public function get_is_reflective(){
   		return $this->is_reflective;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_material_name($material_name){
  		$this->material_name = $material_name;
  	}
  	public function set_is_reflective($is_reflective){
  		$this->is_reflective = $is_reflective;
  	}
}
