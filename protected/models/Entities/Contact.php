<?php
class Contact{

  public $phones = array();
  public $sites = array();
  public $emails = array();
  public $faxes = array();
  public $mobile_phones = array();
  public $icqs = array();

  public function get_phones(){
    return $this->phones;
  }
  public function get_sites(){
    return $this->sites;
  }
  public function get_emails(){
    return $this->emails;
  }
  public function get_faxes(){
    return $this->faxes;
  }
  public function get_mobile_phones(){
    return $this->mobile_phones;
  }
  public function get_icqs(){
    return $this->icqs;
  }

  public function add_phone($phone){
  	array_push($this->phones, $phone);
  }
  public function add_site($site){
  	array_push($this->sites, $site);
  }
  public function add_email($email){
  	array_push($this->emails, $email);
  }
  public function add_fax($fax){
  	array_push($this->faxes, $fax);
  }
  public function add_mobile_phone($mobile_phone){
  	array_push($this->mobile_phones, $mobile_phone);
  }
  public function add_icq($icq){
  	array_push($this->icqs, $icq);
  }

  public function set_phones($phones){
    $this->phones = $phones;
  }
  public function set_sites($sites){
    $this->sites = $sites;
  }
  public function set_emails($emails){
    $this->emails = $emails;
  }
  public function set_faxes($faxes){
    $this->faxes = $faxes;
  }
  public function set_mobile_phones($mobile_phones){
    $this->mobile_phones = $mobile_phones;
  }
  public function set_icqs($icqs){
    $this->icqs = $icqs;
  }
}