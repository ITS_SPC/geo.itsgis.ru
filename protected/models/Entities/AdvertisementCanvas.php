<?php
class AdvertisementCanvas{
	
	public $id;
	public $rent_start;
	public $rent_end;
	public $contact;
  public $is_person;
  public $organizations = array();

	public function get_id(){
   	return $this->id;
  }
  public function get_rent_start(){
   	return $this->rent_start;
  }
  public function get_rent_end(){
  	return $this->rent_end;
  }
  public function get_contact(){
  	return $this->contact;
  }
  public function get_is_person(){
    return $this->is_person;
  }
  public function get_organizations(){
    return $this->organizations;
  }

  public function set_id($id){
  	$this->id = $id;
  }
  public function set_rent_start($rent_start){
  	$this->rent_start = $rent_start;
  }
  public function set_rent_end($rent_end){
  	$this->rent_end = $rent_end;
  }
  public function set_contact($contact){
  	$this->contact = $contact;
  }
  public function set_is_person($is_person){
    $this->is_person = $is_person;
  }
  public function set_organizations($organizations){
    $this->organizations = $organizations;
  }
}
