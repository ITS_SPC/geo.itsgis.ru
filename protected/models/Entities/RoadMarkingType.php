<?php
class RoadMarkingType{
	
	public $id;
	public $type_number;
	public $width;
	public $mult;
	public $stroke_length;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_type_number(){
    	return $this->type_number;
  	}
  	public function get_width(){
   		return $this->width;
  	}
  	public function get_mult(){
   		return $this->mult;
  	}
  	public function get_stroke_length(){
   		return $this->stroke_length;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_type_number($type_number){
  		$this->type_number = $type_number;
  	}
  	public function set_width($width){
  		$this->width = $width;
  	}
  	public function set_mult($mult){
  		$this->mult = $mult;
  	}
  	public function set_stroke_length($stroke_length){
  		$this->stroke_length = $stroke_length;
  	}
}
