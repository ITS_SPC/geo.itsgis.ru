<?php
class SignSize{
	
	public $id;
	public $size_type;
	public $width;
	public $height;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_size_type(){
    	return $this->size_type;
  	}
  	public function get_width(){
   		return $this->width;
  	}
  	public function get_height(){
   		return $this->height;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_size_type($size_type){
  		$this->size_type = $size_type;
  	}
  	public function set_width($width){
  		$this->width = $width;
  	}
  	public function set_height($height){
  		$this->height = $height;
  	}
}
