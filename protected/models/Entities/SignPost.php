<?php
class SignPost extends Object{
	
	public $id;
	public $post_type;
	public $quality;
	public $angle;
	public $address;
	public $install_date;
	public $life_time;
	public $owner;
  public $post_groupings = array();

	public function get_id(){
    	return $this->id;
  	}
  	public function get_post_type(){
    	return $this->post_type;
  	}
  	public function get_quality(){
   		return $this->quality;
  	}
  	public function get_angle(){
   		return $this->angle;
  	}
  	public function get_address(){
   		return $this->address;
  	}
  	public function get_life_time(){
  		return $this->life_time;
  	}
  	public function get_install_date(){
  		return $this->install_date;
  	}
  	public function get_owner(){
  		return $this->owner;
  	}
    public function get_post_groupings(){
      return $this->post_groupings;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_post_type($post_type){
  		$this->post_type = $post_type;
  	}
  	public function set_quality($quality){
  		$this->quality = $quality;
  	}
  	public function set_angle($angle){
  		$this->angle = $angle;
  	}
  	public function set_address($address){
  		$this->address = $address;
  	}
  	public function set_life_time($life_time){
  		$this->life_time = $life_time;
  	}
  	public function set_install_date($install_date){
  		$this->install_date = $install_date;
  	}
  	public function set_owner($owner){
  		$this->owner = $owner;
  	}
    public function set_post_groupings($post_groupings){
      $this->post_groupings = $post_groupings;
    }
}
