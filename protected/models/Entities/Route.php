<?php
class Route{
	
	public $id;
	public $route_name;
	public $transport_type;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_route_name(){
    	return $this->route_name;
  	}
  	public function get_transport_type(){
   		return $this->transport_type;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_route_name($route_name){
  		$this->route_name = $route_name;
  	}
  	public function set_transport_type($transport_type){
  		$this->transport_type = $transport_type;
  	}
}
