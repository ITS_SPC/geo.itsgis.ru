<?php
class Layer extends CFormModel
{
  public $layerid;
  public $alias;

  public function get_id(){
    return $this->layerid;
  }
  public function get_alias(){
    return $this->alias;
  }

  public function set_id($layerid){
    $this->layerid = $layerid;
  }
  public function set_alias($alias){
    $this->alias = $alias;
  }
}