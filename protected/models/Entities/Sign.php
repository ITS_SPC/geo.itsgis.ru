<?php
class Sign extends PostObject{
	
	public $id;
  public $is_gost;
  public $sign_state;
  public $install_date;
  public $uninstall_date;
  public $inspection_date;
  public $note;
  public $street_side;
  public $skin_type;
  public $is_diode;
  public $sign_type;
  public $sign_size;
  public $organization;
  public $sign_picture;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_sign_state(){
    	return $this->sign_state;
  	}
  	public function get_is_yellow(){
   		return $this->is_yellow;
  	}
    public function get_install_date(){
      return $this->install_date;
    }
  	public function get_uninstall_date(){
   		return $this->uninstall_date;
  	}
  	public function get_inspection_date(){
   		return $this->inspection_date;
  	}
  	public function get_is_diode(){
  		return $this->is_diode;
  	}
  	public function get_is_gost(){
  		return $this->is_gost;
  	}
  	public function get_note(){
  		return $this->note;
  	}
    public function get_manufacturer(){
      return $this->manufacturer;
    }
    public function get_street_side(){
      return $this->street_side;
    }
    public function get_skin_type(){
      return $this->skin_type;
    }
    public  function get_organization(){
      return $this->organization;
    }
    public function get_sign_type(){
      return $this->sign_type;
    }
    public function get_sign_size(){
      return $this->sign_size;
    }
    public function get_sign_picture(){
      return $this->sign_picture;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_sign_state($sign_state){
  		$this->sign_state = $sign_state;
  	}
  	public function set_is_yellow($is_yellow){
  		$this->is_yellow = $is_yellow;
  	}
    public function set_install_date($install_date){
      $this->install_date = $install_date;
    }
  	public function set_uninstall_date($uninstall_date){
  		$this->uninstall_date = $uninstall_date;
  	}
  	public function set_inspection_date($inspection_date){
  		$this->inspection_date = $inspection_date;
  	}
  	public function set_is_diode($is_diode){
  		$this->is_diode = $is_diode;
  	}
  	public function set_is_gost($is_gost){
  		$this->is_gost = $is_gost;
  	}
  	public function set_note($note){
  		$this->note = $note;
  	}
    public function set_manufacturer($manufacturer){
      $this->manufacturer = $manufacturer;
    }
    public function set_street_side($street_side){
      $this->street_side = $street_side;
    }
    public function set_skin_type($skin_type){
      $this->skin_type = $skin_type;
    }
    public  function set_organization($organization){
      $this->organization = $organization;
    }
    public function set_sign_type($sign_type){
      $this->sign_type = $sign_type;
    }
    public function set_sign_size($sign_size){
      $this->sign_size = $sign_size;
    }
    public function set_sign_picture($sign_picture){
      $this->sign_picture = $sign_picture;
    }
}
