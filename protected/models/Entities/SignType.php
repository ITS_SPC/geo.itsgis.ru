<?php
class SignType{
	
	public $id;
	public $name;
	public $gost_number;
  public $sign_rule;
  public $format;
  public $isspecial;
  public $danger;

  public function get_id($id){
    return $this->id;
  }
  public function get_name($name){
    return $this->name;
  }
  public function get_gost_number($gost_number){
    return $this->gost_number;
  }
  public function get_sign_rule($sign_rule){
    return $this->sign_rule;
  }
  public function get_format($format){
    return $this->format;
  }
  public function get_isspecial($isspecial){
    return $this->isspecial;
  }
  public function get_danger($danger){
    return $this->danger;
  }

  public function set_id($id){
  	$this->id = $id;
  }
  public function set_name($name){
    $this->name = $name;
  }
  public function set_gost_number($gost_number){
    $this->gost_number = $gost_number;
  }
  public function set_sign_rule($sign_rule){
    $this->sign_rule = $sign_rule;
  }
  public function set_format($format){
    $this->format = $format;
  }
  public function set_isspecial($isspecial){
    $this->isspecial = $isspecial;
  }
  public function set_danger($danger){
    $this->danger = $danger;
  }
}
