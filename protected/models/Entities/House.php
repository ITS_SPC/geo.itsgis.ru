<?php
class House extends Object{

	public $address;
	public $organizations = array();

	public function get_address(){
    	return $this->address;
  	}
  	public function get_organizations(){
  		return $this->organizations;
  	}

  	public function set_address($address){
  		$this->address = $address;
  	}
  	public function set_organizations($organizations){
  		$this->organizations = $organizations;
  	}
}
