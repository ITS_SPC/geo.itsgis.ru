<?php
class TiedTerritoryContract{
	
	public $id;
	public $contract_number;
	public $contract_date;
	public $square;
	public $begin_date;
	public $end_date;
	public $organization;
	public $address;
  public $photoes;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_contract_number(){
    	return $this->contract_number;
  	}
  	public function get_contract_date(){
    	return $this->contract_date;
  	}
  	public function get_square(){
    	return $this->square;
  	}
  	public function get_begin_date(){
    	return $this->begin_date;
  	}
  	public function get_end_date(){
    	return $this->end_date;
  	}
  	public function get_organization(){
    	return $this->organization;
  	}
  	public function get_address(){
    	return $this->address;
  	}
    public function get_photoes(){
      return $this->photoes;
    }


  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_contract_number($contract_number){
  		$this->contract_number = $contract_number;
  	}
  	public function set_contract_date($contract_date){
  		$this->contract_date = $contract_date;
  	}
  	public function set_square($square){
  		$this->square = $square;
  	}
  	public function set_begin_date($begin_date){
  		$this->begin_date = $begin_date;
  	}
  	public function set_end_date($end_date){
  		$this->end_date = $end_date;
  	}
  	public function set_organization($organization){
  		$this->organization = $organization;
  	}
  	public function set_address($address){
  		$this->address = $address;
  	}
    public function set_photoes($photoes){
      $this->photoes = $photoes;
    }
}
