<?php

/**
 * This is the model class for table "layerobject".
 *
 * The followings are the available columns in table 'layerobject':
 * @property string $layerid
 * @property string $alias
 * @property string $mapid
 * @property integer $order_index
 * @property double $vizible_zoom_min
 * @property double $vizible_zoom_max
 * @property boolean $is_plugin_layer
 *
 * The followings are the available model relations:
 * @property PuGeoroleLayer[] $puGeoroleLayers
 * @property Mapobject $map
 * @property Featureobject[] $featureobjects
 */
class Layerobject extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'layerobject';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('layerid, alias, order_index', 'required'),
			array('order_index', 'numerical', 'integerOnly'=>true),
			array('vizible_zoom_min, vizible_zoom_max', 'numerical'),
			array('alias', 'length', 'max'=>255),
			array('mapid, is_plugin_layer', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('layerid, alias, mapid, order_index, vizible_zoom_min, vizible_zoom_max, is_plugin_layer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'puGeoroleLayers' => array(self::HAS_MANY, 'PuGeoroleLayer', 'layerid'),
			'map' => array(self::BELONGS_TO, 'Mapobject', 'mapid'),
			'featureobjects' => array(self::HAS_MANY, 'Featureobject', 'layerid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'layerid' => 'Layerid',
			'alias' => 'Alias',
			'mapid' => 'Mapid',
			'order_index' => 'Order Index',
			'vizible_zoom_min' => 'Vizible Zoom Min',
			'vizible_zoom_max' => 'Vizible Zoom Max',
			'is_plugin_layer' => 'Is Plugin Layer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('layerid',$this->layerid,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('mapid',$this->mapid,true);
		$criteria->compare('order_index',$this->order_index);
		$criteria->compare('vizible_zoom_min',$this->vizible_zoom_min);
		$criteria->compare('vizible_zoom_max',$this->vizible_zoom_max);
		$criteria->compare('is_plugin_layer',$this->is_plugin_layer);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Layerobject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
