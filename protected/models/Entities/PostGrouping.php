<?php
class PostGrouping{
	
	public $id;
	public $group_number;
	public $post_objects = array();

	public function get_id(){
    	return $this->id;
  	}
  	public function get_group_number(){
    	return $this->group_number;
  	}
  	public function get_post_objects(){
  		return $this->post_objects;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_group_number($group_number){
  		$this->group_number = $group_number;
  	}
  	public function set_post_objects($post_objects){
  		$this->post_objects = $post_objects;
  	}
}
