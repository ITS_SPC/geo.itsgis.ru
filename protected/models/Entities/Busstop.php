<?php
class Busstop extends Object{
	
	public $id;
	public $name;
	public $landing_field;
	public $pocket;
	public $pavilion;
	public $road_side;
  public $has_speed_change_rows;
  public $norm_speed_up_lane_length;
  public $norm_speed_down_lane_length;
  public $fact_speed_up_lane_length;
  public $fact_speed_down_lane_length;
  public $owner;
  public $address;
  public $routes = array();
  public $photoes;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_name(){
    	return $this->name;
  	}
  	public function get_landing_field(){
   		return $this->landing_field;
  	}
  	public function get_pocket(){
   		return $this->pocket;
  	}
  	public function get_pavilion(){
   		return $this->pavilion;
  	}
  	public function get_road_side(){
   		return $this->road_side;
  	}
    public function get_owner(){
      return $this->owner;
    }
    public function get_address(){
      return $this->address;
    }
    public function get_has_speed_change_rows(){
      return $this->has_speed_change_rows;
    }
    public function get_norm_speed_up_lane_length(){
      return $this->norm_speed_up_lane_length;
    }
    public function get_norm_speed_down_lane_length(){
      return $this->norm_speed_down_lane_length;
    }
    public function get_fact_speed_up_lane_length(){
      return $this->fact_speed_up_lane_length;
    }
    public function get_fact_speed_down_lane_length(){
      return $this->fact_speed_down_lane_length;
    }
    public function get_routes(){
      return $this->routes;
    }
    public function get_photoes(){
      return $this->photoes;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_name($name){
  		$this->name = $name;
  	}
  	public function set_landing_field($landing_field){
  		$this->landing_field = $landing_field;
  	}
  	public function set_pocket($pocket){
  		$this->pocket = $pocket;
  	}
  	public function set_pavilion($pavilion){
  		$this->pavilion = $pavilion;
  	}
  	public function set_road_side($road_side){
  		$this->road_side = $road_side;
  	}
    public function set_owner($owner){
      $this->owner = $owner;
    }
    public function set_address($address){
      $this->address = $address;
    }
    public function set_has_speed_change_rows($has_speed_change_rows){
      $this->has_speed_change_rows = $has_speed_change_rows;
    }
    public function set_norm_speed_up_lane_length($norm_speed_up_lane_length){
      $this->norm_speed_up_lane_length = $norm_speed_up_lane_length;
    }
    public function set_norm_speed_down_lane_length($norm_speed_down_lane_length){
      $this->norm_speed_down_lane_length = $norm_speed_down_lane_length;
    }
    public function set_fact_speed_up_lane_length($fact_speed_up_lane_length){
      $this->fact_speed_up_lane_length = $fact_speed_up_lane_length;
    }
    public function set_fact_speed_down_lane_length($fact_speed_down_lane_length){
      $this->fact_speed_down_lane_length = $fact_speed_down_lane_length;
    }
    public function set_routes($routes){
      $this->routes = $routes;
    }
    public function set_photoes($photoes){
      $this->photoes = $photoes;
    }

    public function  add_route($route){
      array_push($this->routes, $route);
    }
}
