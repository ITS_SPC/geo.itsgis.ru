<?php
class LightPostGrouping{
	
	public $id;
	public $height;
	public $radius;
	public $angle;
	public $grouping_lamps = array();

	public function get_id(){
    	return $this->id;
  	}
  	public function get_height(){
    	return $this->height;
  	}
  	public function get_radius(){
   		return $this->radius;
  	}
  	public function get_angle(){
   		return $this->angle;
  	}
  	public function get_grouping_lamps(){
  		return $this->grouping_lamps;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_height($height){
  		$this->height = $height;
  	}
  	public function set_radius($radius){
  		$this->radius = $radius;
  	}
  	public function set_angle($angle){
  		$this->angle = $angle;
  	}
  	public function set_grouping_lamps($grouping_lamps){
  		$this->grouping_lamps = $grouping_lamps;
  	}

  	public function add_grouping_lamps($lamp){
  		array_push($this->grouping_lamps, $lamp);
  	}
}
