<?php
class Address{
	
	public $id;
	public $district;
	public $street_house;
  public $address_type;

	public function get_id(){
    return $this->id;
  }
  public function get_district(){
    return $this->district;
  }
  public function get_street_house(){
   	return $this->street_house;
  }
  public function get_address_type(){
    return $this->address_type;
  }


  public function set_id($id){
  	$this->id = $id;
  }
  public function set_district($district){
  	$this->district = $district;
  }
  public function set_street_house($street_house){
  	$this->street_house = $street_house;
  }
  public function set_address_type($address_type){
    $this->address_type = $address_type;
  }
}
