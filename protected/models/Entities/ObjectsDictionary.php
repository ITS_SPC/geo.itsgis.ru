<?php
	class ObjectsDictionary{
		//dictionary
		private static $dictionary = array(
			"barricado" => array(
				"Set" => "Установлена",
				"Required" => "Требуется",
				"Dismantle" => "Демонтировать",
				"Bad" => "Плохое",
				"Excellent" => "Отличное",
				"Fine" => "Хорошее",
				"Uneven" => "Нечетная",
				"Even" => "Четная",
				"HoldingOn" => "Удерживающие",
				"Bounding" => "Ограничивающие",
				"Single" => "Одностороннее",
				"Double" => "Двустороннее",
				"Man" => "Для пешеходов",
				"CarSide" => "Для автомобилей (боковые)",
				"CarFrontal" => "Для автомобилей (фронтальные)",
				"Steel" => "Сталь",
				"Composite" => "Композитный материал",
				"Concrete" => "Бетон",
				"Aluminium" => "Алюминиевый сплав",
				"Plastic" => "Пластик",
				"Road" => "Дорожные",
				"Bridge" => "Мостовые",
				"Group1" => "Группа 1 (вдоль тротуаров)",
				"Group2" => "Группа 2 (у переходов)",
				"Group3" => "Группа 3 (на газонах)",
				"Tanker" => "Наливные",
				"Edge" => "Бордюрные",
				"Protective" => "Защитные",
				"Parapet" => "Парапетные",
				"Elastic" => "Упругопластические",
				"Other" => "Иные типы конструкций",
				"LegHandrail" => "Стоечные перила",
				"ParapetHandrail" => "Парапетные перила",
				"BarrierHandrail" => "Барьерные перила",
				"Guiding" => "Направляющие",
				"Warning" => "Предупреждающие",
				"Combined" => "Комбинированные",
				"Barrier" => "Барьерные",
			),
		"busstop" => array(
			"true" => "Есть",
			"false" => "Нет",
			"Right" => "Правая",
			"Left" => "Левая",
			),
		"sign_post" => array(
				"Metal" => "Металл",
				"Beton" => "Бетон",
				"Wood" => "Дерево",
				"Independent" => "Независимая",
				"Light" => "Световая",
				"Bracket" => "Кронштейн",
				"Wire" => "Растяжка",
			),
		"sign" => array(
				"Bad" => "Плохое",
				"Perfect" => "Идеальное",
				"Good" => "Хорошее",
			),
		"traffic_light" => array(
				"Primary" => "Основной",
				"Dubler" => "Дублер",
				"Repeater" => "Повторитель",
				"Good" => "Хорошее",
				"Perfect" => "Отличное",
				"Bad" => "Плохое",
			),
		);
		//translate
		public static function translate($dictionary, $word){
			$translated = $word;
			if(($dictionary != "") && ($word != "")){
				foreach (self::$dictionary as $d_key => $d_value) {
					if($d_key == $dictionary){
						foreach ($d_value as $w_key => $w_value) {
							if($w_key == $word){
								$translated = $w_value;
								break;
							}
						}
						break;
					}
				}
			}
			return $translated;
		}
	}
?>