<?php
class User extends CFormModel
{
  private $id = null;
  private $user_name;
  private $user_password;
  private $user_metadata;
  private $user_enabled;
  private $user_create_time;
  private $user_expired_time;
  public function get_id(){
    return $this->id;
  }
  public function get_user_name(){
    return $this->user_name;
  }
  public function get_user_password(){
    return $this->user_password;
  }
  public function get_user_metadata(){
    return $this->user_metadata;
  }
  public function get_user_enabled(){
    return $this->user_enabled;
  }
  public function get_user_create_time(){
    return $this->user_create_time;
  }
  public function get_user_expired_time(){
    return $this->user_expired_time;
  }
  public function set_id($id){
    $this->id = $id;
  }
  public function set_user_name($user_name){
    $this->user_name = $user_name;
  }
  public function set_user_password($user_password){
    $this->user_password = $user_password;
  }
  public function set_user_metadata($user_metadata){
    $this->user_metadata = $user_metadata;
  }
  public function set_user_enabled($user_enabled){
    $this->user_enabled = $user_enabled;
  }
  public function set_user_create_time($user_create_time){
    $this->user_create_time = $user_create_time;
  }
  public function set_user_expired_time($user_expired_time){
    $this->user_expired_time = $user_expired_time;
  }
}