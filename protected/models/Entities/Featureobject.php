<?php

/**
 * This is the model class for table "featureobject".
 *
 * The followings are the available columns in table 'featureobject':
 * @property string $id
 * @property string $layerid
 * @property string $geometry
 * @property integer $version
 *
 * The followings are the available model relations:
 * @property Multipolygon $multipolygon
 * @property Picture $picture
 * @property BarricadoPhotoable[] $barricadoPhotoables
 * @property PubtrBusstop[] $pubtrBusstops
 * @property UdsNode[] $udsNodes
 * @property UdsArc[] $udsArcs
 * @property AddressFeatureobject[] $addressFeatureobjects
 * @property PubtrRoute[] $pubtrRoutes
 * @property Polygon $polygon
 * @property Geocollection $geocollection
 * @property RoadmarkingLine[] $roadmarkingLines
 * @property Text $text
 * @property UdsPlot[] $udsPlots
 * @property Styleobject $styleobject
 * @property Multiline $multiline
 * @property Layerobject $layer
 * @property Point $point
 * @property Multipoint $multipoint
 * @property Linestring $linestring
 * @property AdvAdvertisement[] $advAdvertisements
 */
class Featureobject extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'featureobject';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, geometry', 'required'),
			array('version', 'numerical', 'integerOnly'=>true),
			array('layerid', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, layerid, geometry, version', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'multipolygon' => array(self::HAS_ONE, 'Multipolygon', 'id'),
			'picture' => array(self::HAS_ONE, 'Picture', 'id'),
			'barricadoPhotoables' => array(self::HAS_MANY, 'BarricadoPhotoable', 'brc_featureobject_id'),
			'pubtrBusstops' => array(self::HAS_MANY, 'PubtrBusstop', 'feature_id'),
			'udsNodes' => array(self::HAS_MANY, 'UdsNode', 'featureobject_id'),
			'udsArcs' => array(self::HAS_MANY, 'UdsArc', 'featureobject_id'),
			'addressFeatureobjects' => array(self::HAS_MANY, 'AddressFeatureobject', 'feature_id'),
			'pubtrRoutes' => array(self::MANY_MANY, 'PubtrRoute', 'pubtr_routes_arcs_link(arcs_id, routes_id)'),
			'polygon' => array(self::HAS_ONE, 'Polygon', 'id'),
			'geocollection' => array(self::HAS_ONE, 'Geocollection', 'id'),
			'roadmarkingLines' => array(self::HAS_MANY, 'RoadmarkingLine', 'feature_object_id'),
			'text' => array(self::HAS_ONE, 'Text', 'id'),
			'udsPlots' => array(self::HAS_MANY, 'UdsPlot', 'featureobject_id'),
			'styleobject' => array(self::HAS_ONE, 'Styleobject', 'styleid'),
			'multiline' => array(self::HAS_ONE, 'Multiline', 'id'),
			'layer' => array(self::BELONGS_TO, 'Layerobject', 'layerid'),
			'point' => array(self::HAS_ONE, 'Point', 'id'),
			'multipoint' => array(self::HAS_ONE, 'Multipoint', 'id'),
			'linestring' => array(self::HAS_ONE, 'Linestring', 'id'),
			'advAdvertisements' => array(self::HAS_MANY, 'AdvAdvertisement', 'feature_object_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'layerid' => 'Layerid',
			'geometry' => 'Geometry',
			'version' => 'Version',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('layerid',$this->layerid,true);
		$criteria->compare('geometry',$this->geometry,true);
		$criteria->compare('version',$this->version);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Featureobject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
