<?php

/**
 * This is the model class for table "mapobject".
 *
 * The followings are the available columns in table 'mapobject':
 * @property string $id
 * @property string $alias
 * @property double $minx
 * @property double $miny
 * @property double $maxx
 * @property double $maxy
 * @property double $local_x
 * @property double $local_y
 * @property double $global_latitude
 * @property double $global_longitude
 * @property double $local_angle
 *
 * The followings are the available model relations:
 * @property Minimap[] $minimaps
 * @property Layerobject[] $layerobjects
 * @property Mapgrid[] $mapgrs
 */
class Mapobject extends CActiveRecord
{
	public $map_resources;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mapobject';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, alias', 'required'),
			array('minx, miny, maxx, maxy, local_x, local_y, global_latitude, global_longitude, local_angle', 'numerical'),
			array('alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, alias, minx, miny, maxx, maxy, local_x, local_y, global_latitude, global_longitude, local_angle', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'minimaps' => array(self::HAS_MANY, 'Minimap', 'map_id'),
			'layerobjects' => array(self::HAS_MANY, 'Layerobject', 'mapid', 'order'=>'layerobjects.order_index'),
			'mapgrs' => array(self::HAS_MANY, 'Mapgrid', 'mapid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'alias' => 'Alias',
			'minx' => 'Minx',
			'miny' => 'Miny',
			'maxx' => 'Maxx',
			'maxy' => 'Maxy',
			'local_x' => 'Local X',
			'local_y' => 'Local Y',
			'global_latitude' => 'Global Latitude',
			'global_longitude' => 'Global Longitude',
			'local_angle' => 'Local Angle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('minx',$this->minx);
		$criteria->compare('miny',$this->miny);
		$criteria->compare('maxx',$this->maxx);
		$criteria->compare('maxy',$this->maxy);
		$criteria->compare('local_x',$this->local_x);
		$criteria->compare('local_y',$this->local_y);
		$criteria->compare('global_latitude',$this->global_latitude);
		$criteria->compare('global_longitude',$this->global_longitude);
		$criteria->compare('local_angle',$this->local_angle);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mapobject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
