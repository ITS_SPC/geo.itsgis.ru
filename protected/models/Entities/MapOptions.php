<?php

/**
 * This is the model class for table "map_options".
 *
 * The followings are the available columns in table 'map_options':
 * @property string $map_id
 * @property string $map_name
 * @property string $map_alias
 * @property integer $zoom_levels
 * @property string $tiles_path
 * @property string $bound_lon_1
 * @property string $bound_lat_1
 * @property string $bound_lon_2
 * @property string $bound_lat_2
 * @property double $min_resolution
 * @property double $max_resolution
 * @property double $custom_map_center_lon
 * @property double $custom_map_center_lat
 * @property string $description
 * @property string $keywords
 * @property string $about_map
 */
class MapOptions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'map_options';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('map_id, map_name, map_alias, zoom_levels, tiles_path, bound_lon_1, bound_lat_1, bound_lon_2, bound_lat_2, min_resolution, max_resolution, custom_map_center_lon, custom_map_center_lat, description, keywords, about_map', 'required'),
			array('zoom_levels', 'numerical', 'integerOnly'=>true),
			array('min_resolution, max_resolution, custom_map_center_lon, custom_map_center_lat', 'numerical'),
			array('map_id, bound_lon_1, bound_lat_1, bound_lon_2, bound_lat_2', 'length', 'max'=>20),
			array('map_name, map_alias', 'length', 'max'=>100),
			array('tiles_path, description, keywords', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('map_id, map_name, map_alias, zoom_levels, tiles_path, bound_lon_1, bound_lat_1, bound_lon_2, bound_lat_2, min_resolution, max_resolution, custom_map_center_lon, custom_map_center_lat, description, keywords, about_map', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'map_id' => 'Map',
			'map_name' => 'Map Name',
			'map_alias' => 'Map Alias',
			'zoom_levels' => 'Zoom Levels',
			'tiles_path' => 'Tiles Path',
			'bound_lon_1' => 'Bound Lon 1',
			'bound_lat_1' => 'Bound Lat 1',
			'bound_lon_2' => 'Bound Lon 2',
			'bound_lat_2' => 'Bound Lat 2',
			'min_resolution' => 'Min Resolution',
			'max_resolution' => 'Max Resolution',
			'custom_map_center_lon' => 'Custom Map Center Lon',
			'custom_map_center_lat' => 'Custom Map Center Lat',
			'description' => 'Description',
			'keywords' => 'Keywords',
			'about_map' => 'About Map',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('map_id',$this->map_id,true);
		$criteria->compare('map_name',$this->map_name,true);
		$criteria->compare('map_alias',$this->map_alias,true);
		$criteria->compare('zoom_levels',$this->zoom_levels);
		$criteria->compare('tiles_path',$this->tiles_path,true);
		$criteria->compare('bound_lon_1',$this->bound_lon_1,true);
		$criteria->compare('bound_lat_1',$this->bound_lat_1,true);
		$criteria->compare('bound_lon_2',$this->bound_lon_2,true);
		$criteria->compare('bound_lat_2',$this->bound_lat_2,true);
		$criteria->compare('min_resolution',$this->min_resolution);
		$criteria->compare('max_resolution',$this->max_resolution);
		$criteria->compare('custom_map_center_lon',$this->custom_map_center_lon);
		$criteria->compare('custom_map_center_lat',$this->custom_map_center_lat);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('about_map',$this->about_map,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->geo;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MapOptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
