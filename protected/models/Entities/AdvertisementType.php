<?php
class AdvertisementType{
	
	public $id;
	public $type_name;
	public $sides_number;
	public $canvas_number;
  public $type_image;
  public $parameters = array();

	public function get_id(){
   	return $this->id;
  }
  public function get_type_name(){
   	return $this->type_name;
  }
  public function get_sides_number(){
  	return $this->sides_number;
  }
  public function get_canvas_number(){
  	return $this->canvas_number;
  }
  public function get_image(){
    return $this->type_image;
  }
  public function get_parameters(){
    return $this->parameters;
  }

  public function set_id($id){
  	$this->id = $id;
  }
  public function set_type_name($type_name){
  	$this->type_name = $type_name;
  }
  public function set_sides_number($sides_number){
  	$this->sides_number = $sides_number;
  }
  public function set_canvas_number($canvas_number){
  	$this->canvas_number = $canvas_number;
  }
  public function set_image($type_image){
    $this->type_image = $type_image;
  }
  public function set_parameters($parameters){
    $this->parameters = $parameters;
  }
}
