<?php
class AdvertisementSide{
	public $id;
	public $canvases = array();

	public function get_id(){
		return $this->id;
	}
	public function get_canvases(){
		return $this->canvases;
	}

	public function set_id($id){
		$this->id = $id;
	}
	public function set_canvases($canvases){
		$this->canvases = $canvases;
	}

	public function add_canvas($canvas){
		array_push($this->canvases, $canvas);
	}
}