<?php
class TiedTerritory extends Object{

	public $id;
	public $territory_name;
  public $square;
	public $tied_territory_contracts = array();

	public function get_id(){
    return $this->id;
  }
  public function get_territory_name(){
   	return $this->territory_name;
  }
  public function get_tied_territory_contracts(){
  	return $this->tied_territory_contracts;
  }
  public function get_square(){
    return $this->square;
  }

  public function set_id($id){
  	$this->id = $id;
  }
  public function set_territory_name($territory_name){
  	$this->territory_name = $territory_name;
  }
  public function set_tied_territory_contracts($contracts){
  	$this->tied_territory_contracts = $contracts;
  }
  public function set_square($square){
    $this->square = $square;
  }
}
