<?php
class PostObject{
	
	public $id;
	public $post_order;
	public $install_date;
	public $post_object_type;
  public $photoes;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_post_order(){
    	return $this->post_order;
  	}
  	public function get_post_object_type(){
  		return $this->post_object_type;
  	}
  	public function get_install_date(){
  		return $this->install_date;
  	}
    public function get_photoes(){
      return $this->photoes;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_post_order($post_order){
  		$this->post_order = $post_order;
  	}
  	public function set_post_object_type($post_object_type){
  		$this->post_object_type = $post_object_type;
  	}
  	public  function set_install_date($install_date){
  		$this->install_date = $install_date;
  	}
    public function set_photoes($photoes){
      $this->photoes = $photoes;
    }
}
