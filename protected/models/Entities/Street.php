<?php
class Street extends Object{

	public $street_name;

	public function get_street_name(){
    	return $this->street_name;
  	}

  	public function set_street_name($street_name){
  		$this->street_name = $street_name;
  	}
}
