<?php
class LightPost extends Object{
	
	public $id;
	public $post_material;
	public $post_type;
	public $light_post_grouping = array();
  public $photoes;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_post_material(){
    	return $this->post_material;
  	}
  	public function get_post_type(){
   		return $this->post_type;
  	}
  	public function get_light_post_grouping(){
  		return $this->light_post_grouping;
  	}
    public function get_photoes(){
      return $this->photoes;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_post_material($post_material){
  		$this->post_material = $post_material;
  	}
  	public function set_post_type($post_type){
  		$this->post_type = $post_type;
  	}
  	public function set_light_post_grouping($light_post_grouping){
  		$this->light_post_grouping = $light_post_grouping;
  	}
    public function set_photoes($photoes){
      $this->photoes = $photoes;
    }

  	public function add_light_post_grouping($light_post_group){
  		array_push($this->light_post_grouping, $light_post_group);
  	}
}
