<?php
class TrafficLight extends PostObject{
	public $id;
	public $sign_picture;
	public $construct;
	public $signalization;
    public $role;
    public $is_light_diode;
    public $numeric_display;
    public $inspection_date;
    public $uninstall_date;
    public $note;
    public $light_state;
    public $is_active;

	public function get_id(){
		return $this->id;
	}
	public function get_sign_picture(){
      return $this->sign_picture;
    }
    public function get_construct(){
    	return $this->construct;
    }
    public function get_signalization(){
    	return $this->signalization;
    }
    public function get_role(){
        return $this->role;
    }
    public function get_is_light_diode(){
        return $this->is_light_diode;
    }
    public function get_numeric_display(){
        return $this->numeric_display;
    }
    public function get_inspection_date(){
        return $this->inspection_date;
    }
    public function get_uninstall_date(){
        return $this->uninstall_date;
    }
    public function get_note(){
        return $this->note;
    }
    public function get_light_state(){
        return $this->light_state;
    }
    public function get_is_active(){
        return $this->is_active;
    }

	public function set_id($id){
		$this->id = $id;
	}
	public function set_sign_picture($sign_picture){
      $this->sign_picture = $sign_picture;
    }
    public function set_construct($construct){
    	$this->construct = $construct;
    }
    public function set_signalization($signalization){
    	$this->signalization = $signalization;
    }
    public function set_role($role){
        $this->role = $role;
    }
    public function set_is_light_diode($is_light_diode){
        $this->is_light_diode = $is_light_diode;
    }
    public function set_numeric_display($numeric_display){
        $this->numeric_display = $numeric_display;
    }
    public function set_inspection_date($inspection_date){
        $this->inspection_date = $inspection_date;
    }
    public function set_uninstall_date($uninstall_date){
        $this->uninstall_date = $uninstall_date;
    }
    public function set_note($note){
        $this->note = $note;
    }
    public function set_light_state($light_state){
        $this->light_state = $light_state;
    }
    public function set_is_active($is_active){
        $this->is_active = $is_active;
    }
}