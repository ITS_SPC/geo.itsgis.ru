<?php
class Crossroad extends Object{

	public $crossroad_name;

	public function get_crossroad_name(){
    return $this->crossroad_name;
  }

  public function set_crossroad_name($crossroad_name){
  	$this->crossroad_name = $crossroad_name;
  }
}
