<?php
class Barricado extends Object{
	
	public $id;
	public $condition;
	public $road_side;
	public $install_reason;
	public $length;
	public $height;
	public $bracket_step;
	public $bracket_count;
	public $responsible_organization = null;
	public $status;
	public $address;
	public $install_date;
	public $update_date;
  public $barricado_type;
  public $photoes;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_condition(){
    	return $this->condition;
  	}
  	public function get_road_side(){
   		return $this->road_side;
  	}
  	public function get_install_reason(){
   		return $this->install_reason;
  	}
  	public function get_length(){
   		return $this->length;
  	}
  	public function get_height(){
   		return $this->height;
  	}
  	public function get_bracket_step(){
   		return $this->bracket_step;
  	}
  	public function get_bracket_count(){
   		return $this->bracket_count;
  	}
  	public function get_responsible_organization(){
   		return $this->responsible_organization;
  	}
  	public function get_status(){
   		return $this->status;
  	}
  	public function get_address(){
   		return $this->address;
  	}
  	public function get_install_date(){
   		return $this->install_date;
  	}
  	public function get_update_date(){
   		return $this->update_date;
  	}
    public function get_barricado_type(){
      return $this->barricado_type;
    }
    public function get_photoes(){
      return $this->photoes;
    }

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_condition($condition){
  		$this->condition = $condition;
  	}
  	public function set_road_side($road_side){
  		$this->road_side = $road_side;
  	}
  	public function set_install_reason($install_reason){
  		$this->install_reason = $install_reason;
  	}
  	public function set_length($length){
  		$this->length = $length;
  	}
  	public function set_height($height){
  		$this->height = $height;
  	}
  	public function set_bracket_step($bracket_step){
  		$this->bracket_step = $bracket_step;
  	}
  	public function set_bracket_count($bracket_count){
  		$this->bracket_count = $bracket_count;
  	}
  	public function set_responsible_organization($responsible_organization){
  		$this->responsible_organization = $responsible_organization;
  	}
  	public function set_status($status){
  		$this->status = $status;
  	}
  	public function set_address($address){
  		$this->address = $address;
  	}
  	public function set_install_date($install_date){
  		$this->install_date = $install_date;
  	}
  	public function set_update_date($update_date){
  		$this->update_date = $update_date;
  	}
    public function set_barricado_type($barricado_type){
      $this->barricado_type = $barricado_type;
    }
    public function set_photoes($photoes){
      $this->photoes = $photoes;
    }
}
