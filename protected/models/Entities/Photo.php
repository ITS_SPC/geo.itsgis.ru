<?php
class Photo{
	
	public $id;
	public $photo_date;
	public $photo_type;
	public $photo_order;
	public $photo_description;
	public $photo_link;

	public function get_id(){
    	return $this->id;
  	}
  	public function get_photo_date(){
    	return $this->photo_date;
  	}
  	public function get_photo_type(){
   		return $this->photo_type;
  	}
  	public function get_photo_order(){
   		return $this->photo_order;
  	}
  	public function get_photo_description(){
  		return $this->photo_description;
  	}
  	public function get_photo_link(){
  		return $this->photo_link;
  	}

  	public function set_id($id){
  		$this->id = $id;
  	}
  	public function set_photo_date($photo_date){
  		$this->photo_date = $photo_date;
  	}
  	public function set_photo_type($photo_type){
  		$this->photo_type = $photo_type;
  	}
  	public function set_photo_order($photo_order){
  		$this->photo_order = $photo_order;
  	}
  	public function set_photo_description($photo_description){
  		$this->photo_description = $photo_description;
  	}
  	public function set_photo_link($photo_id, $photo_type){
  		$this->photo_link = "http://app.itsgis.ru/itsgis-photo/".$photo_type."/".$photo_id.".jpg";
  	}
}
