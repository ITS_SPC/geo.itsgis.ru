<?php
class Map extends CFormModel{
    public $map_id;
  public $map_alias;
  public $map_name;
  public $zoom_levels;
  public $tiles_path;
  public $bound_lon_1;
  public $bound_lat_1;
  public $bound_lon_2;
  public $bound_lat_2;
  public $min_resolution;
  public $max_resolution;
  public $custom_map_center_lon;
  public $custom_map_center_lat;

  public function get_id(){
      return $this->map_id;
    }
    public function get_alias(){
      return $this->map_alias;
    }
    public function get_name(){
      return $this->map_name;
    }
    public function get_zoom_levels(){
      return $this->zoom_levels;
    }
    public function get_tiles_path(){
      return $this->tiles_path;
    }
    public function get_bound_lon_1(){
      return $this->bound_lon_1;
    }
    public function get_bound_lat_1(){
      return $this->bound_lat_1;
    }
    public function get_bound_lon_2(){
      return $this->bound_lon_2;
    }
    public function get_bound_lat_2(){
      return $this->bound_lat_2;
    }
    public function get_min_resolution(){
      return $this->min_resolution;
    }
    public function get_max_resolution(){
      return $this->max_resolution;
    }
    public function get_custom_map_center_lon(){
      return $this->custom_map_center_lon;
    }
    public function get_custom_map_center_lat(){
      return $this->custom_map_center_lat;
    }

  public function set_id($map_id){
    $this->map_id = $map_id;
  }
  public function set_alias($map_alias){
    $this->map_alias = $map_alias;
  }
  public function set_name($map_name){
    $this->map_name = $map_name;
  }
  public function set_zoom_levels($zoom_levels){
    $this->zoom_levels = $zoom_levels;
  }
  public function set_tiles_path($tiles_path){
    $this->tiles_path = $tiles_path;
  }
  public function set_bound_lon_1($bound_lon_1){
    $this->bound_lon_1 = $bound_lon_1;
  }
  public function set_bound_lat_1($bound_lat_1){
    $this->bound_lat_1 = $bound_lat_1;
  }
  public function set_bound_lon_2($bound_lon_2){
    $this->bound_lon_2 = $bound_lon_2;
  }
  public function set_bound_lat_2($bound_lat_2){
    $this->bound_lat_2 = $bound_lat_2;
  }
  public function set_min_resolution($min_resolution){
    $this->min_resolution = $min_resolution;
  }
  public function set_max_resolution($max_resolution){
    $this->max_resolution = $max_resolution;
  }
  public function set_custom_map_center_lon($custom_map_center_lon){
    $this->custom_map_center_lon = $custom_map_center_lon;
  }
  public function set_custom_map_center_lat($custom_map_center_lat){
    $this->custom_map_center_lat = $custom_map_center_lat;
  }
}