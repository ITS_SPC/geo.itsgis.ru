<?php
class Organization
{
  public $id;
  public $name;
  public $address;
  public $contact;
  public $categories;
  public $work_time = null;

  public function get_id(){
    return $this->id;
  }
  public function get_name(){
    return $this->name;
  }
  public function get_address(){
    return $this->address;
  }
  public function get_contact(){
    return $this->contact;
  }
  public function get_categories(){
    return $this->categories;
  }
  public function get_work_time(){
    return $this->work_time;
  }

  public function add_category($category){
    array_push($this->categories, $category);
  }
  public function add_work_time($work_time){
    array_push($this->work_time, $work_time);
  }

  public function set_id($id){
    $this->id = $id;
  }
  public function set_name($name){
    $this->name = $name;
  }
  public function set_address($address){
    $this->address = $address;
  }
  public function set_contact($contact){
    $this->contact = $contact;
  }
  public function set_categories($categories){
    $this->categories = $categories;
  }
  public function set_work_time($work_time){
    $this->work_time = $work_time;
  }
}