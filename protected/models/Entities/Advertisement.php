<?php
class Advertisement extends Object{
	
	public $id;
	public $install_date;
	public $uninstall_date;
	public $cadastral_number;
  public $angle;
  public $is_valid;
	public $advertisement_type;
	public $address;
  public $sides = array();
  public $photos = array();

	public function get_id(){
   	return $this->id;
  }
  public function get_install_date(){
   	return $this->install_date;
  }
  public function get_uninstall_date(){
  	return $this->uninstall_date;
  }
  public function get_cadastral_number(){
  	return $this->cadastral_number;
  }
  public function get_type(){
    return $this->advertisement_type;
  }
  public function get_angle(){
    return $this->angle;
  }
  public function get_valid(){
    return $this->is_valid;
  }
  public function get_address(){
    return $this->address;
  }
  public function get_sides(){
    return $this->sides;
  }
  public function get_photos(){
    return $this->photos;
  }

  public function set_id($id){
  	$this->id = $id;
  }
  public function set_install_date($install_date){
  	$this->install_date = $install_date;
  }
  public function set_uninstall_date($uninstall_date){
  	$this->uninstall_date = $uninstall_date;
  }
  public function set_cadastral_number($cadastral_number){
  	$this->cadastral_number = $cadastral_number;
  }
  public function set_type($advertisement_type){
  	$this->advertisement_type = $advertisement_type;
  }
  public function set_angle($angle){
    $this->angle = $angle;
  }
  public function set_valid($is_valid){
    $this->is_valid = $is_valid;
  }
  public function set_address($address){
    $this->address = $address;
  }
  public function set_sides($sides){
    $this->sides = $sides;
  }
  public function set_photos($photos){
    $this->photos = $photos;
  }
}
