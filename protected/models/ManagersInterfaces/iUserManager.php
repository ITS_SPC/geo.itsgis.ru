<?php
interface iUserManager{
	/**
	*	Check unique username
	*	Args: username
	*/
	public static function is_unique_username($username);
	/**
	*	Returns array of user objects
	*	Args: -
	*/
	public static function get_users();
	/**
	*	Returns user object
	*	Args: username
	*/
	public static function get_user($username);
}