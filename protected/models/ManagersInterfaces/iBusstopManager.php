<?php
interface iBusstopManager{
	/**
	*	Return busstop object
	*	Args: layer id, click coordinates
	*/
	public static function get_busstop($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns array of busstop routes objects
	*	Args: busstop id
	*/
	public static function get_busstop_routes($busstop_id);
}