<?php
interface iContactManager{
	/**
	*	Return contact object
	*	Args: organization id, contact type
	*/
	public static function get_contact($organization_id, $contact_type);
}