<?php
interface iTargetedPlanManager{
	/**
	*	Returns targeted plan object
	*	Args: map id, click coordinates
	*/
	public static function get_targeted_plan_object($map_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns object type index(position) in array
	*	Args: objects(array), address type(for check in loop) "House", "Crossroad", "Street"
	*/
	public static function object_type_index($objects, $address_type);
}
?>