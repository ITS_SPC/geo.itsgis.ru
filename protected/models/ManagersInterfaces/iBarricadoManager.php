<?php
interface iBarricadoManager{
	/**
	*	Return barricado object
	*	Args: layer id, click coordinates
	*/
	public static function get_barricado($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns barricado type object
	*	Args: barricado id
	*/
	public static function get_barricado_type($barricado_id);
}