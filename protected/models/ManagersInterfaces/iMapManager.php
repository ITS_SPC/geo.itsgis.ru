<?php
interface iMapManager{
	/**
	*	Return array of available maps, except given
	*	Args: mapid
	*/
	public static function get_available_maps($map_id);
	/**
	*	Returns map object
	*	Args: mapid
	*/
	public static function get_map_by_id($mapid);
	/**
	*	Returns object of map (tied territory, barricado etc) on current switched layer
	*	Args: current switched layer id, map id, coordinates
	*/
	public static function get_layer_object_by_coordinates($switched_layer, $map_id,$clickPositionLon, $clickPositionLat);
	/**
	*	Returns layer alias
	*	Args: layers id
	*/
	public static function get_layer_name($layer_id);
	/**
	*	Returns maps available for user
	*	Args: user id
	*/
	public static function get_user_maps($user_id);
}