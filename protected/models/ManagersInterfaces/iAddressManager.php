<?php
interface iAddressManager{
	/**
	*	Return address object
	*	Args: address id
	*/
	public static function get_address($address_id);
	/**
	*	Returns district string
	*	Args: district id
	*/
	public static function get_district($district_id);
	/**
	*	Returns address street geometry
	*	Args: address id
	*/
	public static function get_street_geometries($address_id);
	/**
	*	Returns address street name
	*	Args: address id
	*/
	public static function get_street_name($address_id);
	/**
	*	Returns address crossroad name
	*	Args: address id
	*/
	public static function get_crossroad_name($address_id);
}