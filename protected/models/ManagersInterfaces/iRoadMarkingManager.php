<?php
interface iRoadMarkingManager{
	/**
	*	Return road marking object
	*	Args: layer id, click coordinates
	*/
	public static function get_road_marking($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns road marking material object
	*	Args: material id
	*/
	public static function get_road_marking_material($material_id);
	/**
	*	Returns road marking type object
	*	Args: type id
	*/
	public static function get_road_marking_type($type_id);
}