<?php
interface iSignPostManager{
	/**
	*	Returns post with signs and traffic lights
	*	Args: layer id, click coordinates
	*/
	public static function get_post($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns post groupings objects of post
	*	Args: post id
	*/
	public static function get_post_groupings($post_id);
	/**
	*	Returns post grouping objects (signs and traffic lights)
	*	Args: grouping id
	*/
	public static function get_post_grouping_objects($grouping_id);
	/**
	*	Returns traffic light from post grouping object
	*	Args: post grouping object
	*/
	public static function get_light($post_grouping_object);
	/**
	*	Returns traffic light construct
	*	Args: construct id
	*/
	public static function get_light_construct($construct_id);
	/**
	*	Returns sign from post grouping object
	*	Args: post grouping object
	*/
	public static function get_sign($post_grouping_object);
	/**
	*	Returns sign type
	*	Args: sign type id
	*/
	public static function get_sign_type($sign_type_id);
	/**
	*	Returns sign size
	*	Args: sign size id
	*/
	public function get_sign_size($size_id);
	/**
	*	Returns img url if exist, or "" if not
	*	Args: type of post object, object id
	*/
	public static function checkImageUrl($post_object_type, $post_object_id);
}