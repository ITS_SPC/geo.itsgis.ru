<?php
interface iOrganizationManager{
	/**
	*	Returns organization object by id
	*	Args: organization id
	*/
	public static function get_organization($organization_id);
	/**
	*	Returns organizations array in given address
	*	Args: address id
	*/
	public static function get_organizations_by_address_id($address_id);
	/**
	*	Returns address object of organization
	*	Args: organization id
	*/
	public static function get_organization_address($organization_id);
	/**
	*	Returns array of organization categories
	*	Args: organization id
	*/
	public static function get_organization_categories($organization_id);
	/**
	*	Returns organization work time
	*	Args: organization id
	*/
	public static function get_organization_work_time($organization_id);
	/**
	*	Returns organization work time of day
	*	Args: day id, day name, organization id
	*/
	public static function get_day_work_time($day_id, $day_name, $organization_id);
	/**
	*	Returns organization dinner time of day
	*	Args: day name, organization id
	*/
	public static function get_day_dinner_time($day_name, $organization_id);
	/**
	*	Returns organization time interval
	*	Args: time interval
	*/
	public static function get_time_interval($id);
}