<?php
interface iPhotoManager{
	/**
	*	Return object photo object
	*	Args: object id, object type
	*/
	public static function get_photoes($object_id, $object_type);
}