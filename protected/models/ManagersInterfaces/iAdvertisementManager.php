<?php
interface iAdvertisementManager{
	/**
	*	Returns advertisement object
	*	Args: layer id, click coordinates
	*/
	public static function get_advertisement($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns advertisement sides(array) with canvas
	*	Args: advertisement_id
	*/
	public static function get_advertisement_sides($advertisement_id);
	/**
	*	Returns true if side is already exist, else false
	*	Args: sides(array), side
	*/
	public static function is_exist($sides, $side);
	/**
	*	Returns array of rent canvas organizations
	*	Args: canvas id
	*/
	public static function get_advertisement_canvas_rent_organizations($canvas_id);
	/**
	*	Returns parameters array of advertisement type
	*	Args: advertisement type id
	*/
	public static function get_advertisement_type_parameters($type_id);
}