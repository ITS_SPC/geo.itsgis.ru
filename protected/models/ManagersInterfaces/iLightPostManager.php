<?php
interface iLightPostManager{
	/**
	*	Returns light post object
	*	Args: map id, click coordinates
	*/
	public static function get_light_post($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns light post groupings
	*	Args: light post id
	*/
	public static function get_ligth_post_grouping($light_post_id);
	/**
	*	Returns light post grouping lamps
	*	Args: light post grouping id
	*/
	public static function get_light_post_grouping_lamps($light_post_grouping);
	/**
	*	Returns lamp type
	*	Args: lamp type id
	*/
	public static function get_lamp_type($lamp_type_id);
}