<?php
interface iTiedTerritoryManager{
	/**
	*	Return tied territory object
	*	Args: layer id, click coordinates
	*/
	public static function get_tied_territory($layer_id, $clickPositionLon, $clickPositionLat);
	/**
	*	Returns array of tt contract objects
	*	Args: tied territory id
	*/
	public static function get_tied_territory_contracts($tt_id);
}