<?php
interface iLayerManager{
	/**
	*	Returns array of plugin layers objects
	*	Args: map id
	*/
	public static function get_plugin_layers($mapid);
	/**
	*	Returns array of plugin layers objects available for current user
	*	Args: user id, map id
	*/
	public static function get_user_available_layers($user_id, $mapid);
}