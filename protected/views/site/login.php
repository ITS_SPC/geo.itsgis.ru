<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="row" style="width:400px;margin:20px auto 0 auto">
    <?php if(isset($errors)) { ?>
    <div class="row error_container">
        <div class="cols col-12">
            <?php
                foreach($errors as $error){
                    foreach($error as $field_error){ ?>
                        <span><?php echo CHtml::encode($field_error); ?></span><br>
                    <?php }
                }
            ?>
        </div>
    </div>
    <?php } ?>
	<div class="cols col-12">
		<form class='login-form' method="POST" action="?r=site/login">
            <?php if(isset($user_login_form)){
                $user_login_form = json_decode($user_login_form);
            } ?>
            <div class="row login_header">
                <div class="cols col-12" style="margin:0">
                    <span>Авторизация</span>
                </div>
            </div>
            <span class="login-lable">Логин</span>
			<input class="user-input-field" type="text" value="<?php if(isset($user_login_form)){echo CHtml::encode($user_login_form->username);} ?>" placeholder="Введите логин..." name="username">
			<span class="login-lable">Пароль</span>
            <input class="user-input-field" type="password" value="" placeholder="Введите пароль..." name="password">
    		<div class="row" style="text-align:center">
    			<div class="cols col-6">
    				<?$this->widget('CCaptcha', array('buttonLabel' => '<br>[новый код]'));?>
    			</div>
    			<div class="cols col-6">
    				<?php echo CHtml::textField('captcha', '', array('placeholder'=>'Результат выражения...'));?>
    			</div>
    		</div>
    		<div class="row">
    			<div class="cols col-12">
    				<a href="?r=site/registrationForm" style="margin:0 5px 0 0">ЗАРЕГИСТРИРОВАТЬСЯ</a>
   					<input class="submit-button" type="submit" name="okbutton" value="ВОЙТИ">
    			</div>
    		</div>
 		</form>
 	</div>
 </div>
