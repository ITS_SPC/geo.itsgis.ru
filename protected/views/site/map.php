<div class="map-controllers">
	<a href="http://its-spc.ru">
		<div class="map-controllers-button" style="background: url(img/inteltranslogo.png) no-repeat;">
		</div>
	</a>
	<div class="map-controllers-button layers" style="background: url(img/layers.png) no-repeat;">
		<div class="submenu">
			<ul>
			</ul>
		</div>
	</div>
	<div class="map-controllers-button city" style="background: url(img/<?php $current_map = CJSON::decode($map); echo mb_strtolower($current_map['map_alias']);?>.png) no-repeat;">
		<div class="submenu">
			<ul>
				<?php
    				if(isset($maps)){ 
    					for($i = 0; $i < count($maps); $i++){ ?>
    						<li><a href="?r=site/map&mapid=<?php echo $maps[$i]->get_id();?>"><?php echo $maps[$i]->get_name();?></a></li>
    					<?php } ?>
    			<?php }	?>
    		</ul>
    	</div>
	</div>
	<?php if(Yii::app()->user->getIsGuest()){ ?>
		<a href="?r=site/loginUser"><div class="map-controllers-button entry" style="background: url(img/entry.png) no-repeat;"></div></a>
	<?php } 
	else{ ?>
		<a href="?r=site/logoutUser"><div class="map-controllers-button exit" style="background: url(img/exit.png) no-repeat;"></div></a>
	<?php }?>
</div>
<div style="display:block" class="zoom-panel">
	<div id="distance-tool" class="map-measure-tool-button distance-tool" style="background: url(img/distance-tool.png) no-repeat;"></div>
	<div id="square-tool" class="map-measure-tool-button square-tool" style="background: url(img/square-tool.png) no-repeat;"></div>
	<div id="coordinates-tool" class="map-measure-tool-button square-tool" style="background: url(img/coordinates-tool.jpg) no-repeat;"></div>
	<div id="error-request-tool" class="map-measure-tool-button square-tool" style="background: url(img/error-tool.jpg) no-repeat;"></div>
</div>
<div class="zoom-panel" style="display:none; bottom: 100px;">
	<div id="inc-zoom" class="map-zoom-button" style="background: url(img/plus.png) no-repeat;"></div>
	<div id="dec-zoom" class="map-zoom-button" style="background: url(img/minus.png) no-repeat;"></div>
</div>
<!--<div class="info-panel">
<h3></h3>
<div class="info-panel-container"></div>
</div>-->
<div id="map" class="map">
</div>
<div class="measurement-block">
 <span style="font-size:14pt" id="output"></span>
 </div>
<?php //echo $mapobject;?>
<script>
/*$(document).ready*/$(function(){
var mapOptions = <?php echo $map;?>,
	layers = <?php echo $layers;?>,
	layersControlPanel = $(".map-controllers-button.layers .submenu ul");
mapModule.mapInitialization({
	mapContainer: 'map',
	mapOptions: mapOptions,
	mapLayers: layers,
	mapStyles: [{
		styleName:"objectSelection",
		pointRadius: 3,
		strokeColor: "rgb(9, 102, 131)",
		fillOpacity: 0.4,
		fillColor: "#005D70",
		strokeWidth: "3",
	}],
	mapControlls:{
		layersControl: $(".map-controllers-button.layers .submenu ul"),
		zoomIn: $('#inc-zoom'),
		zoomOut: $('#dec-zoom'),
		distanceMeasureTool: $("#distance-tool"),
		squareMeasureTool: $("#square-tool"),
		globalCoordinatesConverterTool: $("#coordinates-tool"),
		errorRequestTool: $("#error-request-tool"),
	}
});
//mapModule.mapInitialization('map', mapOptions, layers, layersControlPanel);
});
</script>
