<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="row" style="width:400px;margin:120px auto 0 auto">
	<div class="cols col-12">
		<form class='login-form' method="POST" action="?r=site/loginUser">
            <div class="row login_header">
                <div class="cols col-12" style="margin:0">
                    <span>Ваш запрос успешно отправлен!</span>
                </div>
            </div>
            <div class="row login_header">
                <div class="cols col-12" style="margin:0">
                    <span>Ожидайте ответа на указанную Вами почту</span>
                </div>
            </div>
    		<div class="row" style="margin-top:20px">
    			<div class="cols col-12" style="text-align:center">
   					<input class="submit-button" type="submit" name="okbutton" value="ВОЙТИ">
    			</div>
    		</div>
 		</form>
 	</div>
 </div>
