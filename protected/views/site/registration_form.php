<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<script>
	$(document).ready(function(){
		RegisterModule.initRegisterModule(
			{textField: "surname", errorField: "surname_error"},
			{textField: "name", errorField: "name_error"},
			{textField: "lastname", errorField: "lastname_error"},
			{textField: "login", errorField: "login_error"},
			"submit"
		);
	});
</script>
<div class="row" style="width:550px;margin:0px auto 0 auto">
    <?php if(isset($errors) && count($errors) != 0) { ?>
    <div class="row error_container">
        <div class="cols col-12">
        	<?php
        		foreach($errors as $error){
        			foreach($error as $field_error){ ?>
        				<span><?php echo CHtml::encode($field_error); ?></span><br>
        			<?php }
        		}
        	?>
        </div>
    </div>
    <?php } ?>
	<div class="cols col-12">
		<form class='registration-form' method="POST" action="?r=site/sendRegistration">
			<?php if(isset($user_registration)){
				$user_registration = json_decode($user_registration);
			} ?>
			<div class="row login_header">
        		<div class="cols col-12">
            		<span style="font-size:20px">Регистрация</span>
        		</div>
    		</div>
			<div class="row">
				<div class="cols col-4 user-data">
					<span>ФАМИЛИЯ</span>
					<input id="surname" class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->surname);} ?>" placeholder="ВВЕДИТЕ ФАМИЛИЮ..." name="surname">
					<a id="surname_error" class="error"></a>
				</div>
				<div class="cols col-4 user-data">
					<span>ИМЯ</span>
					<input id="name" class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->name);} ?>" placeholder="ВВЕДИТЕ ИМЯ..." name="name">
					<a id="name_error" class="error"></a>
				</div>
				<div class="cols col-4 user-data">
					<span>ОТЧЕСТВО</span>
					<input id="lastname" class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->lastname);} ?>" placeholder="ВВЕДИТЕ ОТЧЕСТВО..." name="lastname">
					<a id="lastname_error" class="error"></a>
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span>ЛОГИН:</span>
				</div>
				<div class="cols col-10">
					<input id="login" class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->login);} ?>" placeholder="" name="login">
					<a id="login_error" class="error"></a>
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span>ПАРОЛЬ:</span>
				</div>
				<div class="cols col-10">
					<input id="password" class="user-input-field-registration" type="password" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->password);} ?>" placeholder="ПАРОЛЬ" name="password">
					<a id="password_error" class="error"></a>
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span style="margin:0">ПОВТОРИТЕ ПАРОЛЬ:</span>
				</div>
				<div class="cols col-10">
					<input id="repeat_password" class="user-input-field-registration" type="password" value="" placeholder="ПОВТОРИТЕ ПАРОЛЬ" name="password_repeat">
					<a id="repeat_password_error" class="error"></a>
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span>ДОЛЖНОСТЬ:</span>
				</div>
				<div class="cols col-10">
					<input class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->office);} ?>" placeholder="ВАША ДОЛЖНОСТЬ" name="office">
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span>ОРГАНИЗАЦИЯ:</span>
				</div>
				<div class="cols col-10">
					<input class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->organization);} ?>" placeholder="ОРГАНИЗАЦИЯ" name="organization">
				</div>
			</div>
			<div class="row">
				<div class="cols col-2">
					<span>EMAIL:</span>
				</div>
				<div class="cols col-10">
					<input class="user-input-field-registration" type="text" value="<?php if(isset($user_registration)){ echo CHtml::encode($user_registration->email);} ?>" placeholder="EMAIL" name="email">
				</div>
			</div>
    		<div class="row" style="text-align:center">
    			<div class="cols col-6">
    				<?$this->widget('CCaptcha', array('buttonLabel' => '<br>[новый код]'));?>
    			</div>
    			<div class="cols col-6">
    				<?php echo CHtml::textField('captcha', '', array('placeholder'=>'РЕЗУЛЬТАТ ВЫРАЖЕНИЯ'));?>
    			</div>
    		</div>
    		<div class="row">
    			<div class="cols col-12" style="text-align:right">
    				<a href="?r=site/login" style="margin:0 25px 0 0">ВОЙТИ</a>
   					<input style="margin-right: 20px;" class="submit-button" type="submit" name="okbutton" value="ЗАРЕГИСТРИРОВАТЬСЯ">
    			</div>
    		</div>
 		</form>
 	</div>
 </div>
 <script type="text/javascript" src="js/register.module.js"></script>
