<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<link href="http://waymark.its-spc.ru/templates/atomic/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<!-- blueprint CSS framework -->
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.min.css" media="screen, projection">-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/map.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" media="screen, projection">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			var animatedLayers = $("#simple3D div"),
				animatedContainer = $("#simple3D");
			//BackgroundAnimationModule.initializeModule(animatedContainer, animatedLayers);
		});
	</script>
	<title>Геопортал<?php //echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<div class="wrapper">
		<!--<div id="simple3D" style="overflow: hidden;">
			<div class="layer1" style="top: 0px; left: 0px; "></div>
			<div class="layer2" style="top: 0px; left: 0px; "></div>
			<div class="layer3" style="top: 0px; left: 0px; "></div>
			<div class="layer8" style="top: 0px; left: 0px; "></div>
		</div>-->
		<div class="container">
			<div class="row header">
				<div class="cols col-4">
					<a href="http://its-spc.ru">
						<img src="img/logo.png">
					</a>
				</div>
				<div class="cols col-4" style="text-align:center">
					<div class="row">
						<div class="cols col-12">
							<a href="http://geo.itsgis.ru">
								<span class="header-logo">ГЕОПОРТАЛ</span>
							</a>
						</div>
					</div>
				</div>
				<div class="cols col-4 header-right">
					<span>Добро пожаловать, </span>
					<?php
						if(Yii::app()->user->getIsGuest()){
							$this->beginWidget('CHtmlPurifier');
								echo "Гость";
							$this->endWidget();
						}
					?>
					<?php //echo Yii::app()->user->getId();?>
					<?php 
						if(!Yii::app()->user->getIsGuest()){
					?>
							<a href='?r=site/logout' class='login'>ВЫЙТИ</a>
					<?php }?>
				</div>
			</div>
				<?php echo $content; ?>
			</div><!-- page -->
	</div>
	<div class="cover"></div>
	<div class="register-form modal-form">
		<form method="POST" action="?r=site/login">
			<input class="user-input-field" type="text" value="" placeholder="ВВЕДИТЕ ЛОГИН" name="username">
			<input class="user-input-field" type="password" value="" placeholder="ВВЕДИТЕ ПАРОЛЬ" name="password">
   			<input type="submit" name="okbutton" value="OK">
 		</form>
	</div>
	<script type="text/javascript" src="js/background.animation.module.js"></script>
</body>
</html>
