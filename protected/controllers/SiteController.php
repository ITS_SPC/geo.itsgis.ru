<?php
Yii::import('application.vendors.*');
require_once('PHPMailer/class.phpmailer.php');
class SiteController extends Controller
{
	public $is_authorized = false;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'MathCaptchaAction',
				'foreColor'=>0x005E6C,
				'minLength' => 1,
				'maxLength' => 20,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	/*public function filters(){
		return array('accessControl');
	}
	public function accessControl(){
		return array(
			array(
				'deny',
				'expression' => 'strpos($_SERVER[\'HTTP_USER_AGENT\'], \'MSIE\') !== FALSE',
				'message' => 'Вы используете неправильный браузер!'
			),
			array('deny')
		);
	}*/
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex(){
		if(isset($_POST['mapid'])){
			$map_id = $_POST['mapid'];
			$this->actionMap($map_id);
		}
		else{
			if(isset($_COOKIE["map"])){
				$map_id = $_COOKIE["map"];
				$this->actionMap($map_id);
			}
			else{
				$this->actionMap(3833856);
			}
		}
	}
	public function actionTestGetLayerName(){
		echo MapManager::get_layer_name(1);
	}
	public function actionClicker(){
		if(isset($_POST['layers']) && isset($_POST['clickPositionLon']) && isset($_POST['clickPositionLat']) && isset($_POST['mapId'])){
			$clickPositionLon = $_POST['clickPositionLon'];
			$clickPositionLat = $_POST['clickPositionLat'];
			$map_id = $_POST['mapId'];
			$switched_layers = array_reverse($_POST['layers']);
			$object = null;
			for($i = 0, $maxi = count($switched_layers); $i < $maxi; $i++){
				$object = MapManager::get_layer_object_by_coordinates($switched_layers[$i], $map_id, $clickPositionLon, $clickPositionLat);
				if($object != null){
					$i = $maxi;
				}
			}
			header('Content-Type:application/json');
			echo json_encode($object);
			//print_r($_POST['layers']);
		}
	}
	public function actionLoginUser(){
		$captcha=Yii::app()->getController()->createAction("captcha");
		$captcha->getVerifyCode(true);
       	$this->render('login');
	}
	public function actionMap($mapid){
		setcookie("map", $mapid);
		$maps = MapManager::get_available_maps($mapid);
		$map = MapManager::get_map_by_id($mapid);
		$layers = array();
		if(!Yii::app()->user->getIsGuest()){
			/*TO DO: some refactoring*/
			$user_maps = MapManager::get_user_maps(Yii::app()->user->getId());
			if(count($user_maps) == 0){
				$user_layers = LayerManager::get_user_available_layers(Yii::app()->user->getId(), $mapid);
				if(count($user_layers) == 0){
					$layers = LayerManager::get_plugin_layers($mapid);
				}
				else{
					$layers = $user_layers;
				}
			}
			else{
				$current_map_is_available = false;
				for($i = 0, $maxi = count($user_maps); $i < $maxi; $i++){
					if($user_maps[$i]->get_id() == $mapid){
						$current_map_is_available = true;
						$i = count($user_maps);
					}
				}
				if($current_map_is_available){
					$user_layers = LayerManager::get_user_available_layers(Yii::app()->user->getId(), $mapid);
					if(count($user_layers) == 0){
						$layers = LayerManager::get_plugin_layers($mapid);
					}
					else{
						$layers = $user_layers;
					}
				}
			}
			
			//$layers = LayerManager::get_user_available_layers(Yii::app()->user->getId()/*37158916*/, $mapid);
			//if(count($layers) == 0){
			//	$layers = LayerManager::get_plugin_layers($mapid);
			//}
		}
		$this->layout = 'map';
       	$this->render('map', array('map'=>CJSON::encode($map), 'layers'=>CJSON::encode($layers), 'maps' => $maps));
	}
	public function actionGlobalCoordinatesProxy() {
		header('Content-Type:application/json');
		$point = new Point();
		if (isset($_POST['latitude']) && isset($_POST['longitude']) && isset($_POST['map'])) {
			$point->set_x($_POST['latitude']);
			$point->set_y($_POST['longitude']);
			$map_id = $_POST['map'];
			$request = file_get_contents("http://app.itsgis.ru:8732/?map=".$map_id."&x=".$point->get_y()."&y=".$point->get_x());
			//parse req
			$request = explode("\n", $request);
			$point->set_x(str_replace(',', '.', $point->get_x()));
			$point->set_y(str_replace(',', '.', $point->get_y()));
			$point->set_latitude($request[0]);
			$point->set_longitude($request[1]);
			echo json_encode($point);
		} else {
			echo json_encode($point);
		}
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
	public function actionRegistrationForm(){
		$errors = array();
		$user_registration = "";
		if(isset($_GET['errors'])){
			$errors = $_GET['errors'];
		}
		if(isset($_GET['user_registration'])){
			$user_registration = $_GET['user_registration'];
		}
		$this->render('registration_form', array('errors' => $errors, 'user_registration' => $user_registration));
	}
	public function actionSendFeedback() {
		if (isset($_POST['sender_name']) && isset($_POST['sender_email']) && isset($_POST['feedback_comment'])) {
			$attachment_error = false;
			$feedback_form = new MapFeedbackForm;
			$address = new Address;
			$feedback_form->set_sender_name($_POST['sender_name']);
			$feedback_form->set_sender_email($_POST['sender_email']);
			$feedback_form->set_sender_comments($_POST['feedback_comment']);

			$address->set_district((isset($_POST['feedback_district'])) ? $_POST['feedback_district'] : "none");
			$address->set_street_house((isset($_POST['feedback_street'])) ? $_POST['feedback_street'] : "none");

			$feedback_form->set_lon($_POST['feedback_lon']);
			$feedback_form->set_lat($_POST['feedback_lat']);

			$feedback_form->set_address($address);

			if ($feedback_form->validate()) {
				$mail = new PHPMailer();
				$mail->CharSet = "Utf-8";
				$mail->From = $feedback_form->get_sender_email();
				$mail->FromName = $feedback_form->get_sender_name();
				$mail->WordWrap = 50;
				$mail->IsHTML(true);
				$mail->AddAddress($feedback_form->get_sender_email(), $feedback_form->get_sender_name());
				$mail->Subject = "Ошибка на карте";
				$mail->Body = $feedback_form->get_message();
				//try to attach file
				if (isset($_FILES["screenshot"]["name"])) {
					$whitelist = array(".jpg", ".jpeg", ".gif", ".png");
					$file_name = $_FILES["screenshot"]["name"];
					$file_type = $_FILES["screenshot"]["type"];
					$file_ext = strrchr($file_name, '.');
					$pos = strpos($file_name, 'php');
					if (!($pos === false)) {
						$attachment_error = true;
					} else {
						if (!(in_array($file_ext, $whitelist))) {
							$attachment_error = true;
						} else {
							$pos = strpos($file_type, "image");
							if ($pos === false) {
								$attachment_error = true;
							} else {
								$image_info = getimagesize($_FILES["screenshot"]["tmp_name"]);
								if ($image_info['mime'] != 'image/gif' && $image_info['mime'] != 'image/jpeg' && $image_info['mime'] != 'image/jpg' && $image_info['mime'] != 'image/png') {
									$attachment_error = true;
								} else {
									if (substr_count($file_type, '/') > 1) {
										$attachment_error = true;
									}
								}
							}
						}
					}
					if (!$attachment_error) {
						$file_name = $_FILES["screenshot"]["name"];
						$file_name = date("Y-m-d H:i:s");
						$file_name = str_replace( " ", "_", $file_name);
						$file_name = str_replace( ":", "-", $file_name);
						$uploadfile =  $file_name.$file_ext;
						if (move_uploaded_file($_FILES['screenshot']['tmp_name'], "./feedback/".$uploadfile)) {
							$mail->AddAttachment("./feedback/".$uploadfile);
						}
					}
				}
				if (!$mail->Send()) {
  					echo 'Невозможно отправить сообщение!';
				} else {
  					echo 'Сообщение отправлено!';
				}
			} else {
				header('Content-Type:application/json');
				echo json_encode($feedback_form->errors);
			}
		} else {
			echo "Ошибка при отправке почты!";
		}
	}
	public function actionSendRegistration(){
		if(isset($_POST['surname']) && isset($_POST['name']) && isset($_POST['lastname']) && isset($_POST['login']) && isset($_POST['password']) && isset($_POST['password_repeat']) && isset($_POST['office']) && isset($_POST['organization']) && isset($_POST['email'])){
			$user_registration = new UserRegistrationForm;
			$errors = array();
			$captcha=Yii::app()->getController()->createAction("captcha");
			$code = $captcha->verifyCode;
			$user_registration->set_surname($_POST['surname']);
			$user_registration->set_name($_POST['name']);
			$user_registration->set_lastname($_POST['lastname']);
			$user_registration->set_login($_POST['login']);
			$user_registration->set_password($_POST['password']);
			$user_registration->set_office($_POST['office']);
			$user_registration->set_organization($_POST['organization']);
			$user_registration->set_email($_POST['email']);
			$user_registration->set_retyped_password($_POST['password_repeat']);
			$user_registration->set_captcha($_POST['captcha']);
			if($user_registration->validate()){
				$to  = 'arslan92@mail.ru';
				$subject = 'Запрос на регистрацию';
				$message = '
				<html>
					<head>
  						<title>Запрос на регистрацию</title>
  						<meta http-equiv="content-type" content="text/html; charset=utf-8">
  						<style>
  							.link-cell a {
  								color: white!important;
  							}
  						</style>
					</head>
					<body>
						<div style="width:100%;float:left;margin:20px auto;background:#337D8D;color:white">
							<div style="margin:20px auto; width:50%;text-align:center">
								<img src="http://geo.itsgis.ru/img/logo.png">
								<span style="margin:20px auto; display:block">УЧЕТНЫЕ ДАННЫЕ ПОЛЬЗОВАТЕЛЯ</span>
							</div>
  							<table style="width:50%; margin:30px auto">
    							<tr style="text-align:center;">
      								<td style="width:100px;border: 1px solid white;">ФИО</td>
      								<td style="width:100px;border: 1px solid white;">'.$user_registration->get_surname().' '.$user_registration->get_name().' '.$user_registration->get_lastname().'</td>
    							</tr>
    							<tr style="text-align:center">
    								<td style="width:100px;border: 1px solid white;">Логин</td>
      								<td style="width:100px;border: 1px solid white;">'.$user_registration->get_login().'</td>
    							</tr>
    							<tr style="text-align:center">
    								<td style="width:100px;border: 1px solid white;">Пароль</td>
      								<td style="width:100px;border: 1px solid white;">'.$user_registration->get_password().'</td>
    							</tr>
    							<tr style="text-align:center">
    								<td style="width:100px;border: 1px solid white;">Организация</td>
      								<td style="width:100px;border: 1px solid white;">'.$user_registration->get_organization().'</td>
    							</tr>
    							<tr style="text-align:center">
    								<td style="width:100px;border: 1px solid white;">Должность</td>
      								<td style="width:100px;border: 1px solid white;">'.$user_registration->get_office().'</td>
    							</tr>
    							<tr style="text-align:center">
    								<td style="width:100px;border: 1px solid white;">E-mail</td>
      								<td class="link-cell" style="width:100px;border: 1px solid white;"><a style="color:white">'.$user_registration->get_email().'</a></td>
    							</tr>
  							</table>
  						</div>
					</body>
				</html>';
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				$headers .= 'To: ITSSPC <arslan92@mail.ru>' . "\r\n";
				$headers .= 'From: '.$user_registration->get_surname().' '.$user_registration->get_name().' '.$user_registration->get_lastname().' <'.$user_registration->get_email().'>' . "\r\n";
				$success = mail($to, $subject, $message, $headers);
				if(!$success){
					$user_registration->addError('message_send', 'Ошибка при отправке запроса! Попробуйте еще раз.');
					$captcha->getVerifyCode(true);
					$this->redirect(array('RegistrationForm', 'errors' => $user_registration->errors, 'user_registration' => json_encode($user_registration)));
				}
				else{
					$this->render('success');
				}
			}
			else{
				$this->redirect(array('RegistrationForm', 'errors' => $user_registration->errors, 'user_registration' => json_encode($user_registration)));
			}
		}
	}
	public function actionLogin(){
		if(isset($_POST['username']) && isset($_POST['password'])){
			$user_login_form = new UserLoginForm;
			$user_login_form->set_username($_POST['username']);
			$user_login_form->set_password($_POST['password']);
			$user_login_form->set_captcha($_POST['captcha']);
			if($user_login_form->validate()){
				Yii::app()->user->login($user_login_form->get_identity());
        		$this->redirect(array('Index'));
			}
			else{
				$this->render('login', array('errors' => $user_login_form->errors, 'user_login_form' => json_encode($user_login_form)));
			}
		}
		else{
			$this->render('login');
		}
	}
	public function actionLogoutUser(){
		Yii::app()->user->logout();
		$this->redirect(array('Index'));
	}
	public function actionRegistration(){
		$work_time = array();
		$work_time = OrganizationManager::get_organization_work_time(11304974);
		print_r($work_time);
		/*$work_time = array();
		$work_time['monday'] = array('start_time' => '08:00:00', 'end_time' => '19:00:00');
		$work_time['thusday'] = array('start_time' => '08:00:00', 'end_time' => '19:00:00');
		$work_time['wednesday'] = array('start_time' => '08:00:00', 'end_time' => '19:00:00');
		$work_time['sunday'] = array('start_time' => '08:00:00', 'end_time' => '19:00:00');
		$days = "";
		foreach ($work_time as $key => $value) {
			$days .= $key." ";
		}
		echo $days;*/
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionGeoParth(){
		$geoPathFiles = array();
		$files = scandir("geopath/");
		for($i = 0, $maxi = count($files); $i < $maxi; $i++){
			$rows = array();
			$file = file_get_contents('geopath/'.$files[$i], true);
			$rows = split ( "\n" , $file );
			$coordinates = array();
			for($j = 0, $maxj = count($rows); $j < $maxj; $j++){
				if(strstr($rows[$j], "[G]")){
					$coordinatesPare = array();
					preg_match('[N(\d+)\S(\d+)]i',$rows[$j],$foundN);
					preg_match('[E(\d+)\S(\d+)]i',$rows[$j],$foundE);
					array_push($coordinatesPare, preg_replace('/[^0-9.]/', '', $foundN[0]));
					array_push($coordinatesPare, preg_replace('/[^0-9.]/', '', $foundE[0]));
					array_push($coordinates, $coordinatesPare);
				}
			}
			array_push($geoPathFiles, $coordinates);
		}
		header('Content-Type:application/json');
		echo json_encode($geoPathFiles);
	}
}