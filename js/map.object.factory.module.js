var MapObjectFactoryModule = (function(){
	function MapObject(){
	} 
	MapObject.factory = function (type, config) {
		var constructor = type,
			newObject;
		if (typeof MapObject[constructor] !== "function") {
			throw {
				name: "Error",
				message: constructor + " doesn't exist!";
			}
		}
		newObject = new MapObject[constructor](config);

		return newObject;
	}
	MapObject.TiedTerritory = function (config) {
		var configuration = config || {};

		if (typeof configuration === "object") {
			for (var key in configuration) {
				this.key = configuration[key];
			}
			this.popup_tmpl = new EJS({url: 'templates/tt_popup_template.ejs'}).render(this);
			this.info_template = new EJS({url: 'templates/tt_info_template.ejs'}).render(this);
		}
	}
	MapObject.CablePost = function (config) {
	}
	MapObject.RoadMarking = function (config) {
	}
	MapObject.Barricado = function (config) {
	}
	MapObject.Busstop = function (config) {
	}
	MapObject.Post = function (config) {
	}
	MapObject.Advertisement = function (config) {
	}
	MapObject.House = function (config) {
	}
	MapObject.Street = function (config) {
	}
	MapObject.Crossroad = function (config) {
	}

	return {
		factory: function(type, config) {
			return MapObject.factory(type, config);
		},
	}
})();