var MapUIModule = (function(){
		var toggleControlPanel = function(){
			var layerListContainer = $(".map-layer-list-container");
			if(layerListContainer.height() != 0){
				layerListContainer.animate({"height":"0"}, 300);
			}
			else{
				layerListContainer.animate({"height":"100"}, 300);
			}
			/*else{
				layerListContainer.height() = 'auto';
			}*/
		},
		toggleMapControlPanel = function(){
			var layerListContainer = $(".map-list-container");
			if(layerListContainer.height() != 0){
				layerListContainer.animate({"height":"0"}, 300);
			}
			else{
				layerListContainer.animate({"height":"100"}, 300);
			}
			/*else{
				layerListContainer.height() = 'auto';
			}*/
		};
		return{
			mapUIModuleInitialization: function(layersContainer, mapsContainer, loginButton){
				layersContainer.on("click", toggleControlPanel);
				mapsContainer.on("click", toggleMapControlPanel);
			}/*,
			getMap: getMap,
			selectedObjectLayer: selectedObjectLayer*/
		}
}());