var BackgroundAnimationModule = (function(){
	var layers,
		layersContainer,
		changePosition = function(){
			var top = 0,
				left = 0;
			left = $(window).width();
			top = $(window).height();
			layers.each(function(){
				$(this).animate({left: left, top: top}, (Math.random() * (100000 - 50000) + 50000), 'linear', function() {
					$(this).css('left', -$(window).width());
					$(this).css('top', -$(window).height());
					changePosition();
  				});
			});
		},
		initLayers = function(layers){
			layers.each(function(){
				$(this).css('left', -$(window).width());
				$(this).css('top', -$(window).height());
			});
		},
		onWindowResize = function(){
			layersContainer.css("height", $(window).height());
		};
		return{
			initializeModule: function(animatedContainer, animationLayers){
				layersContainer = animatedContainer;
				layers = animationLayers;
				layersContainer.css("height", $(window).height());
				$(window).resize(onWindowResize);
				initLayers(animationLayers);
				changePosition();
			},
			changePosition: changePosition
		}
}());