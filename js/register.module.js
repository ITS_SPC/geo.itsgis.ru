var RegisterModule = (function(){
  var  loginUser = function(){
  	var loginForm = $('.register-form'),
  		  coverWrapper = $('.cover');
  	coverWrapper.css({display:"block"});
  	loginForm.animate({left:0},700);
  },
  checkSurnameField = function(event){
    if(RegisterModule.checkLang($(this).val(), "ru")){
      login = event.data.loginField.val().split(".");
      login[1] = $(this).val().translit().toLowerCase();
      event.data.loginField.val(login[0] + "." + login[1]);
      event.data.errorField.text("");
      event.data.errorField.slideUp(300);
      event.data.loginErrorField.slideUp(300);
    }
    else{
      $(this).val("");
      event.data.errorField.text("Введите буквы кириллицей");
      event.data.errorField.slideDown(300);
    }
  },
  checkNameField = function(event){
    if(RegisterModule.checkLang($(this).val(), "ru")){
      event.data.loginField.val($(this).val()[0].translit().toLowerCase() + "." + event.data.surnameField.val().translit().toLowerCase());
      event.data.errorField.text("");
      event.data.errorField.slideUp(300);
      event.data.loginErrorField.slideUp(300);
    }
    else{
      $(this).val("");
      event.data.errorField.text("Введите буквы кириллицей");
      event.data.errorField.slideDown(300);
    }
  },
  checkLastNameField = function(event){
    if(RegisterModule.checkLang($(this).val(), "ru")){
      event.data.errorField.text("");
      event.data.errorField.slideUp(300);
    }
    else{
      $(this).val("");
      event.data.errorField.text("Введите буквы кириллицей");
      event.data.errorField.slideDown(300);
    }
  },
  checkLoginField = function(event){
    if(RegisterModule.checkLang($(this).val(), "en")){
      event.data.errorField.text("");
      event.data.errorField.slideUp(300);
    }
    else{
      $(this).val("");
      event.data.errorField.text("Введите буквы латиницей");
      event.data.errorField.slideDown(300);
    }
  },
  checkLang = function(text, lang){
    if((typeof lang === "string") && (typeof text === "string")){
      switch(lang){
        case "ru":{
          if(/[^А-яЁё]/.test(text)){
            return false;
          }
          else{
            return true;
          }
        }break;
        case "en":{
          if(/[^A-z.]/.test(text)){
            return false;
          }
          else{
            return true;
          }
        }break;
        default:{
          return false;
        }break;
      }
    }
    else{
      return false;
    }
  };
  return {
    initRegisterModule: function(surname, name, lastname, login, submit){
      $("#" + surname.textField).on('change', {loginField: $("#" + login.textField), errorField: $("#" + surname.errorField), loginErrorField: $("#" + login.errorField)}, checkSurnameField);
      $("#" + name.textField).on('change', {surnameField: $("#" + surname.textField), loginField: $("#" + login.textField), errorField: $("#" + name.errorField)}, checkNameField);
      $("#" + lastname.textField).on('change', {loginField: $("#" + login.textField), errorField: $("#" + lastname.errorField)}, checkLastNameField);
      $("#" + login.textField).on('change', {errorField: $("#" + login.errorField)}, checkLoginField);
      String.prototype.translit = (function(){
        var L = {
                  'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
                  'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
                  'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
                  'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
                  'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
                  'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
                  'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
                  'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
                  'Я':'Ya','я':'ya'},
            r = '',
            k;
        for (k in L) r += k;
        r = new RegExp('[' + r + ']', 'g');
        k = function(a){
          return a in L ? L[a] : '';
        };
        return function(){
          return this.replace(r, k);
        };
      })();
    },
    checkLang: checkLang
  };
}());