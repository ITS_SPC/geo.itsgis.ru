var mapModule = (function(){
		var configuration,
			getConfiguration = function() {
				return configuration;
			},
         	getMap = function(){
         		return configuration.map;
         	},
			getTilesUrl = function(bounds){
				//console.log(bounds);
				/*параметр res - разрешение карты на текущем зуме (количество метров в одном пикселе)*/
				var result = "",
    				res = this.map.getResolution(),
    				x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w)),
    				y = Math.round((this.maxExtent.top - bounds.top) / (res * this.tileSize.h)),
				/*+zoomOffset (4)*/
    				z = this.map.getZoom() + 4,
    				path = "tile-" + z + "-" + x + "-" + y + "." + this.type,
    				url = this.url;
    				if(this.isBaseLayer){
    					path = "tile-" + z + "-" + x + "-" + y + "." + this.type;
    				}
    				else{
    					if(this.name == "Streets"){
    						path = "tile-" + z + "-" + x + "-" + y + "." + this.type;
    					}
    					else{
    						path = "&tile=tile-" + z + "-" + x + "-" + y;
    					}
    				}
    			if(url instanceof Array){
        			url = this.selectUrl(path, url);
    			}
    			result = url + path;
    			return result;
			},
			getMapAreaTiles = function(longitude, latitude){
        		var tileNamePattern = new RegExp('tile+\-+[0-9]+\-+[0-9]+\-+[0-9]+'),
        			//tilesAreaUrls = new Array(),
        			imagesOptions = new Array();

				for (var i = 0, maxi = configuration.map.layers.length; i < maxi; i++) {
					if(configuration.map.layers[i].className == "olLayerGrid" && configuration.map.layers[i].getTileData(new OpenLayers.LonLat(longitude, latitude))){
						var tileUrl = getTilesUrl.apply(configuration.map.layers[i], [configuration.map.layers[i].getTileData(new OpenLayers.LonLat(longitude, latitude)).tile.bounds]);
						var myArray = tileNamePattern.exec(tileUrl);
						var tile = myArray[0].split("-");
						var x = tile[2], y = tile[3], posX = 0, posY = 0;
						x = x - 1;
						y = y - 1;
						for(var gi = 0; gi < 3; gi++){
							for(var gj = 0; gj < 3; gj++){
								
								//tilesAreaUrls.push("<img style='width: 130px; height: 130px;position:absolute;top:" + posX + "px;left:" + posY + "px' src='"+tileUrl.replace(new RegExp('tile+\-+[0-9]+\-+[0-9]+\-+[0-9]+'), tile[0] + "-" + tile[1] + "-" + x + "-" + y)+"'>");
								

								imagesOptions.push({
									imgSrc: tileUrl.replace(new RegExp('tile+\-+[0-9]+\-+[0-9]+\-+[0-9]+'), tile[0] + "-" + tile[1] + "-" + x + "-" + y),
									imgWidth: 130,
									imgHeight: 130,
									posX: posX,
									posY: posY,
								});
								
								x += 1;
								posY += 130;
							}
							x = x - 3;
							y += 1;
							posY = 0;
							posX += 130; 
						}
					}
				}
				return /*tilesAreaUrls*/imagesOptions;
			},
			/**
			*	layerObject{
			*		id:Number,		
			*		alias:"",
			*		visibility: true/false,
			*		tiles_path: "",
			*		tiles_type: "",
			*		get_tiles_function: function,
			*		
			*	}
			*/
			addTMSLayers = function(layerObjects){
				if(layerObjects && layerObjects.length){
					for (var i = 0, maxi = layerObjects.length; i < maxi; i++) {
						var layer = new OpenLayers.Layer.TMS(layerObjects[i].alias, layerObjects[i].tiles_path, layerObjects[i].layer_options);
						layer.id = layerObjects[i].id;
						layer.setVisibility(layerObjects[i].visibility);
						configuration.map.addLayer(layer);
					}
				}
			},
			bindEvents = function(){
				//configuration.map.events.register('click', configuration.map, mapClickEventHandler);
				configuration.map.events.register('zoomstart', configuration.map, handleZoom);
				configuration.map.events.register('zoomend', configuration.map, handleZoomEnd);

				$(".map-controllers-button.layers .submenu input").on("change", changeLayer);
				configuration.mapControlls.zoomIn.click(increaseZoom);
				configuration.mapControlls.zoomOut.click(decreaseZoom);

				//square measurement tool (squareMeasureTool.clickTrigger - static parameter for click counter)

				//distance measurement tool (distanceMeasureTool.clickTrigger - static parameter for click counter)
				/*configuration.mapControlls.distanceTool.click(distanceMeasureTool);
				distanceMeasureTool.clickTrigger = 0;*/
			},
			drawPoint = function(xCoordinate, yCoordinate, layer){
				var point = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(xCoordinate, yCoordinate),null, {
            			strokeColor: "red",
            			strokeWidth: 2,
            			strokeDashstyle: "",
            			pointRadius: 1,
            			pointerEvents: "visiblePainted",
            			strokeOpacity: 1
         		});
				layer.addFeatures([point]);
			},
			drawBorders = function(geometry, layer){
				var prev_coord = new Array();
				for (var i = (geometry.length - 1); i--;) {
					var linestring = geometry[i].split(',');
					prev_coord = new Array();
					for (var j = (linestring.length); j--;) {
						var lon_lat = linestring[j].split(' ');
						if(prev_coord.length === 0){
							prev_coord = lon_lat;
						}
						else{
							var line_points = [new OpenLayers.Geometry.Point(prev_coord[0],prev_coord[1]), new OpenLayers.Geometry.Point(lon_lat[0],lon_lat[1])];
			 				var line = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(line_points),null,border_style);
			 				layer.addFeatures([line]);
			 				prev_coord = lon_lat;
						} 
					}
				}
			},
			drawObject = function(geometriesWKT, layer){
				var wkt, feature;
				wkt = new OpenLayers.Format.WKT();
				feature = wkt.read(geometriesWKT);
				feature.style = {pointRadius: 3, strokeColor: "rgb(9, 102, 131)", fillOpacity: 0.4, fillColor: "#005D70", strokeWidth: "3"};
				layer.addFeatures([feature]);
			},
			createPopup = function(map, feature, layer){
  				for (var i = 0, max = layer.features.length; i < max; i++) {
     				if (layer.features[i].popup == null) continue;
     				destroyPopup(layer.features[i]);
  				}
          		feature.popup = new OpenLayers.Popup.FramedCloud("pop",feature.geometry.getBounds().getCenterLonLat(),null,'<div class="markerContent">'+feature.attributes.description+'</div>',null,true, function() {destroyPopup(feature)}); 
          		map.addPopup(feature.popup);
			},
			destroyPopup = function(feature){
				feature.popup.destroy();
				feature.popup = null;
			},
			mapSnap = function(x, y, svgId, lon, lat){
				var snapPaper, circle;
				snapPaper = Snap(svgId);
				if(snapPaper.circle !== undefined){
					circle = snapPaper.circle(x, y, 0);
					circle.attr({
						fill: "white",
						stroke: "white",
						opacity: 0.6
					});
					circle.animate({r:10}, 200, mina.easein, function(){circle.animate({r:0},200,mina.easein); setTimeout(function(){circle.remove()},210);});
				}
				var size = new OpenLayers.Size(25, 25),
					offset = new OpenLayers.Pixel(-(size.w/2), -(size.h/2)),
					icon = new OpenLayers.Icon('img/loading.gif',size,offset);
            	configuration.map.getLayersBy("name", "Markers")[0].addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(lon, lat),icon));
			},
			getOrganizationText = function(iNumber, aEndings){
    			var sEnding, i;
    			iNumber = iNumber % 100;
    			if (iNumber>=11 && iNumber<=19) {
        			sEnding=aEndings[2];
    			}
    			else {
        			i = iNumber % 10;
        			switch (i){
            			case (1): sEnding = aEndings[0]; break;
            			case (2):
            			case (3):
            			case (4): sEnding = aEndings[1]; break;
            			default: sEnding = aEndings[2];
        			}
    			}
    			return sEnding;
			},
			mapClickEventHandler = function(evt){
				//getTileData
				
				var clickPositionLon = 0,
					clickPositionLat = 0,
					layerContainer,
					svgId,
					layersId = [];
					///*evt.target.nodeName*/evt.element.nodeName
				/*--------todo: optimized for ie8-----------*/
				if(typeof evt.target != 'undefined'){
					if(evt.target.nodeName === "DIV"){
						configuration.map.getLayersBy("name", "Object")[0].destroyFeatures();
						//selectedObjectLayer.destroyFeatures();
						while(configuration.map.popups.length){
							configuration.map.removePopup(configuration.map.popups[0]);
						}
						return;
					}
				}
				/*------------------------------------------*/
				clickPositionLon = configuration.map.getLonLatFromViewPortPx(evt.xy).lon;
				clickPositionLat = configuration.map.getLonLatFromViewPortPx(evt.xy).lat;

				/*------map feedback---------*/
				/*
				var myRe = new RegExp('tile+\-+[0-9]+\-+[0-9]+\-+[0-9]+');
				var myArray = myRe.exec("http://app.itsgis.ru/municipal-geoportal/samara/map/tile-6-39-45.jpg");
				console.log(myArray);
				*/
				

				/*---------------------------*/

				configuration.map.getLayersBy("name", "Object")[0].destroyFeatures();
				//selectedObjectLayer.destroyFeatures();
				configuration.map.getLayersBy("name", "Markers")[0].removeMarker(configuration.map.getLayersBy("name", "Markers")[0].markers[0]);
				//loadingObjectLayer.removeMarker(loadingObjectLayer.markers[0]);
				while(configuration.map.popups.length){
					configuration.map.removePopup(configuration.map.popups[0]);
				}
				layerContainer = document.getElementById(/*selectedObjectLayer.id*/configuration.map.getLayersBy("name", "Object")[0].id);
				svgId = "#" +  layerContainer.childNodes[0].id;
				/*--------todo: optimized for ie8-----------*/
				mapSnap(evt.clientX, evt.clientY, svgId, clickPositionLon, clickPositionLat);
				/*------------------------------------------*/
				for(var i = 0, maxLayers = configuration.map.layers.length; i < maxLayers; i++){
					/*--add only switched plugin layers---*/
					if(configuration.map.layers[i].visibility && (/*typeof map.layers[i].id !== "string"*/Number(configuration.map.layers[i].id))){
						layersId.push(configuration.map.layers[i].id);
					}
				}
				//console.log(configuration.mapOptions.map_id);
				$.post('?r=site/Clicker',{
						//send map layers id
						'layers': layersId,
						'mapId': configuration.mapOptions.map_id,
						'clickPositionLon': clickPositionLon,
						'clickPositionLat': clickPositionLat
						},
						//callback function
						function (object) {
							configuration.map.getLayersBy("name", "Markers")[0].removeMarker(configuration.map.getLayersBy("name", "Markers")[0].markers[0]);
							//console.log(object);
							if(object != null){
								if(object.object_type != null){
									for(var i = 0, maxgeom = object.geometries.length; i < maxgeom; i++){
										drawObject(object.geometries[i], configuration.map.getLayersBy("name", "Object")[0]);
									}
									switch(object.object_type){
										case 'tied_territory':{
											object.popup_tmpl = new EJS({url: 'templates/tt_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/tt_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											object.address = {};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var newTemplate = TemplateModule.CreateTiedTerritoryTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'cable_post':{
											console.log(object);
											object.popup_tmpl = new EJS({url: 'templates/cable_post_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/cable_post_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var lightPostTemplate = TemplateModule.CreateLightPostTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'road_marking':{
											object.popup_tmpl = new EJS({url: 'templates/road_marking_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/road_marking_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											object.address = {};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var roadMarkingTemplate = TemplateModule.CreateRoadMarkingTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'barricado':{
											object.popup_tmpl = new EJS({url: 'templates/barricado_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/barricado_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var barricadoTemplate = TemplateModule.CreateBarricadoTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'busstop':{
											object.popup_tmpl = new EJS({url: 'templates/busstop_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/busstop_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var busstopTemplate = TemplateModule.CreateBusstopTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'post':{
											object.popup_tmpl = new EJS({url: 'templates/post_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/post_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var signPostTemplate = TemplateModule.CreateSignPostTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});

										}break;
										case 'advertisement':{
											object.popup_tmpl = new EJS({url: 'templates/advertisement_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/advertisement_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var advertisementTemplate = TemplateModule.CreateAdvertisementTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});
										}break;
										case 'house':{
											object.popup_tmpl = new EJS({url: 'templates/house_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/house_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var houseTemplate = TemplateModule.CreateHouseTemplate(object);
											var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
												mapClickCoordinates: {
													lon: clickPositionLon,
													lat: clickPositionLat,
												},
												imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
												info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render(object),
											});

										}break;
										case 'street':{
											object.popup_tmpl = new EJS({url: 'templates/street_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/street_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var streetTemplate = TemplateModule.CreateStreetTemplate(object);
										}break;
										case 'crossroad':{
											object.popup_tmpl = new EJS({url: 'templates/crossroad_popup_template.ejs'}).render(object);
											object.info_template = new EJS({url: 'templates/crossroad_info_template.ejs'}).render(object);
											object.clickCoordinates = {
												lon: clickPositionLon,
												lat: clickPositionLat,
											};
											pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(clickPositionLon, clickPositionLat), {description: object.popup_tmpl}, {pointRadius: 0});
											configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
											createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
											var crossroadTemplate = TemplateModule.CreateCrossroadTemplate(object);
										}break;
									}
								}
							}
						}
				);
			},
			handleZoomEnd = function(event){
			},
			handleZoom = function(event){
				/*----todo----*/
				var visibleLayers = configuration.map.getLayersBy("isBaseLayer", false);
				for(var i = 0, maxi = visibleLayers.length; i < maxi; i++){
					if(!visibleLayers[i].visibility) continue;
					visibleLayers[i].redraw();
				}
				/*var layer = map.getLayersBy("id", 101)[0];
				var layer2 = map.getLayersBy("id", 105)[0];
				layer.redraw();
				layer2.redraw();*/
				/*-------------*/

				/*layer.destroy();
				layer2.destroy();

				var newLayer = new OpenLayers.Layer.TMS("Опоры",'http://app.itsgis.ru:8732/?map=27295744&layer=101',{type : "png",getURL : getTilesUrl,isBaseLayer:false});
					newLayer.id = 101;
					newLayer.setVisibility(true);
					map.addLayer(newLayer);
				var newLayer2 = new OpenLayers.Layer.TMS("Опоры",'http://app.itsgis.ru:8732/?map=27295744&layer=105',{type : "png",getURL : getTilesUrl,isBaseLayer:false});
					newLayer2.id = 105;
					newLayer2.setVisibility(true);
					map.addLayer(newLayer2);*/
				//if(map.getZoom() < 7){
					//console.log("Событие " + event);
					//event.stop();
				//}
			},
			initializeLayerController = function(layers, layersControlPanel, mapOptions){
				layersControlPanel.append( "<label><li><input id='" + Number(mapOptions.map_id) + "' type='checkbox' value='layer_" + Number(mapOptions.map_id) + "' name='" + "Основной слой" + "' checked>" + "<span>" + "Основной слой" + "</span></li></label>" );
				layersControlPanel.append( "<label><li><input id='" + Number(mapOptions.map_id) + "_street" + "' type='checkbox' value='layer_" + Number(mapOptions.map_id) + "_street" + "' name='" + "Подписи зданий и улиц" + "' checked>" + "<span>" + "Подписи зданий и улиц" + "</span></li></label>" );
				if(layers && layers.length){
					for(var i = 0, max = layers.length; i < max; i++){
						if((layers[i].alias != "Паспортизация") && (layers[i].alias != "Узлы") && (layers[i].alias != "Участки") && (layers[i].alias != "Дуги") && (layers[i].alias != "Маршруты")){
							layersControlPanel.append( "<label><li><input id='" + layers[i].layerid + "' type='checkbox' value='layer_" + layers[i].layerid + "' name='" + ((layers[i].alias != 'Дуги') ? (layers[i].alias) : 'Интенсивность') + "'>" + "<span>" + ((layers[i].alias != 'Дуги') ? (layers[i].alias) : 'Интенсивность') + "</span></li></label>" );
						}
					}
				}
				$("span.map-container-header").text(mapOptions.map_name);
			},
			changeLayer = function(){
				var layerid = $(this).attr("id");
         		if($(this).prop("checked")){
         			/*if( (map.getLayersBy("id", $(this).attr("id"))[0].features != undefined) && (map.getLayersBy("id", $(this).attr("id"))[0].features.length == 0)){
         				jQuery.ajax({
							type: "POST",
							url: "?r=site/getLayerObjects",
							cash: false,
							async: true,
							data: {"layerid": layerid},
							success: function(response){
								var switchedLayer = map.getLayersBy("id", layerid)[0];
								for(var i = 0; i < response.length; i++){
									drawObject(response[i].geometry, switchedLayer);
								}
							}
						});
					}*/
         			configuration.map.getLayersBy("id", $(this).attr("id"))[0].setVisibility(true);
         		}
         		else{
         			//console.log($(this).attr("id"));
         			configuration.map.getLayersBy("id", $(this).attr("id"))[0].setVisibility(false);
         		}
         	},
         	increaseZoom = function(){
         		configuration.map.zoomIn();
         	},
         	decreaseZoom = function(){
         		configuration.map.zoomOut();
         	},
         	distanceMeasureTool = function(){
         		console.dir(distanceMeasureTool.clickTrigger++);
         	};
        /*------------------Tools factory--------------------------*/
        /**
        *	Tool maker
        */
        function MapToolMaker() {
        }
        MapToolMaker.prototype.aboutMe = function() {
        	return "I'm " + this.toolName;
        };
        /**
        * Tool factory method
        */
        MapToolMaker.factory = function(toolType, controls) {
        	var constr = toolType,
        		newTool;
        	if (typeof MapToolMaker[toolType] !== "function") {
        		throw {
        			name: "Error",
        			message: constr + " doesn't exist",
        		};
        	}
        	newTool = new MapToolMaker[constr](controls);
        	return newTool;
        };
        /**
        * Tools mediator (need singletone)
        */
        MapToolMaker.ToolsMediator = function(controls) {
        	this.tools = configuration.toolsContainer.tools;
        };
        MapToolMaker.ToolsMediator.prototype.toolActivation = function() {
        	//console.log(this.tools);
        	for (var tool in this.tools) {
        		this.tools[tool].deactivateTool();
        	}
        };
        MapToolMaker.ToolsMediator.prototype.toolActivated = function(activatedTool) {
        	//deactivate other tools
        	for (var tool in this.tools) {
        		if (this.tools[tool].toolName !== activatedTool.toolName) {
        			this.tools[tool].deactivateTool();
        		}
        	}
        };
        MapToolMaker.ToolsMediator.prototype.toolDeactivated = function() {
        	//check, if all tools deactivated -> activate clicker
        	var isAllDeactivated = true;
        	for (var tool in this.tools) {
        		if (this.tools[tool].toolStatus) {
        			isAllDeactivated = false;
        		}
        	}
        	if (isAllDeactivated) {
        		this.tools['clickerTool'].activateTool();
        	}
        };
        /**
        * Map clicker tool
        */
        MapToolMaker.ClickerTool = function() {
        	this.toolStatus = false;
        	this.toolName = "ClickerTool";
        	return this;
        };
        MapToolMaker.ClickerTool.prototype.activateTool = function() {
        	this.toolStatus = true;
        	configuration.map.events.register('click', configuration.map, mapClickEventHandler);
        	return this;
        };
        MapToolMaker.ClickerTool.prototype.deactivateTool = function() {
        	this.toolStatus = false;
        	configuration.map.events.unregister('click', configuration.map, mapClickEventHandler);
        	return this;
        };
        /**
        * Error request tool
        */
        MapToolMaker.ErrorRequestTool = function(controls) {
        	this.toolName = "ErrorRequestTool";
        	this.control = controls;
        	this.toolStatus = false;
        	this.control.on('click', {tool: this}, this.changeToolStatus);
        };
        MapToolMaker.ErrorRequestTool.prototype.deactivateTool = function() {
        	this.toolStatus = false;
        	configuration.map.events.unregister('click', configuration.map, this.getErrorCoordinatesFromMap);
        	this.control.css({"background-position":"0 0"});
        	/*configuration.map.getLayersBy("name", "Object")[0].destroyFeatures();
			while(configuration.map.popups.length){
				configuration.map.removePopup(configuration.map.popups[0]);
			}
			configuration.map.events.unregister('click', configuration.map, this.getCoordinatesFromMap);
			this.control.css({"background-position":"0 0"});*/	
        };
        MapToolMaker.ErrorRequestTool.prototype.activateTool = function() {
        	this.toolStatus = true;
        	configuration.map.events.register('click', configuration.map, this.getErrorCoordinatesFromMap);
        	this.control.css({"background-position":"0 -46px"});
        };
        MapToolMaker.ErrorRequestTool.prototype.getErrorCoordinatesFromMap = function(event) {
        	var clickPositionLon = configuration.map.getLonLatFromViewPortPx(event.xy).lon,
        		clickPositionLat = configuration.map.getLonLatFromViewPortPx(event.xy).lat;
        	var mapFeedbackTemplate = TemplateModule.CreateMapFeedbackTemplate({
				mapClickCoordinates: {
					lon: clickPositionLon,
					lat: clickPositionLat,
				},
				imagesOptions: getMapAreaTiles(clickPositionLon, clickPositionLat),
				info_template: new EJS({url: 'templates/map_feedback_info_template.ejs'}).render({address:"", clickCoordinates:{lon:clickPositionLon,lat:clickPositionLat}}),
			});
			mapFeedbackTemplate.render();
        };
        MapToolMaker.ErrorRequestTool.prototype.changeToolStatus = function(event) {
        	var tool = event.data.tool;
        	if (tool.toolStatus) {
        		tool.deactivateTool();
        		configuration.toolsContainer.toolsMediator.toolDeactivated();
        	} else {
        		configuration.toolsContainer.toolsMediator.toolActivated(tool);
        		tool.activateTool();
        	}
        };
        /**
        * Global coordinates converter tool
        */
        MapToolMaker.GlobalCoordinatesConverter = function(controls) {
        	this.toolName = "GlobalCoordinatesConverter";
        	this.control = controls;
        	this.toolStatus = false;
        	this.control.on('click', {tool: this}, this.changeToolStatus);
        };
        MapToolMaker.GlobalCoordinatesConverter.prototype.getCoordinatesFromMap = function(event) {
        	event = event || window.event;
        	if(typeof event.target != 'undefined'){
					if(event.target.nodeName === "DIV"){
						configuration.map.getLayersBy("name", "Object")[0].destroyFeatures();
						while(configuration.map.popups.length){
							configuration.map.removePopup(configuration.map.popups[0]);
						}
						return;
					}
			}

			/*use proxy for cross domain req*/
			var proxyRequest = $.post("?r=site/GlobalCoordinatesProxy", 
				{
					map: configuration.mapOptions.map_id,
					latitude: configuration.map.getLonLatFromViewPortPx(event.xy).lat.toString().replace('.', ','),
					longitude: configuration.map.getLonLatFromViewPortPx(event.xy).lon.toString().replace('.', ',')
				})
				.done(function(point) {
					popupTemplate = new EJS({url: 'templates/global_coordinates_popup_template.ejs'}).render(point);
					pointFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(point.y, point.x), {description: popupTemplate}, {pointRadius: 0});
					configuration.map.getLayersBy("name", "Object")[0].addFeatures([pointFeature]);
					createPopup(configuration.map, pointFeature, configuration.map.getLayersBy("name", "Object")[0]);
  				})
  				.fail(function() {
    				alert("Произошла непредвиденная ошибка! Мы пытаемся её исправить.");
  				});
        };
        MapToolMaker.GlobalCoordinatesConverter.prototype.deactivateTool = function() {
        	this.toolStatus = false;
        	configuration.map.getLayersBy("name", "Object")[0].destroyFeatures();
			while(configuration.map.popups.length){
				configuration.map.removePopup(configuration.map.popups[0]);
			}
			configuration.map.events.unregister('click', configuration.map, this.getCoordinatesFromMap);
			this.control.css({"background-position":"0 0"});	
        };
        MapToolMaker.GlobalCoordinatesConverter.prototype.activateTool = function() {
        	this.toolStatus = true;
        	configuration.map.events.register('click', configuration.map, this.getCoordinatesFromMap);
        	this.control.css({"background-position":"0 -46px"});
        };
        MapToolMaker.GlobalCoordinatesConverter.prototype.changeToolStatus = function(event) {
        	var tool = event.data.tool;
        	if (tool.toolStatus) {
        		tool.deactivateTool();
        		configuration.toolsContainer.toolsMediator.toolDeactivated();
        	} else {
        		configuration.toolsContainer.toolsMediator.toolActivated(tool);
        		tool.activateTool();
        	}
        };
        /**
        * Base Measurement tool
        */
        MapToolMaker.BaseMeasurementTool = function(toolControls) {
        	this.toolStatus = false;
        	this.toolControls = toolControls;
        	this.measurementOutput = $('<div/>', {"class": "measurement-block"});
        	this.renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
        	this.renderer = (this.renderer) ? [this.renderer] : OpenLayers.Layer.Vector.prototype.renderers;
        	this.sketchSymbolizers = {
        		"Point": {
        			pointRadius: 5,
        			graphicName: "circle",
        			fillColor: "rgb(9, 102, 131)",
        			fillOpacity: 0.3,
        			strokeWidth: 1,
        			strokeOpacity: 1,
        			strokeColor: "rgb(9, 102, 131)",
        		},
        		"Line": {
        			strokeWidth: 3,
        			strokeOpacity: 1,
        			strokeColor: "rgb(9, 102, 131)",
        			strokeDashstyle: "",
        		},
        		"Polygon": {
        			strokeWidth: 2,
        			strokeOpacity: 1,
        			strokeColor: "rgb(9, 102, 131)",
        			fillColor: "rgb(9, 102, 131)",
        			fillOpacity: 0.3,
        		},
        	};
        	this.style = new OpenLayers.Style();
        	this.style.addRules([
        		new OpenLayers.Rule({symbolizer: this.sketchSymbolizers})
        	]);
        };
        MapToolMaker.BaseMeasurementTool.prototype.aboutMe = function() {
        	return "Hi! I'm " + this.toolName;
        };
        MapToolMaker.BaseMeasurementTool.prototype.handleMeasurements = function(event) {
        	var geometry = event.geometry;
            	var units = event.units;
            	var order = event.order;
            	var measure = event.measure;
            	var element = document.getElementById('output');
            	var out = "";
            	if(order == 1) {
               	 	out += measure.toFixed(3) + " " + units;
            	} else {
                	out += measure.toFixed(3) + " " + units + "<sup>2</" + "sup>";
            	}
            	element.innerHTML = out;
        };
        /*MapToolMaker.BaseMeasurementTool.prototype.bindMouseEvent = function() {
        	configuration.map.events.register("mousemove", configuration.map, this.onMouseMove);
        };
        MapToolMaker.BaseMeasurementTool.prototype.unbindMouseEvent = function() {
        	configuration.map.events.unregister('mousemove', configuration.map, this.onMouseMove);
        };
       	MapToolMaker.BaseMeasurementTool.prototype.onMouseMove = function(e) {
        	$(".measurement-block").css('left', ((e.x) ? (e.x + 20) : (e.clientX + 20)) + 'px').css('top', ((e.y) ? (e.y - 80) : (e.clientY - 80)) + 'px');
        };*/
        /*MapToolMaker.BaseMeasurementTool.prototype.activateTool = function() {
        	this.toolStatus = true;
        	this.measureControl.activate();
        	this.bindMouseEvent();
        	$(".measurement-block").css('display', 'block');
        };
        MapToolMaker.BaseMeasurementTool.prototype.deactivateTool = function() {
        	this.toolStatus = false;
			this.unbindMouseEvent();
        	$(".measurement-block").css('display', 'none');
        	$(".measurement-block #output").empty();
        	this.measureControl.deactivate();
        };*/
        /**
		*	Distance measurement tool
		*/
        MapToolMaker.DistanceMeasurementTool = function(toolControls) {
        	this.toolName = 'DistanceMeasureTool';
        	MapToolMaker.BaseMeasurementTool.apply(this, toolControls);
        	this.measureControl = new OpenLayers.Control.Measure(OpenLayers.Handler.Path, {
        		persist: true,
                handlerOptions: {
                    layerOptions: {
                        renderers: this.renderer,
                        styleMap: new OpenLayers.StyleMap({"default": this.style}),
                    }
                }
        	});
        	this.measureControl.events.on({
        		"measure": this.handleMeasurements,
        		"measurepartial": this.handleMeasurements
        	});
        	this.measureControl.setImmediate(true);
        	configuration.map.addControl(this.measureControl);
        	//!
        	toolControls[0].on('click', {tool: this}, this.handleDistanceMeasurementTool);
        	this.handleDistanceMeasurementTool.clickTrigger = 0;
        };
        MapToolMaker.DistanceMeasurementTool.prototype = Object.create(MapToolMaker.BaseMeasurementTool.prototype);
		MapToolMaker.DistanceMeasurementTool.prototype.constructor = MapToolMaker.BaseMeasurementTool;
		MapToolMaker.DistanceMeasurementTool.prototype.handleDistanceMeasurementTool = function(event) {
			var tool = event.data.tool;
        		if(!tool.toolStatus){
        			configuration.toolsContainer.toolsMediator.toolActivated(tool);
        			tool.activateTool();
        		}
        		else{
        			tool.deactivateTool();
        			configuration.toolsContainer.toolsMediator.toolDeactivated();
        		}
		};
		MapToolMaker.DistanceMeasurementTool.prototype.activateTool = function() {
        	this.toolStatus = true;
        	this.measureControl.activate();
        	configuration.map.events.register("mousemove", configuration.map, this.onMouseMove);
        	$(".measurement-block").css('display', 'block');
        	$(this.toolControls[0]).css({"background-position":"0 -46px"});
        };
        MapToolMaker.DistanceMeasurementTool.prototype.deactivateTool = function() {
        	this.toolStatus = false;
			configuration.map.events.unregister("mousemove", configuration.map, this.onMouseMove);
        	$(".measurement-block").css('display', 'none');
        	$(".measurement-block #output").empty();
        	this.measureControl.deactivate();
        	$(this.toolControls[0]).css({"background-position":"0 0"});
        };
        MapToolMaker.DistanceMeasurementTool.prototype.onMouseMove = function(e) {
        	$(".measurement-block").css('left', ((e.x) ? (e.x + 20) : (e.clientX + 20)) + 'px').css('top', ((e.y) ? (e.y - 80) : (e.clientY - 80)) + 'px');
        };
		/**
		*	Square measurement tool
		*/
        MapToolMaker.SquareMeasurementTool = function(toolControls) {
        	this.toolName = 'SquareMeasureTool';
        	MapToolMaker.BaseMeasurementTool.apply(this, toolControls);
        	this.measureControl = new OpenLayers.Control.Measure(OpenLayers.Handler.Polygon, {
        		persist: true,
                handlerOptions: {
                    layerOptions: {
                        renderers: this.renderer,
                        styleMap: new OpenLayers.StyleMap({"default": this.style}),
                    }
                }
        	});
        	this.measureControl.events.on({
        		"measure": this.handleMeasurements,
        		"measurepartial": this.handleMeasurements
        	});
        	this.measureControl.setImmediate(true);
        	configuration.map.addControl(this.measureControl);
        	//!
        	toolControls[0].on('click', {tool: this}, this.handleDistanceMeasurementTool);
        	this.handleDistanceMeasurementTool.clickTrigger = 0;
        };
        MapToolMaker.SquareMeasurementTool.prototype = Object.create(MapToolMaker.BaseMeasurementTool.prototype);
		MapToolMaker.SquareMeasurementTool.prototype.constructor = MapToolMaker.BaseMeasurementTool;
		MapToolMaker.SquareMeasurementTool.prototype.handleDistanceMeasurementTool = function(event) {
			var tool = event.data.tool;			
        	if(!tool.toolStatus) {
        		configuration.toolsContainer.toolsMediator.toolActivated(tool);
        		tool.activateTool();
        	} else {
        		tool.deactivateTool();
        		configuration.toolsContainer.toolsMediator.toolDeactivated();
        	}
		};
		MapToolMaker.SquareMeasurementTool.prototype.activateTool = function() {
        	this.toolStatus = true;
        	this.measureControl.activate();
        	configuration.map.events.register("mousemove", configuration.map, this.onMouseMove);
        	$(".measurement-block").css('display', 'block');
        	$(this.toolControls[0]).css({"background-position":"0 -46px"});
        };
        MapToolMaker.SquareMeasurementTool.prototype.deactivateTool = function() {
        	this.toolStatus = false;
			configuration.map.events.unregister("mousemove", configuration.map, this.onMouseMove);
        	$(".measurement-block").css('display', 'none');
        	$(".measurement-block #output").empty();
        	this.measureControl.deactivate();
        	$(this.toolControls[0]).css({"background-position":"0 0"});
        };
        MapToolMaker.SquareMeasurementTool.prototype.onMouseMove = function(e) {
        	$(".measurement-block").css('left', ((e.x) ? (e.x + 20) : (e.clientX + 20)) + 'px').css('top', ((e.y) ? (e.y - 80) : (e.clientY - 80)) + 'px');
        };
        /*-------------------------------------------------*/
		return{
			/*mapContainerId, mapOptions, layers, layersControlPanel*/
			mapInitialization: function(config){
				//main module config object
				configuration = config;

				if(configuration.mapContainer){
					if(configuration.mapOptions){
						/*initialize OpenLayers map*/
						//var tileManager = new OpenLayers.TileManager({tilesPerFrame: 1, frameDelay: 1});
						configuration.map = new OpenLayers.Map(configuration.mapContainer,
							{
								minResolution : Number(configuration.mapOptions.min_resolution),
								projection : new OpenLayers.Projection("EPSG:2058"),
								maxExtent : new OpenLayers.Bounds(Number(configuration.mapOptions.bound_lon_1),Number(configuration.mapOptions.bound_lat_1),Number(configuration.mapOptions.bound_lon_2),Number(configuration.mapOptions.bound_lat_2)),
								theme : false,
								numZoomLevels : Number(configuration.mapOptions.zoom_levels),
								controls: [
									new OpenLayers.Control.PanZoomBar(),
									new OpenLayers.Control.Navigation()
								],
								maxResolution: Number(configuration.mapOptions.max_resolution),
								displayProjection : "EPSG:2058",
								units : "m",
								//tileManager: tileManager,
							});

						//add map layer, street alias layer
						addTMSLayers([{
							id: Number(configuration.mapOptions.map_id),
							alias: configuration.mapOptions.map_alias,
							visibility: true,
							tiles_path: configuration.mapOptions.tiles_path,
							layer_options: {type: "jpg", getURL: getTilesUrl}
						},
						{
							id: Number(configuration.mapOptions.map_id) + "_street",
							alias: "Streets",
							visibility: true,
							tiles_path: configuration.mapOptions.tiles_path.replace("map","text"),
							layer_options: {
								type : "png",
								getURL : getTilesUrl,
								isBaseLayer:false
							},
						}]);

						//add plugin layers (if exist)
						if(configuration.mapLayers){
							for(var i = 0, max = configuration.mapLayers.length; i < max; i++){
								addTMSLayers([{
									id: configuration.mapLayers[i].layerid,
									alias: configuration.mapLayers[i].alias,
									visibility: false,
									tiles_path: 'http://app.itsgis.ru:8732/?map=' + configuration.mapOptions.map_id + '&layer=' + configuration.mapLayers[i].layerid,
									layer_options: {
										type : "png",
										getURL : getTilesUrl,
										isBaseLayer: false,
									},
								}]);
							}
						}
						initializeLayerController(configuration.mapLayers, configuration.mapControlls.layersControl, configuration.mapOptions);

						//add service layers
						configuration.map.addLayers([ 
							new OpenLayers.Layer.Vector( "Object", {style: OpenLayers.Feature.Vector.style["default"]}), 
							new OpenLayers.Layer.Markers( "Markers" )
						]);

						//move map
						configuration.map.moveTo(new OpenLayers.LonLat(Number(configuration.mapOptions.custom_map_center_lon), Number(configuration.mapOptions.custom_map_center_lat)),0);

						//register map and other events
						bindEvents();

						//create tools
						configuration.toolsContainer = {
							tools: {
								'clickerTool': MapToolMaker.factory('ClickerTool').activateTool(),
								'squareMeasureTool': MapToolMaker.factory('SquareMeasurementTool', [configuration.mapControlls.squareMeasureTool]),
								'distanceMeasureTool': MapToolMaker.factory('DistanceMeasurementTool', [configuration.mapControlls.distanceMeasureTool]),
								'GlobalCoordinatesConverter': MapToolMaker.factory('GlobalCoordinatesConverter', configuration.mapControlls.globalCoordinatesConverterTool),
								'ErrorRequestTool': MapToolMaker.factory('ErrorRequestTool', configuration.mapControlls.errorRequestTool),
							},
						};
						//map tools mediator
						configuration.toolsContainer.toolsMediator = MapToolMaker.factory('ToolsMediator');

					}
					else{
						alert("Ошибка инициализации настроек карты!");
						$("body").empty();
					}
				}
				else{
					alert("Ошибка инициализации контейнера карты!");
					$("body").empty();
				}
			},
			getConfiguration: getConfiguration,
		}
}());