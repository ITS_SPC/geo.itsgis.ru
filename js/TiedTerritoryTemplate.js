/*----------------------------------------------------------*/
var TemplateModule = (function(){
	var info_panel = $('<div/>',{
			"class": "info-panel"
		}),
		info_panel_container = $('<div/>', {
			"class": "info-panel-container-wrapper"
		}),
		info_panel_overlay = $('<div/>', {
			'class': "info-panel-close info-panel-overlay"
		}),
		close_button = $('<a/>',{
			'style': "display:inline",
			"class": "info-panel-close info-panel-close-button"
		}), 
		checkObject = function(object){
			for (var property in object) {
				if (object.hasOwnProperty(property)){
					return true;
				}
    		}
    		return false;
		},
		info_panel_content = $(".info-panel-content");
	/**
	*	Inheritance function.
	*/
	function inherits(child, parent) {
		child.prototype = Object.create(parent.prototype);
		child.prototype.constructor = parent;
	}
	function implementCreateObjectFunction(){
		if (!Object.create) {
    		Object.create = function(object, properties) {
        		if (typeof object !== 'object' && typeof object !== 'function'){
        			throw new TypeError('Object prototype may only be an Object: ' + object);
        		}
        		else{
        			if (object === null) throw new Error("This browser's implementation of Object.create is a shim and doesn't support 'null' as the first argument.");
        		} 
        		if (typeof properties != 'undefined'){
        			throw new Error("This browser's implementation of Object.create is a shim and doesn't support a second argument.");
        		}
        		function F() {}
        		F.prototype = object;
        		return new F();
    		};
		}
	}
	function BasicTemplate(config){
		this.configuration = config;
	}
	BasicTemplate.prototype.getTemplateConfiguration = function(){
		return this.configuration;
	};
	BasicTemplate.prototype.render = function(){
		info_panel.empty();		
		info_panel_container.empty();
		info_panel_overlay.css("display", "block");
		info_panel_container.append(this.configuration.info_template);
		info_panel.append(info_panel_container);
		info_panel.append(close_button);
    	info_panel.css("display", "block");
    	$("body").append(info_panel_overlay);
    	$("body").append(info_panel);
    	this.resizeInfoPanel();
    	this.bindEvents();
	};
	BasicTemplate.prototype.renderPhotoes = function(event){
		//render photos (override this if you need)
		var photoes = [];
		if(this.configuration.photoes && (this.configuration.photoes.length)){
			for(var i = 0, maxi = this.configuration.photoes.length; i < maxi; i++){
				photoes.push({href: this.configuration.photoes[i].photo_link});
			}
			$.fancybox(photoes, {
         		loop : true,
   				'titlePosition' 	: 'over',
   				'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
		    		return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
				}
			});
		}
	};
	BasicTemplate.prototype.closeInfoPanel = function(){
		//close info panel
		info_panel.css("display", "none");
		info_panel_overlay.css("display", "none");
		info_panel.empty();
		this.unbinedEvents();
	};
	BasicTemplate.prototype.resizeInfoPanel = function(){
		//on resize window
		if($(window).height() <= 500){
			info_panel_container.css({"max-height": $(window).height()});
			info_panel_content.css({"max-height": $(window).height()});
		}
		else{
			info_panel_container.css({"max-height": 500});
			info_panel_content.css({"max-height": 440});
		}
		info_panel.css("top", /*(($(window).height() - info_panel.outerHeight()) / 2) + $(window).scrollTop() + "px"*/"10px");
    	info_panel.css("left", (($(window).width() - info_panel.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	};
	BasicTemplate.prototype.initTemplate = function(){
		$("#info-button").on("click", this.render.bind(this));
		$( window ).resize(this.resizeInfoPanel);
	};
	BasicTemplate.prototype.bindEvents = function(){
		$("body").delegate("#show_photoes", "click", this.renderPhotoes.bind(this));
		$("body").delegate(".info-panel-close", "click", this.closeInfoPanel.bind(this));
	};
	BasicTemplate.prototype.unbinedEvents = function(){
		$("body").undelegate( "#show_photoes", "click");
		$("body").undelegate( ".info-panel-close", "click");
	};
	/**
	*	Implement Object.create function for ie8
	*/
	implementCreateObjectFunction();
	/**
	*	Tied territory template class.
	*	Displays form with full information about tied territory object
	*	Overrides: renderPhotoes
	*
	*/
	function TiedTerritoryTemplate(config){
		BasicTemplate.apply(this, arguments);
	};
	inherits(TiedTerritoryTemplate, BasicTemplate);
	TiedTerritoryTemplate.prototype.renderPhotoes = function(event){
		var photoes = [];
			if(this.configuration.tied_territory_contracts[0].photoes.length){
				for(var i = 0, maxi = this.configuration.tied_territory_contracts[0].photoes.length; i < maxi; i++){
					photoes.push({href: this.configuration.tied_territory_contracts[0].photoes[i].photo_link});
				}
			}
         	$.fancybox(photoes, {
         		loop : true,
   				'titlePosition' 	: 'over',
   				'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
		    		return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
				}
			});
	};
	/**
	*	Barricado template class.
	*	Displays form with full information about barricado object
	*
	*
	*/
	function BarricadoTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(BarricadoTemplate, BasicTemplate);
	/**
	*	Cablenetwork light posts template class.
	*	Displays form with full information about cablenetwork lightpost object
	*
	*
	*/
	function LightPostTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(LightPostTemplate, BasicTemplate);
	LightPostTemplate.prototype.bindEvents = function () {
		BasicTemplate.prototype.bindEvents.apply(this, arguments);
		$("body").delegate(".about-light", "click", function(){
			$(this).next(".about-light-container").slideToggle( "slow" );
		});
	};
	LightPostTemplate.prototype.unbinedEvents = function(){
		BasicTemplate.prototype.unbinedEvents.apply(this,arguments);
		$("body").undelegate( ".about-light", "click");
	};


	/**
	*	Busstop template class.
	*	Displays form with full information about busstop object
	*
	*
	*/
	function BusstopTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(BusstopTemplate, BasicTemplate);
	/**
	*	Road marking template class.
	*	Displays form with full information about road marking object
	*
	*
	*/
	function RoadMarkingTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(RoadMarkingTemplate, BasicTemplate);
	/**
	*	House template class.
	*	Displays form with full information about house object
	*
	*
	*/
	function HouseTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(HouseTemplate, BasicTemplate);
	HouseTemplate.prototype.bindEvents = function(){
		BasicTemplate.prototype.bindEvents.apply(this, arguments);
		$("body").delegate(".organization-link", "click", function(){
			$(this).next("li").slideToggle( "slow" );
			//$('.info-panel-content').animate({scrollTop: $(this).offset().top - 70}, 500);
		});
		$("body").delegate(".organization-schedule-link", "click", function(){
			$(this).next(".organization-schedule-container").slideToggle( "slow" );
		});
	};
	HouseTemplate.prototype.unbinedEvents = function(){
		BasicTemplate.prototype.unbinedEvents.apply(this, arguments);
		$("body").undelegate( ".organization-link", "click");
		$("body").undelegate( ".organization-schedule-link", "click");
	};
	/**
	*	Crossroad template class.
	*	Displays form with full information about crossroad object
	*
	*
	*/
	function CrossroadTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(CrossroadTemplate, BasicTemplate);
	/**
	*	Street template class.
	*	Displays form with full information about street object
	*
	*
	*/
	function StreetTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(StreetTemplate, BasicTemplate);
	/**
	*	Post template class.
	*	Displays form with full information about post object
	*
	*
	*/
	function SignPostTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(SignPostTemplate, BasicTemplate);
	SignPostTemplate.prototype.bindEvents = function(){
		BasicTemplate.prototype.bindEvents.apply(this, arguments);
		$("body").delegate(".about-sign", "click", function(){
			$(this).next(".about-sign-container").slideToggle( "slow" );
		});
	};
	SignPostTemplate.prototype.unbinedEvents = function(){
		BasicTemplate.prototype.unbinedEvents.apply(this, arguments);
		$("body").undelegate( ".about-sign", "click");
	};
	SignPostTemplate.prototype.renderPhotoes = function(event){
		/*get string "index1_index2", where index1 - index in grouping array, index2 - index in grouping objects array*/
		var photoes = []/*,
			object_destination = event.target.className*/;
		//object_destination = object_destination.split("_");
		if(this.configuration /*&& object_destination.length*/){
			if(this.configuration.post_groupings && this.configuration.post_groupings.length){
				for(var i = 0, maxi = this.configuration.post_groupings.length; i < maxi; i++){
					if(this.configuration.post_groupings[i].post_objects && this.configuration.post_groupings[i].post_objects.length){
						for(var j = 0, maxj = this.configuration.post_groupings[i].post_objects.length; j < maxj; j++){
							if(this.configuration.post_groupings[i].post_objects[j].id == event.target.className){
								if(this.configuration.post_groupings[i].post_objects[j].photoes && this.configuration.post_groupings[i].post_objects[j].photoes.length){
									for(var p = 0, maxp = this.configuration.post_groupings[i].post_objects[j].photoes.length; p < maxp; p++){
										photoes.push({href: this.configuration.post_groupings[i].post_objects[j].photoes[p].photo_link});
									}
								}
								i = maxi;
								j = maxj;
							}
						}
					}
				}
			}
			/*for(var i = 0, maxi = this.configuration.post_groupings[object_destination[0]].post_objects[object_destination[1]].photoes.length; i < maxi; i++){
				photoes.push({href: this.configuration.post_groupings[object_destination[0]].post_objects[object_destination[1]].photoes[i].photo_link});
			}*/
			if(photoes.length){
				$.fancybox(photoes, {
         			loop : true,
   					'titlePosition' 	: 'over',
   					'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
		    			return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
					}
				});
			}
		}
	};
	/**
	*	Advertisment template class.
	*	Displays form with full information about advertisment object
	*
	*
	*/
	function AdvertisementTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(AdvertisementTemplate, BasicTemplate);
	/**
	*	Default template
	*/
	function DefaultTemplate(config) {
		BasicTemplate.apply(this, arguments);
	}
	inherits(DefaultTemplate, BasicTemplate);
	DefaultTemplate.prototype = Object.create(BasicTemplate.prototype);
	DefaultTemplate.prototype.constructor = DefaultTemplate;
	DefaultTemplate.prototype.initTemplate = function() {
		$( window ).resize(this.resizeInfoPanel);
	};
	/**
	*	Map feedback template class.
	*	Displays form piece of map and comment input
	*
	*
	*/
	function MapFeedbackTemplate(config){
		BasicTemplate.apply(this, arguments);
	}
	inherits(MapFeedbackTemplate, BasicTemplate);
	MapFeedbackTemplate.prototype.initTemplate = function() {
		$("#feedback-button").on("click", this.render.bind(this));
		$( window ).resize(this.resizeInfoPanel);
	};
	MapFeedbackTemplate.prototype.sendFeedback = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
		//clear
		$("#feedback-error-container").empty();
		var options = {
			url: '?r=site/SendFeedback',
			type: 'POST',
			success: function(data) {
				if (typeof data !== "object") {
					TemplateModule.CreateDefaultTemplate({info_template: new EJS({url: 'templates/message_info_template.ejs'}).render({messageHeader: "", messageBody: data})}).render();
				} else {
					//add error messages on view
					$("#feedback-error-container").empty();
					for (var key in data) {
						$("#feedback-error-container").append(data[key].join("<br>")).append("<br>");
					}
					$("#feedback-form").children().prop('disabled', false);
				}
			},
		};
		$(this).ajaxSubmit(options);
		$("#feedback-form").children().prop('disabled', true);
	};
	MapFeedbackTemplate.prototype.bindEvents = function() {
		BasicTemplate.prototype.bindEvents.apply(this, arguments);
		$("body").delegate("#feedback-form", "submit", this.sendFeedback);
	};
	MapFeedbackTemplate.prototype.unbinedEvents = function() {
		BasicTemplate.prototype.unbinedEvents.apply(this, arguments);
		$("body").undelegate( "#feedback-form", "submit");
	};
	return {
		CreateDefaultTemplate: function(config) {
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var default_template = new DefaultTemplate(config);
				default_template.initTemplate();
				return default_template;
			}
			catch(err){
				//console.log(err);
			}
		},
		CreateTiedTerritoryTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var tt_template = new TiedTerritoryTemplate(config);
				tt_template.initTemplate();
				return tt_template;
			}
			catch(err){
			}
		},
		CreateBarricadoTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var barricadoTemplate = new BarricadoTemplate(config);
				barricadoTemplate.initTemplate();
				return barricadoTemplate;
			}
			catch(err){
			}
		},
		CreateLightPostTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var lightPostTemplate = new LightPostTemplate(config);
				lightPostTemplate.initTemplate();
				return lightPostTemplate;
			}
			catch(err){
			}
		},
		CreateBusstopTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var busstopTemplate = new BusstopTemplate(config);
				busstopTemplate.initTemplate();
				return busstopTemplate;
			}
			catch(err){
			}
		},
		CreateRoadMarkingTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var roadMarkingTemplate = new RoadMarkingTemplate(config);
				roadMarkingTemplate.initTemplate();
				return roadMarkingTemplate;
			}
			catch(err){
			}
		},
		CreateCrossroadTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var crossroadTemplate = new CrossroadTemplate(config);
				crossroadTemplate.initTemplate();
				return crossroadTemplate;
			}
			catch(err){
			}
		},
		CreateHouseTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var houseTemplate = new HouseTemplate(config);
				houseTemplate.initTemplate();
				return houseTemplate;
			}
			catch(err){
			}
		},
		CreateStreetTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var streetTemplate = new StreetTemplate(config);
				streetTemplate.initTemplate();
				return streetTemplate;
			}
			catch(err){
			}
		},
		CreateAdvertisementTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var advertisementTemplate = new AdvertisementTemplate(config);
				advertisementTemplate.initTemplate();
				return advertisementTemplate;
			}
			catch(err){
			}
		},
		CreateSignPostTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var signPostTemplate = new SignPostTemplate(config);
				signPostTemplate.initTemplate();
				return signPostTemplate;
			}
			catch(err){
			}
		},
		CreateMapFeedbackTemplate: function(config){
			try{
				if((typeof config) != "object"){
					throw "template configuration is not an object...";
				}
				else{
					if(!checkObject(config)){
						throw "template configuration is an empty object...";
					}
				}
				var mapFeedbackTemplate = new MapFeedbackTemplate(config);
				mapFeedbackTemplate.initTemplate();
				return mapFeedbackTemplate;
			}
			catch(err){
				//console.log(err);
			}
		},
	};
})();
/*-----------------------------------------------------------------------------------*/
