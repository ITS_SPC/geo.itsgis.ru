<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::$classMap = array(	//Entities
						'Organization' => 'protected/components/Managers/Organization.php',
						'TiedTerritoryContract' => 'protected/models/Entities/TiedTerritoryContract.php',
						'TiedTerritory' => 'protected/models/Entities/TiedTerritory.php',
						'Organization' => 'protected/models/Entities/Organization.php',
						'Address' => 'protected/models/Entities/Address.php',
						'District' => 'protected/models/Entities/District.php',
						'Object' => 'protected/models/Entities/Object.php',
						'Map' => 'protected/models/Entities/Map.php',
						'Layer' => 'protected/models/Entities/Layer.php',
						'Layerobject' => 'protected/models/Entities/Layerobject.php',
						'Mapobject' => 'protected/models/Entities/Mapobject.php',
						'MapOptions' => 'protected/models/Entities/MapOptions.php',
						'User' => 'protected/models/Entities/User.php',
						'Featureobject' => 'protected/models/Entities/Featureobject.php',
						'Contact' => 'protected/models/Entities/Contact.php',
						'LightPost' => 'protected/models/Entities/LightPost.php',
						'LightPostGrouping' => 'protected/models/Entities/LightPostGrouping.php',
						'LightPostLamp' => 'protected/models/Entities/LightPostLamp.php',
						'LampType' => 'protected/models/Entities/LampType.php',
						'RoadMarking' => 'protected/models/Entities/RoadMarking.php',
						'RoadMarkingType' => 'protected/models/Entities/RoadMarkingType.php',
						'RoadMarkingMaterial' => 'protected/models/Entities/RoadMarkingMaterial.php',
						'Barricado' => 'protected/models/Entities/Barricado.php',
						'BarricadoType' => 'protected/models/Entities/BarricadoType.php',
						'SignPost' => 'protected/models/Entities/SignPost.php',
						'PostObject' => 'protected/models/Entities/PostObject.php',
						'PostGrouping' => 'protected/models/Entities/PostGrouping.php',
						'Sign' => 'protected/models/Entities/Sign.php',
						'TrafficLight' => 'protected/models/Entities/TrafficLight.php',
						'SignType' => 'protected/models/Entities/SignType.php',
						'SignSize' => 'protected/models/Entities/SignSize.php',
						'Busstop' => 'protected/models/Entities/Busstop.php',
						'Route' => 'protected/models/Entities/Route.php',
						'Advertisement' => 'protected/models/Entities/Advertisement.php',
						'AdvertisementType' => 'protected/models/Entities/AdvertisementType.php',
						'AdvertisementCanvas' => 'protected/models/Entities/AdvertisementCanvas.php',
						'AdvertisementSide' => 'protected/models/Entities/AdvertisementSide.php',
						'Photo' => 'protected/models/Entities/Photo.php',
						'House' => 'protected/models/Entities/House.php',
						'Crossroad' => 'protected/models/Entities/Crossroad.php',
						'Street' => 'protected/models/Entities/Street.php',
						'TrafficLightConstruct' => 'protected/models/Entities/TrafficLightConstruct.php',
						'Point' => 'protected/models/Entities/Point.php',
						//Managers
						'UserManager' => 'protected/models/Managers/UserManager.php',
						'MapManager' => 'protected/models/Managers/MapManager.php',
						'LayerManager' => 'protected/models/Managers/LayerManager.php',
						'TiedTerritoryManager' => 'protected/models/Managers/TiedTerritoryManager.php',
						'OrganizationManager' => 'protected/models/Managers/OrganizationManager.php',
						'AddressManager' => 'protected/models/Managers/AddressManager.php',
						'ContactManager' => 'protected/models/Managers/ContactManager.php',
						'LightPostManager' => 'protected/models/Managers/LightPostManager.php',
						'RoadMarkingManager' => 'protected/models/Managers/RoadMarkingManager.php',
						'BarricadoManager' => 'protected/models/Managers/BarricadoManager.php',
						'SignPostManager' => 'protected/models/Managers/SignPostManager.php',
						'BusstopManager' => 'protected/models/Managers/BusstopManager.php',
						'AdvertisementManager' => 'protected/models/Managers/AdvertisementManager.php',
						'PhotoManager' => 'protected/models/Managers/PhotoManager.php',
						'TargetedPlanManager' => 'protected/models/Managers/TargetedPlanManager.php',
						//Forms
						'UserLoginForm' => 'protected/models/FormsModels/UserLoginForm.php',
						'UserRegistrationForm' => 'protected/models/FormsModels/UserRegistrationForm.php',
						'ContactForm' => 'protected/models/FormsModels/ContactForm.php',
						'LoginForm' => 'protected/models/FormsModels/LoginForm.php',
						'MapFeedbackForm' => 'protected/models/FormsModels/MapFeedbackForm.php',
						//Dictionary
						'ObjectsDictionary' => 'protected/models/Entities/ObjectsDictionary.php',
						//Manager interfaces
						'iMapManager' => 'protected/models/ManagersInterfaces/iMapManager.php',
						'iOrganizationManager' => 'protected/models/ManagersInterfaces/iOrganizationManager.php',
						'iBarricadoManager' => 'protected/models/ManagersInterfaces/iBarricadoManager.php',
						'iTiedTerritoryManager' => 'protected/models/ManagersInterfaces/iTiedTerritoryManager.php',
						'iBusstopManager' => 'protected/models/ManagersInterfaces/iBusstopManager.php',
						'iAddressManager' => 'protected/models/ManagersInterfaces/iAddressManager.php',
						'iRoadMarkingManager' => 'protected/models/ManagersInterfaces/iRoadMarkingManager.php',
						'iAdvertisementManager' => 'protected/models/ManagersInterfaces/iAdvertisementManager.php',
						'iPhotoManager' => 'protected/models/ManagersInterfaces/iPhotoManager.php',
						'iContactManager' => 'protected/models/ManagersInterfaces/iContactManager.php',
						'iUserManager' => 'protected/models/ManagersInterfaces/iUserManager.php',
						'iLayerManager' => 'protected/models/ManagersInterfaces/iLayerManager.php',
						'iTargetedPlanManager' => 'protected/models/ManagersInterfaces/iTargetedPlanManager.php',
						'iLightPostManager' => 'protected/models/ManagersInterfaces/iLightPostManager.php',
						'iSignPostManager' => 'protected/models/ManagersInterfaces/iSignPostManager.php',
						);
Yii::createWebApplication($config)->run();
